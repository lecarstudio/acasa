<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Favorite extends Model {

    protected $fillable = [
        'user_id', 'store_id'
    ];

    public function store(){
        return $this->belongsTo(Store::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}