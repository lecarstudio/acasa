<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class DetailOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id){
        $orders = Order::where('id', $id)->get();
        return view('front.detail_order', compact('orders'));
    }
}
