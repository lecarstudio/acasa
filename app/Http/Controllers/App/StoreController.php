<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Store;
use App\Favorite;

class StoreController extends Controller
{
    public function index($id){
        $stores = Store::where('category_id',$id)->get();
        return response()->json([
            'status'=>true,
            'stores' => $stores
        ]);
    }
    public function show(Request $request, $id){
        $stores = Store::find($id);
        $isFav = Favorite::where('user_id', $request->user_id)
                    ->where('store_id', $id)->count() > 0;
        return response()->json([
            'status'=>true,
            'store' => $stores->load('packages','packages.products','packages.products.options','packages.products.promotions','subcategory','user'),
            'isFav' => $isFav
        ]);
    } 

    public function search(Request $request){
        $stores = Store::where('category_id',$request->category_id)->where('name', 'LIKE', '%'.$request->search.'%')->get();
        return response()->json([
            'status'=>true,
            'stores' => $stores
        ]);
    }
   
}
