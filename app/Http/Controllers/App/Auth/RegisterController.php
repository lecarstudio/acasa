<?php

namespace App\Http\Controllers\App\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class RegisterController extends Controller
{
    //
    public function Register(Request $request){

  
    $customAttributes  = [
        'email' => 'Dirección de correo',
        'name' => 'Nombre',
        'username' => 'Nombre de usuario',
        'password' => 'Contraseña',
        'address' => 'Dirección',
        'state' => 'Estado',
        'city' => 'Ciudad',
        'phone' => 'Teléfono',
    ];
        
      	$validator = Validator::make($request->all(), [
            'username' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'address' => 'required|string|max:255',
            'state' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
        ],[],$customAttributes );

      if($validator->fails()){
        return response()->json([
            'status'=>false,
            'errors'=> $validator->errors()->all()
        ]);
      }else{
        $user = User::create($request->except('password')); 
        $user->password = bcrypt($request->password);
        $user->save();
        return response()->json([
            'status'=>true,
            'user'=>$user
        ]);
      }
    }

    

public function RegisterModal(Request $request){

  
  $customAttributes  = [
      'email' => 'Dirección de correo',
      'name' => 'Nombre',
      'last_name' => 'Apellidos',
      'password' => 'Contraseña',
      'city' => 'Ciudad',
      'phone' => 'Teléfono',
  ];
      
      $validator = Validator::make($request->all(), [
          'name' => 'required|string|max:255',
          'last_name' => 'required|string|max:255',
          'email' => 'required|string|email|max:255|unique:users',
          'password' => 'required|string|min:8|confirmed',
          'phone' => 'required|string|max:255',
      ],[],$customAttributes );

    if($validator->fails()){
      return response()->json([
          'status'=>false,
          'errors'=> $validator->errors()->all()
      ]);
    }else{
      $user = User::create($request->except('password')); 
      $user->password = bcrypt($request->password);
      $user->save();
      $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
          return response()->json([
            'status'=>true,
            'user'=>$user
        ]);
        }
    }
  }

  public function RegisterPartner(Request $request){
  
    
    $customAttributes  = [
        'email' => 'Dirección de correo',
        'name' => 'Nombre',
        'last_name' => 'Apellidos',
        'password' => 'Contraseña',
        'city' => 'Ciudad',
        'phone' => 'Teléfono',
        'company' => 'Negocio',
        'nit' => 'Nit',
        'fiscal_direction' => 'Dirección fiscal',
        'bank_id' => 'Banco',
        'bank_account' => 'Número de cuenta'
    ];
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'phone' => 'required|string|max:255',
            'company' => 'required|string|max:255',
            'nit' => 'required|string|max:255',
            'fiscal_direction' => 'required|string|max:255',
            'bank_id' => 'required',
            'bank_account' => 'required'

        ],[],$customAttributes );
  
      if($validator->fails()){
        return response()->json([
            'status'=>false,
            'errors'=> $validator->errors()->all()
        ]);
      }else{
        $user = User::create($request->except('password')); 
        $user->password = bcrypt($request->password);
        $user->save();
        $credentials = $request->only('email', 'password');
          if (Auth::attempt($credentials)) {
            return response()->json([
              'status'=>true,
              'user'=>$user
          ]);
          }
      }
    }

    public function registerMovil(Request $request){

  
      $customAttributes  = [
          'email' => 'Dirección de correo',
          'name' => 'Nombre de usuario',
          'password' => 'Contraseña',
      ];
          
          $validator = Validator::make($request->all(), [
              'name' => 'required|string|max:255',
              'email' => 'required|string|email|max:255|unique:users',
              'password' => 'required|string|min:8|confirmed',
          ],[],$customAttributes );
  
        if($validator->fails()){
          return response()->json([
              'status'=>false,
              'msgs'=> $validator->errors()->all()
          ]);
        }else{
          $user = User::create($request->except('password')); 
          $user->password = bcrypt($request->password);
          $user->save();
          return response()->json([
              'status'=>true,
              'user'=>$user
          ]);
        }
      }
}