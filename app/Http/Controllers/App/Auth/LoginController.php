<?php

namespace App\Http\Controllers\App\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Validator;
use Auth;


class LoginController extends Controller
{

    public function login(Request $request){
      $customAttributes  = [
        'email' => '"Correo Electrónico"',
        'password' => '"Contraseña"',
      ];
     
      $validator = Validator::make($request->all(),[
        'email'=>'required|email',
        'password'=>'required|min:6'
      ],[],$customAttributes);
      if($validator->fails()){
        return response()->json([
          'status'=>false,
          'title' =>'Ups! Ha ocurrido un error',
          'msgs'=> $validator->errors()->all()
          ]);
      }else{

        if(Auth::attempt($request->except('_token'))){
          return response()->json(['status'=>true,'errors'=>['Usuario logeado'],'user'=>Auth::user()]);

        }else{
		      return response()->json([
            'status'=>false,
            'title' => "Ups! Ha ocurrido un error",
            'msgs' => ["Correo o contraseña incorrectos.","Por favor verifique sus datos"]
          ]);
        }
      }
    }

    public function token(Request $request){
      $user = User::find($request->user_id);
      return response()->json([
        'status'=>true,
        'errors'=>['Usuario logeado'],
        'user'=> $user
      ]);
    }
   
}
