<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\City;
 
class CityController extends Controller
{
    public function index(){
        $cities = City::all();
        return response()->json([
            'status'=>true,
            'cities' => $cities
        ]);
    } 
   
}
