<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Category;
 
class CategoryController extends Controller
{
    public function index(){
        $categories = Category::all();
        return response()->json([
            'status'=>true,
            'categories' => $categories,
        ]);
    } 
   
}
