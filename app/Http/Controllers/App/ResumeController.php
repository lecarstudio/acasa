<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
 
class ResumeController extends Controller
{
    public function days(){
        $weekMap = [
            1 => 'Lunes',
            2 => 'Martes',
            3 => 'Miercoles',
            4 => 'Jueves',
            5 => 'Viernes',
            6 => 'Sabado',
            0 => 'Domingo',
        ];

        //Calcula el día
        $days = collect([]);
        $dayOfTheWeek = Carbon::now()->dayOfWeek;
        $weekMap[$dayOfTheWeek] = 'Hoy';
        if($dayOfTheWeek == 6)
            $weekMap[0] = 'Mañana';
        else
            $weekMap[$dayOfTheWeek+1] = 'Mañana';

        for ($i=0; $i < count($weekMap) ; $i++) {
            if($dayOfTheWeek == $i)
                $days->push([ 
                    'label' => $weekMap[$i],
                    'value' => Carbon::now()->toDateString(),
                    'day' => Carbon::now()->format('d')
                    ]
                );
            else
                $days->push([ 
                    'label' => $weekMap[$i],
                    'value' => Carbon::now()->next($i)->toDateString(),
                    'day' => Carbon::now()->next($i)->format('d')
                    ]
                );
        }
        //Calculas las horas
        $hours = collect([]);
        $mytime =Carbon::now()->format('H:i:s');
        $hour =Carbon::now()->format('H');
        for ($i=$hour; $i < 24 ; $i++) { 
            $hours->push([ 
                'label' => $i.':00 - '.($i+1).':00',
                'value' => $i.':00 - '.($i+1).':00'
                ]
            );
        }
        
        return response()->json([
            'status'=>true,
            'days' => $days,
            'hours' => $hours
        ]);
    } 
   
}
