<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Order;
use App\Store;
use App\Option;
use App\Product;
use App\OrderProduct;
use App\OptionOrderProduct;
use App\Setting;

class OrderController extends Controller
{
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
       
        if(Order::where('user_id',$request->user_id)->where('status','Carrito')->where('store_id',$request->store_id)->count() > 0){
            $order = Order::where('user_id',$request->user_id)->where('status','Carrito')->where('store_id',$request->store_id)->get()->first();
            $order = $order->load('products','products.promotions');
            foreach ($order->products as $key => $product) {
               $product->pivot->options;
            }
            $this->totalOrder($order);
            $order->save();
            
            return response()->json([
                'status'=> true,
                'order' => $order,
            ]);
        }else{
            return response()->json([
                'status'=> true,
                'order' => []
            ]);
        }
    }

    public function all(Request $request){
       
        if(Order::where('user_id',$request->user_id)->count() > 0){
            $orders = Order::where('user_id',$request->user_id)->with('products','products.promotions','store')->get();
            // foreach ($orders->products as $key => $product) {
            //    $product->pivot->options;
            // }
            // $this->totalOrder($order);
            // $orders->save();
            
            return response()->json([
                'status'=> true,
                'orders' => $orders,
            ]);
        }else{
            return response()->json([
                'status'=> true,
                'orders' => []
            ]);
        }
    }

    public function store(Request $request){

        $numOptions = count($request->options);
        $products = Product::where('id', $request->product_id)->first();
        $conOptions = 0;
        foreach ($request->options as $key => $option_id) {
            $option = Option::find($option_id);
            if($option->price == 0){
                $conOptions++;
            }
        }
        if($conOptions > $products->limit_options){
            return response()->json([
                'status'=> false,
                'title' =>'Revisa tu pedido',
                'text' => 'Sólo puedes agregar '.$products->limit_options.' opciones gratis'
            ]);
        }


        $store = Store::find($request->store_id);
        if($store->horary_open <= date('H:i:s') &&
            $store->horary_close >= date('H:i:s')){
            try{
                if(Order::where('user_id',$request->user_id)->where('status','Carrito')->where('store_id',$request->store_id)->count() > 0)
                    $order = Order::where('user_id',$request->user_id)->where('status','Carrito')->where('store_id',$request->store_id)->get()->first();
                else{
                    $order = Order::create($request->all());
                }
                if(!$order->products->contains($request->product_id)){
                    $order->products()->attach($request->product_id, ['qty' => $request->qty]);
                    $orderProduct  = OrderProduct::where('product_id',$request->product_id)->where('order_id',$order->id)->first();
                    foreach ($request->options as $key => $option_id) {
                        if(!$orderProduct->options->contains($option_id)){
                            $orderProduct->options()->attach($option_id, ['product_id' => $request->product_id]);
                        }
                    }
                
                }
                $order->save();
                $order = $order->load('products','products.promotions');
                $this->totalOrder($order);
                
                $order->save();
                foreach ($order->products as $key => $product) {
                    $product->pivot->options;
                 }


                return response()->json([
                    'status'=> true,
                    'order' => $order
                ]);

            } catch (QueryException $e) {
                return response()->json([
                    'status' => false,
                    'error' => $e,
                    'title' =>'Ups! Error inesperado',
                    'text' => 'Lo sentimos un error ha ocurrido, por favor contacte a su administrador del sistema.'
                ]);
            }
        }else{
            return response()->json([
                'status'=> false,
                'title' =>'Ups! Establecimiento Cerrado',
                'text' => 'Agrega productos cuando el establecimiento este abierto.'
            ]);
        }
    }
    public function delete(Request $request){
        try{
            $order = Order::find($request->order_id);
            $order->products()->detach($request->product_id);
            $order->save();
            $this->totalOrder($order);
            $order->save();
            $order = $order->load('products','products.promotions');
            foreach ($order->products as  $product) {
                $product->pivot->options;
            }

            return response()->json([
                'status'=> true,
                'order' => $order
                ]);

        } catch (QueryException $e) {
            return response()->json([
                'status' => false,
                'error' => $e
            ]);
        }
    }

    public function resume(Request $request){
        try{
            $order = Order::find($request->order_id);
            $order = $order->load('products','products.promotions');
            foreach ($order->products as $key => $product) {
                $product->pivot->options;
            }
            return response()->json([
                'status'=> true,
                'order' => $order
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'status' => false,
                'error' => $e
            ]);
        }
    }

    public function confirm(Request $request){
        try{
            $order = Order::find($request->order_id);
            $order->fill($request->except('order_id','delivery_date'));
            $date = date_create($request->delivery_date);
            $order->delivery_date = date_format($date,"Y-m-d");
            $order->status = 'Pendiente';
            $order->save();

            return response()->json([
                'status'=> true
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'status' => false,
                'error' => $e
            ]);
        }
    }

    public function storeOption(Request $request){
        // $store = Store::find($request->store_id);
        // if($store->horary_open <= date('H:i:s') &&
        //     $store->horary_close >= date('H:i:s')){
            try{
                $order = Order::find($request->order_id);
              
                if($order->products->contains($request->product_id)){
                    $orderProduct  = OrderProduct::where('product_id',$request->product_id)->where('order_id',$order->id)->first();
                    if(!$orderProduct->options->contains($request->option_id)){
                        $orderProduct->options()->attach($request->option_id, ['product_id' => $request->product_id]);
                    }
                }
                $order->save();
                $order = $order->load('products','products.promotions');
                $this->totalOrder($order);
                
                $order->save();
                foreach ($order->products as $key => $product) {
                    $product->pivot->options;
                 }


                return response()->json([
                    'status'=> true,
                    'order' => $order
                ]);

            } catch (QueryException $e) {
                return response()->json([
                    'status' => false,
                    'error' => $e,
                    'title' =>'Ups! Error inesperado',
                    'text' => 'Lo sentimos un error ha ocurrido, por favor contacte a su administrador del sistema.'
                ]);
            }
        // }else{
        //     return response()->json([
        //         'status'=> false,
        //         'title' =>'Ups! Establecimiento Cerrado',
        //         'text' => 'Agrega productos cuando el establecimiento este abierto.'
        //     ]);
        // }
    }

    public function deleteOption(Request $request){
        try{
            if($request->isMovil){
                $order = Order::find($request->order_id);
                if($order->products->contains($request->product_id)){
                    $orderProduct  = OrderProduct::where('product_id',$request->product_id)->where('order_id',$order->id)->first();
                    $option = OptionOrderProduct::where('order_product_id',$orderProduct->id)->where('option_id', $request->option_id)->first();
                }
            }else
                $option = OptionOrderProduct::find($request->id);
            $option->delete();
            $order = Order::find($request->order_id);
            $this->totalOrder($order);
            $order->save();

            $order = $order->load('products','products.promotions');
            foreach ($order->products as  $product) {
                $product->pivot->options;
            }

            return response()->json([
                'status'=> true,
                'order' => $order
            ]);

        } catch (QueryException $e) {
            return response()->json([
                'status' => false,
                'error' => $e
            ]);
        }
    }


    public function updateQty(Request $request){
        $order = Order::find($request->order_id);

        $productDetail = $order->products->find($request->product_id);
        if($productDetail->pivot->qty >= 1 && $request->qty != -1){
            $order->products()->updateExistingPivot($request->product_id, ['qty' => $productDetail->pivot->qty + $request->qty]);
        }
        if($productDetail->pivot->qty >= 2 && $request->qty != 1){
            $order->products()->updateExistingPivot($request->product_id, ['qty' => $productDetail->pivot->qty + $request->qty]);
        }
      
        $order->save();
        $order = $order->load('products','products.promotions');
        $this->totalOrder($order);
        $order->save();

        foreach ($order->products as  $product) {
            $product->pivot->options;
        }

        return response()->json([
            'status'=> true,
            'order' => $order,
        ]);

    }

    public function updateOptionQty(Request $request){
        if($request->isMovil){
           
            $order = Order::find($request->order_id);
            if($order->products->contains($request->product_id)){
                $orderProduct  = OrderProduct::where('product_id',$request->product_id)->where('order_id',$order->id)->first();
                $option = OptionOrderProduct::where('order_product_id',$orderProduct->id)->where('option_id', $request->option_id)->first();
            }
        }else
            $option = OptionOrderProduct::find($request->id);

        if($option->qty >= 1 && $request->qty != -1){
            $option->qty = $option->qty + $request->qty;
        }
        if($option->qty >= 2 && $request->qty != 1){
            $option->qty = $option->qty + $request->qty;
        }
        $option->save();
        $order = Order::find($request->order_id);
        $order = $order->load('products','products.promotions');
        $this->totalOrder($order);
        $order->save();

        

        return response()->json([
            'status'=> true,
            'order' => $order,
            '$request' => $request->all()
        ]);
    }


    public function totalOrder($order){
        $total = 0;
      
        foreach ($order->products as  $product) {
            if(count($product->promotions)){
                $total += ($product->pivot->qty * $product->promotions[0]->price);
            }else{
                $total += ($product->pivot->qty * $product->price);
            }
           foreach ($product->pivot->options as $key => $option) {
            if($option->pivot->qty < $option->limiter){
                $total += ($option->pivot->qty * $option->price);
            }else{
                $total += ($option->price + $option->limiter_price) * ($option->pivot->qty - $option->limiter);
            }
           }
        }

        foreach ($order->products as  $product) {
            $product->pivot->options;
        }

        $order->total_cost = $total;
    }

    public function currentOrder(Request $request){
        $tittleOrder = Setting::where('id', 37)->get();
        $fecha = date("Y-m");
        $mes= "";
        $orders = Order::where('status','!=' ,'Completado')
                    ->where('status','!=' ,'Carrito')
                    ->where('user_id', $request->user_id)
                    ->with('store','repartidor')
                    ->get();
        foreach ($orders as $key => $order) {
            $order->timeLine = $this->setTimeLine($order->status);
           
        }
        return response()->json([
            'status'=> true,
            'orders' => $orders,
        ]);
        
    }

    public function setTimeLine($status){
        switch ($status) {
            case 'Pendiente':
                $collection = collect([
                    ['title'=> 'Pendiente', 'description'=> 'En preparación', 'circleColor'=> '#e61d72', 'lineColor'=> '#e61d72'],
                    ['title'=> 'En Camino', 'description'=> 'Tu pedido esta en camino'],
                    ['title'=> 'Entregado', 'description'=> 'Pedido entregado'],
                ]);
            break;
            case 'En Camino':
                $collection = collect([
                    ['title'=> 'Pendiente', 'description'=> 'En preparación', 'circleColor'=> '#e61d72', 'lineColor'=> '#e61d72'],
                    ['title'=> 'En Camino', 'description'=> 'Tu pedido esta en camino','circleColor'=> '#e61d72', 'lineColor'=> '#e61d72'],
                    ['title'=> 'Entregado', 'description'=> 'Pedido entregado'],
                ]);
            break;
            case 'Entregado':
                $collection = collect([
                    ['title'=> 'Pendiente', 'description'=> 'En preparación', 'circleColor'=> '#e61d72', 'lineColor'=> '#e61d72'],
                    ['title'=> 'En Camino', 'description'=> 'Tu pedido esta en camino','circleColor'=> '#e61d72', 'lineColor'=> '#e61d72'],
                    ['title'=> 'Entregado', 'description'=> 'Pedido entregado','circleColor'=> '#e61d72', 'lineColor'=> '#e61d72'],
                ]);
            break;
            default:
            $collection = collect([
                ['title'=> 'Pendiente', 'description'=> 'En preparación'],
                ['title'=> 'En Camino', 'description'=> 'Tu pedido esta en camino'],
                ['title'=> 'Entregado', 'description'=> 'Pedido entregado'],
            ]);
            break;
        }
        
        return $collection;
    }

}
