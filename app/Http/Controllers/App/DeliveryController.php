<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Store;
use App\DeliveryOrder;
use App\Order;

class DeliveryController extends Controller
{
    public function index(Request $request){
        $orders = Order::where('status','Pendiente')->with('store')->get();
        return response()->json([
            'status'=>true,
            'orders' => $orders
        ]);
    }
    public function show(Request $request){
        $delivery = DeliveryOrder::where('user_id',$request->user_id)
            ->where('status', 'En Camino')
            ->with('order','order.user','order.store','user')
            ->first();
        return response()->json([
            'status'=>true,
            'delivery' => $delivery
        ]);
    } 

    public function accept(Request $request){
        if(DeliveryOrder::where('user_id',$request->user_id)->where('status','En Camino')->count() > 0){
            return response()->json([
              'status'=>false,
              'title' =>'Ups! Ya tienes un pedido pendiente de entrega',
              'msgs'=> ['Sólo puedes tener un pedido en entrega']
              ]);
        }else{
                $order = Order::find($request->order_id);
                $order->status = 'En Camino';
                $order->save();

                $delivery = new DeliveryOrder();
                $delivery->user_id = $request->user_id;
                $delivery->order_id = $order->id;
                $delivery->status = 'En Camino';
                $delivery->save();

                return response()->json([
                    'status'=>true,
                    'delivery' => $delivery
                ]);
        }
    }
    public function confirm(Request $request){
        $delivery = DeliveryOrder::find($request->delivery_id);
        $delivery->status = 'Completa';
        $delivery->save();

        $order = Order::find($delivery->order_id);
        $order->status = 'Entregado';
        $order->save();

        return response()->json([
            'status'=>true,
            'delivery' => []
        ]);
    }
    public function user(Request $request){
        $deliveries = DeliveryOrder::where('user_id',$request->user_id)
            ->with('order','order.user','order.store','user')
            ->get();
        return response()->json([
            'status'=>true,
            'deliveries' => $deliveries
        ]);
    } 
   
}
