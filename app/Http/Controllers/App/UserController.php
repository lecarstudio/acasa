<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use App\User;
use App\Address;
 use Storage;
class UserController extends Controller
{
    public function update(Request $request){
        $customAttributes  = [
            'email' => '"Correo Electrónico"',
            'name' => '"Nombre"',
            'password' => '"Contraseña"',
        ];
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'string|min:8',
        ],[],$customAttributes);
        
        if($validator->fails()){
            return response()->json([
                'status'=>false,
                'title' =>'Ups! Ha ocurrido un error',
                'msgs'=> $validator->errors()->all()
            ]);
        }else{
            $user = User::find($request->id);
            $user->fill($request->except('password','avatar'));
            if($request->password != null)
                $user->password = Hash::make($request->password);
            $user->save();
            return response()->json([
                'status'=>true,
                'user' => $user,
                'title' =>'Datos actualizados!',
                'msgs'=> ['Tus datos se han guardado correctamente.']
            ]);
        }
    }

    public function updateAvatar (Request $request){
        $user = User::find($request->user_id);
        if ($request->hasFile('avatar')) {
            Storage::disk('public')->delete($user->avatar);
            $path = $request->file('avatar')->store('users/'.$user->id, 'public');
            $user->avatar = $path;
            $user->save();
        }
        return response()->json([
            'status'=>true,
            'title' =>'Datos actualizados!',
            'msgs'=> ['Tus datos se han guardado correctamente.']
        ]);
    }
}
      