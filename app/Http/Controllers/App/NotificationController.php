<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Notification;

class NotificationController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $notifications  = Notification::where('user_id', $request->user_id)->get();

        return response()->json([
            'status'=>true,
            'notifications' => $notifications
        ]);

    }
    
    public function view($id){
        
        $notify = Notification::find($id);
        $notify->view = true;
        $notify->save();

        return redirect($notify->url);
       
    }
}
