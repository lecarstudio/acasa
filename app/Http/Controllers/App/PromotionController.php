<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Promotion;
 
class PromotionController extends Controller
{
    public function index(){
        $promotions = Promotion::all();
        return response()->json([
            'status'=>true,
            'promotions' => $promotions->load('product')
        ]);
    }
   
}
