<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use App\User;
use App\Address;
 
class AddressController extends Controller
{

    public function address($id){
        $addresses = Address::where('user_id',$id)->get();
        return response()->json([
            'status'=>true,
            'addresses' => $addresses
        ]);
    }
    public function addAddress(Request $request){
        $customAttributes  = [
            'name' => 'Alias',
            'address' => 'Dirección',
            'latitude' => 'Ubicación',
            'references' => 'Referencias',
        ];
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'latitude' => 'required',
            'references' => 'required|string',
        ],[],$customAttributes);
        
        if($validator->fails()){
            return response()->json([
                'status'=>false,
                'title' =>'Ups! Ha ocurrido un error',
                'msgs'=> $validator->errors()->all()
            ]);
        }else{
            $address = Address::create($request->all());
            $addresses = Address::where('user_id',$request->user_id)->get();

            return response()->json([
                'status'=>true,
                'address' => $address,
                'addresses' => $addresses,
                'title' =>'Datos guardados!',
                'msgs'=> ['La dirección se han guardado correctamente.']
            ]);
        }
    }
    public function deleteAddress($id){
        $address = Address::find($id);
        $userID = $address->user_id;
        $address->delete();
        $addresses = Address::where('user_id',$userID)->get();

        return response()->json([
            'status'=>true,
            'addresses' => $addresses,
            'title' =>'Datos Eliminados!',
            'msgs'=> ['La dirección se han eliminado correctamente.']
        ]);
    }
}
      