<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Favorite;
use App\Setting;

class FavoriteController extends Controller
{
    public function index(Request $request){
        if($request->category_id != 0)
            $favourites = Favorite::where('user_id', $request->user_id)->whereHas('store', function ($q) {
                    return $q->where('category_id', request('category_id'));
            })->get();
        else
            $favourites = Favorite::where('user_id', $request->user_id)->get();

        return response()->json([
            'status'=>true,
            'favourites' => $favourites->load('store')
        ]);
    }
    

    public function store(Request $request){
        if(Favorite::where('user_id',$request->user_id)->where('store_id',$request->store_id)->count() == 0){
            $fav = Favorite::create($request->all());
            return response()->json([
                'status'=>true,
                'isFav' => true
            ]);
        }else{
            $fav = Favorite::where('user_id',$request->user_id)->where('store_id',$request->store_id)->first();
            $fav->delete();
            return response()->json([
                'status'=>true,
                'isFav' => false
            ]);
        }
        
        
       
    }

    public function destroy(Request $request){
        if(Favorite::where('id',$request->favorite_id)->count() != 0){
            $fav = Favorite::find($request->favorite_id);
            $fav->delete();
        }
        if($request->category_id != 0)
            $favourites = Favorite::where('user_id', $request->user_id)->whereHas('store', function ($q) {
                    return $q->where('category_id', request('category_id'));
            })->get();
        else
            $favourites = Favorite::where('user_id', $request->user_id)->get();

        return response()->json([
            'status'=>true,
            'favourites' => $favourites->load('store')
        ]);
    
       
    }
}
      