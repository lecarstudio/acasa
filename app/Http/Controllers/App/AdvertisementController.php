<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Advertisement;
 
class AdvertisementController extends Controller
{
    public function index($id){
        $ads = Advertisement::filter($id);
        return response()->json([
            'status'=>true,
            'ads' => $ads
        ]);
    }
   
}
