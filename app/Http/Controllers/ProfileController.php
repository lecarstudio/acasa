<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use Storage;
Use App\User;
Use App\Address;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->filesystem = config('voyager.storage.disk');
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $addresses = Address::where('user_id', Auth::User()->id)->where('type', 'Entrega')->get();
        return view('user.profile', compact('addresses'));
    }

   
    public function addAddress(Request $request)
    {
        $address = Address::create($request->all());

        $base_api = "https://sandbox.qpaypro.com/payment/tokenization/create_user_client_token_validations";

        $formData = [
            "x_login" => "visanetgt_qpay",
            "x_private_key" => 88888888888,
            "x_api_secret" =>  99999999999,
            "x_first_name" =>  Auth::user()->name,
            "x_last_name" => Auth::user()->last_name,
            "x_address" => $request->address,
            "x_city" =>   Auth::user()->city->name,
            "x_state" =>  Auth::user()->city->name,
            "x_zip" =>  78890,
            "x_country" =>  "Guatemala",
            "x_email" =>  Auth::user()->email
        ];

        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $base_api);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $formData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
 
        $resp = curl_exec($ch);
        $info = curl_getinfo($ch);
 
         //Resultado 'TOKEN GENERADO'

        $resJson = json_decode($resp);
 
        Auth::user()->user_client_id = $resJson->userClientToken->user_client_id;
        Auth::user()->save();

        return back()->with('success', 'Dirección agregada correctamente!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       // request()->validate([
      // 'name' => 'required|string|max:255',
      // 'last_name' => 'required|string|max:255',
      // 'email' => 'required|string|email',
      // 'phone' => 'required',
      // 'city_id' => 'required'
      // ]);
      $user = User::find(Auth::user()->id);
      // if ($request->hasFile('avatar')){
      //   if($user->avatar != 'users/default.png')
      //     Storage::disk($this->filesystem)->delete($user->avatar);
      //   $user->avatar = str_replace('/public','',$request->avatar->store('users',$this->filesystem));
      // }
      $user->fill($request->except('password'));
      if($request->password != null)
        $user->password = Hash::make($request->password);
      $user->save();

      return back()->with('success', 'Datos actualizados correctamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Address::destroy($id);
        return back()->with('success', 'Se eliminó correctamente!');
       
    }

    public function RegisterAddress(Request $request){
  
   
        $customAttributes  = [
            'name' => 'Nombre',
            'address' => 'Dirección',
            'latitude' => 'Latitud',
            'longitude' => 'Longitud'
        ];
            
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'address' => 'required|string|max:255',
                'latitude' => 'required|string|max:255',
                'longitude' => 'required|string|max:255'
    
            ],[],$customAttributes );
      
          if($validator->fails()){
            return response()->json([
                'status'=>false,
                'errors'=> $validator->errors()->all()
            ]);
          }else{

            $address = Address::create($request->except('password')); 
            $address->save();
            // $addresses = Address::where('user_id', Auth::User()->id)->where('type', 'Entrega')->get();
            return response()->json([
                  'status'=>true,
                  'address'=>$address
            ]);

          }
        }

    public function updateClient(Request $request){
        $user = User::find(Auth::user()->id);

        if($request->user_client_id != $user->user_client_id){
            $user = User::find($user->id);
            $user->user_client_id = $request->user_client_id;
            $user->save();
        }
    }
    
}
