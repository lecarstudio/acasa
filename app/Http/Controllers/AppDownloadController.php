<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class AppDownloadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $tittle = Setting::where('id', 31)->get();
        $subtittle = Setting::where('id', 32)->get();
        $description = Setting::where('id', 33)->get();
        $image = Setting::where('id', 34)->get();
        $slogan = Setting::where('id', 35)->get();
        return view('front.app_download', compact('tittle','subtittle','description','image','slogan'));
    }
}
