<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Order;
use Auth;
use App\Address;
use App\Setting;
use App\Store;

class ResumeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index($orderID){
        $order = Order::find($orderID);
        $store = Store::find($order->store_id);
        $hourOpen = intval(explode(':',$store->horary_open)[0]);
        $hourClose = intval(explode(':',$store->horary_close)[0]);

        $weekMap = [
            0 => 'Domingo',
            1 => 'Lunes',
            2 => 'Martes',
            3 => 'Miercoles',
            4 => 'Jueves',
            5 => 'Viernes',
            6 => 'Sabado',
        ];

        //Calcula el día
        $days = collect([]);
        $dayOfTheWeek = Carbon::now()->dayOfWeek;
        $weekMap[$dayOfTheWeek] = 'Hoy';
        if($dayOfTheWeek == 6)
            $weekMap[0] = 'Mañana';
        else
            $weekMap[$dayOfTheWeek+1] = 'Mañana';

        for ($i=0; $i < count($weekMap) ; $i++) {
            if($dayOfTheWeek == $i)
                $days->push([ 
                    'label' => $weekMap[$i],
                    'value' => Carbon::now()->toDateString(),
                    'day' => Carbon::now()->format('d')
                    ]
                );
            else
                $days->push([ 
                    'label' => $weekMap[$i],
                    'value' => Carbon::now()->next($i)->toDateString(),
                    'day' => Carbon::now()->next($i)->format('d')
                    ]
                );
        }
        //Calculas las horas
        $hoursOpen = collect([]);
        $mytime =Carbon::now()->format('H:i:s');
        $hour =Carbon::now()->format('H');
        for ($i=$hourOpen; $i < $hourClose ; $i++) { 
            $hoursOpen->push([ 
                'label' => $i.':00 - '.($i+1).':00',
                'value' => $i.':00 - '.($i+1).':00'
                ]
            );
        }


        $hours = collect([]);
        $mytime =Carbon::now()->format('H:i:s');
        $hour =Carbon::now()->format('H');
        for ($i=$hour; $i < $hourClose ; $i++) { 
            $hours->push([ 
                'label' => $i.':00 - '.($i+1).':00',
                'value' => $i.':00 - '.($i+1).':00'
                ]
            );
        }

        $addresses = Address::where('user_id', Auth::User()->id)->where('type', 'Entrega')->get();
        $setting = Setting::where('id', 12)->get();
        $costKm = Setting::where('id', 11)->get();
       
        return view('front.resume', compact('days','hours','hoursOpen','order','addresses', 'setting', 'costKm'));
    }

    

}
