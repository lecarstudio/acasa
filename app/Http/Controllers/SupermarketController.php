<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use App\Address;
use App\Store;
use App\Setting;
class SupermarketController extends Controller
{
 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($store_id){
        
        $weekMap = [
            0 => 'Domingo',
            1 => 'Lunes',
            2 => 'Martes',
            3 => 'Miercoles',
            4 => 'Jueves',
            5 => 'Viernes',
            6 => 'Sabado',
        ];

        //Calcula el día
        $days = collect([]);
        $dayOfTheWeek = Carbon::now()->dayOfWeek;
        $weekMap[$dayOfTheWeek] = 'Hoy';
        $weekMap[$dayOfTheWeek+1] = 'Mañana';
        for ($i=$dayOfTheWeek; $i < count($weekMap) ; $i++) { 
            $days->push([ 
                'label' => $weekMap[$i],
                'value' => Carbon::now()
                ->next($i)
                ->toDateString(),
                'day' => Carbon::now()
                ->next($i)->format('d')
                ]
            );
        }
        //Calculas las horas
        $hours = collect([]);
        $mytime =Carbon::now()->format('H:i:s');
        $hour =Carbon::now()->format('H');
        for ($i=$hour; $i < 24 ; $i++) { 
            $hours->push([ 
                'label' => $i.':00 - '.($i+1).':00',
                'value' => $i.':00 - '.($i+1).':00'
                ]
            );
        }
        $addresses = Address::where('user_id', Auth::User()->id)->where('type', 'Entrega')->get();
        $stores = Store::where('category_id', 6)->get();
        $tittle = Setting::where('id', 41)->get();
        $time = Setting::where('id', 42)->get();
        $cost = Setting::where('id', 43)->get();
        $costKm = Setting::where('id', 11)->get();
        return view('front.supermarket', compact('days','hours','addresses', 'stores', 'tittle', 'time', 'cost', 'store_id', 'costKm'));
    }
}
