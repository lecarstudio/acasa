<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use App\Store;
use App\Advertisement;
use Auth;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id){
        if(isset(Auth::User()->city_id)){
            $advertisements = Advertisement::where('category_id',$id)
            ->where('validity_start', '<=' ,date("Y-m-d"))
            ->where('validity_end', '>=' ,date("Y-m-d"))
            ->where('city_id', Auth::User()->city_id)
            ->get();
        }else{
            $advertisements = Advertisement::where('category_id',$id)
            ->where('validity_start', '<=' ,date("Y-m-d"))
            ->where('validity_end', '>=' ,date("Y-m-d"))
            ->get();
        }
        
        if(isset(Auth::User()->city_id)){        
            $stores = Store::where('category_id',$id)
            ->where('city_id', Auth::User()->city_id)
            ->get();
        }else{
            $stores = Store::where('category_id',$id)->get();
        }

        
        $subCategories = SubCategory::where('category_id',$id)->get();
        
        
        return view('front.categories', compact('subCategories', 'stores', 'advertisements'));
    } 
    public function subCategory($id,$category_id){
        if(isset(Auth::User()->city_id)){
            $advertisements = Advertisement::where('category_id',$category_id)
            ->where('validity_start', '<=' ,date("Y-m-d"))
            ->where('validity_end', '>=' ,date("Y-m-d"))
            ->where('city_id', Auth::User()->city_id)
            ->get();
        }else{
            $advertisements = Advertisement::where('category_id',$category_id)
            ->where('validity_start', '<=' ,date("Y-m-d"))
            ->where('validity_end', '>=' ,date("Y-m-d"))
            ->get();
        }
        
        if(isset(Auth::User()->city_id)){        
            $stores = Store::where('sub_category_id',$id)
            ->where('city_id', Auth::User()->city_id)
            ->get();
        }else{
            $stores = Store::where('sub_category_id',$id)->get();
        }
        
        $subCategories = SubCategory::where('category_id',$id)->get();
        return view('front.categories', compact('subCategories', 'stores', 'advertisements'));
    }
}
