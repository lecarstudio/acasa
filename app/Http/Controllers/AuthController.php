<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response;
Use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use Storage;

class AuthController extends Controller
{

  public function __construct()
  {
      $this->filesystem = config('voyager.storage.disk');
  }
    
    public function postLogin(Request $request)
    {
        request()->validate([
        'email' => 'required',
        'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->back();
        }
        
    }

    public function postRegistration(Request $request)
    {  
        request()->validate([
        'name' => 'required|string|max:255',
        'last_name' => 'required|string|max:255',
        'email' => 'required|string|email|unique:users',
        'password' => 'required|min:8|confirmed',
        'phone' => 'required',
        'city_id' => 'required'
        ]);
        
        $data = $request->all();

        $check = $this->create($data);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->back();
        }
    }
    
    public function dashboard()
    {
      if(Auth::check()){
        return view('dashboard');
      }
    }

 public function create(array $data)
 {
   return User::create([
     'role_id' => 2,
     'city_id' => $data['city_id'],
     'name' => $data['name'],
     'last_name' => $data['last_name'],
     'phone' => $data['phone'],
     'email' => $data['email'],
     'password' => Hash::make($data['password']),
     'remember_token' => $data['_token'],
    ]);
   }

}
