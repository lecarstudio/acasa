<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Order;
use App\Store;
use App\Setting;
use Redirect;
use Session;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status = '',$orderID = ''){
        $msg = '';
       
        $tittleOrder = Setting::where('id', 37)->get();
        $fecha = date("Y-m");
        $mes= "";
        $orders = Order::whereIn('status', ['Pendiente','Sin Pagar'])->where('user_id', Auth::User()->id)->orderBy('id', 'DESC')->get();
        $findOrders = Order::where('status', 'Completado')->where('delivery_date', 'LIKE', "%$fecha%" )->where('user_id', Auth::User()->id)->get();
        return view('front.order', compact('orders', 'findOrders', 'mes', 'tittleOrder'));

        // switch ($status) {
        //     case 'error':
        //         Session::flash('error', 'Error por favor intenta más tarde');
        //         return view('front.order', compact('orders', 'findOrders', 'mes', 'tittleOrder'));
        //     break;
        //     case 'cancel':
        //         Session::flash('error', 'Proceso cancelado por el usuario');
        //         return view('front.order', compact('orders', 'findOrders', 'mes', 'tittleOrder'));
        //     break;
        //     case 'success':
        //         $order = Order::find($orderID);
        //         $order->status = 'Pendiente';
        //         $order->save();
        //         $orders = Order::where('status', 'Pendiente')->orWhere('status', 'Sin Pagar')->where('user_id', Auth::User()->id)->orderBy('id', 'DESC')->get();
        //         $findOrders = Order::where('status', 'Completado')->where('user_id', Auth::User()->id)->where('delivery_date', 'LIKE', "%$fecha%" )->get();
        //         Session::flash('success', 'Pago completado');
        //         return view('front.order', compact('orders', 'findOrders', 'mes', 'tittleOrder'));
        //     break;
        //     default:
        //         return view('front.order', compact('orders', 'findOrders', 'mes', 'tittleOrder'));
        //     break;
        // }
    }
    public function update(Request $request)
    {
        if($request->direction == null && $request->address_id == null){
            return back()->with('error', 'Selecciona una dirección de envío.');
        }
        $customAttributes  = [
            'delivery_date' => 'Fecha de entrega',
            'delivery_hour' => 'Hora de entrega',
        ];

        request()->validate([
            'delivery_date' => 'required',
            'delivery_hour' => 'required',
        ],[],$customAttributes);
  

      if($request->phone == null){
        return back()->with('error', 'Selecciona un teléfono.');
        }
    
      $order = Order::find($request->id);
      $order->fill($request->except('delivery-hour','x_exp_month','x_exp_year',
      'cc_cvv2','x_card_type'));
      $order->status = 'Sin Pagar';
      $order->save();

        $paymentToken = $this->generateToken($order->id);

        $url = 'https://sandbox.qpaypro.com/payment/store?token='.$paymentToken;
        return Redirect::to($url);

    //   return view('front.payment', compact('paymentToken'));

    }

    public function completePayment(Request $request){
        // $order = Order::find($orderID);
        // $paymentToken = $this->generateToken($orderID);
        // // dd($paymentToken);

        // $url = 'https://sandbox.qpaypro.com/payment/store?token='.$paymentToken;
        // return Redirect::to($url);


        if($request->direction == null && $request->address_id == null){
            return back()->with('error', 'Selecciona una dirección de envío.');
        }
        $customAttributes  = [
            'delivery_date' => 'Fecha de entrega',
            'delivery_hour' => 'Hora de entrega',
        ];

        request()->validate([
            'delivery_date' => 'required',
            'delivery_hour' => 'required',
        ],[],$customAttributes);
  

      if($request->phone == null){
        return back()->with('error', 'Selecciona un teléfono.');
        }
    
        $order = Order::find($request->order_id);
        $order->fill($request->except('delivery_hour, cc_number'));
        $order->status = 'Sin Pagar';
        $order->type = 'Pedido';
        $order->save();



        if($order->type == 'Mandado'){
            $products[] = Array('Costo del mandado', $order->total_cost, '', '1', $order->total_cost, $order->total_cost);

        }elseif($order->type == 'Super'){
            $products[] = Array('Costo del super', $order->total_cost, '', '1', $order->total_cost, $order->total_cost);
        }else{
            $order->type = 'Pedido';
            $products = [];
        foreach ($order->products as $key => $product) {
            $products[] = Array($product->name, $product->id, '', $product->pivot->qty, $product->price, $product->price * $product->pivot->qty);
            foreach ($product->pivot->options as $key => $option) {
                $products[] = Array($option->name, $option->id, '', $option->pivot->qty, $option->price, $option->price * $option->pivot->qty);

            }
        }
        }

        //Creación de productos:
 
        $products[] = Array('Propina', $order->tip, '', '1', $order->tip, $order->tip);

        $base_api = "https://sandbox.qpaypro.com/payment/api_v1";

        $testMode = true;
        $sessionID = uniqid();
        $orgID = $testMode ? '1snn5n9w' : 'k8vif92e';
        $mechantID = 'visanetgt_qpay';

        if(isset($_GET["fingerId"])){
        $fingerId = $_GET["fingerId"];
        }else{
        $fingerId = '';
        }

        $array = [
        "x_login"=> "visanetgt_qpay",
        "x_private_key"=> "88888888888",
        "x_api_secret"=> "99999999999",
        "x_product_id"=> "1",
        "x_audit_number"=> "20",
        "x_fp_sequence"=> "20",
        "x_fp_timestamp"=> "154681351",
        "x_invoice_num"=> "001",
        "x_currency_code"=> "GTQ",
        "x_amount"=> $order->total_cost,
        "x_line_item"=>  json_encode($products),
        "x_freight"=> "0",
        "x_email"=> "email@tuempresa.com",
        "cc_number"=> $request->cc_number,
        "cc_exp"=> $request->x_exp_month.'/'. $request->x_exp_year,
        "cc_cvv2"=> $request->cc_cvv2,
        "cc_name"=> "Pedro Quino",
        "x_first_name"=> "Pedro",
        "x_last_name"=> "Quino",
        "x_company"=> "1234567-8",
        "x_address"=> "1 calle 2 ave",
        "x_city"=> "Guatemala",
        "x_state"=> "Guatemala",
        "x_country"=> "Guatemala",
        "x_zip"=> "01011",
        "x_relay_response"=> "none",
        "x_relay_url"=> "none",
        "x_type"=> "AUTH_ONLY",
        "x_method"=> "CC",
        "http_origin"=> "tuempresa.com",
        "cc_type"=> $request->x_card_type == '001' ? 'visa' : 'mastercard',
        "visaencuotas"=> 0,
        "device_fingerprint_id"=> $sessionID,
        "finger"=>$fingerId
        ];


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $base_api);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $array);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $resp = curl_exec($ch);

        $info = curl_getinfo($ch);
        $resp = strstr($resp, "{", false);
        $json = json_decode($resp);

       
        $tittleOrder = Setting::where('id', 37)->get();
        $fecha = date("Y-m");
        $mes= "";

        switch ($json->title) {
            case 'Error':
                Session::flash('error', 'Error por favor intenta más tarde');
                return view('front.order', compact('orders', 'findOrders', 'mes', 'tittleOrder'));
            break;
            case 'Cancel':
                Session::flash('error', 'Proceso cancelado por el usuario');
                return view('front.order', compact('orders', 'findOrders', 'mes', 'tittleOrder'));
            break;
            case 'Éxito':
                $order->status = 'Pendiente';
                $order->save();
                $orders = Order::where('status', 'Pendiente')->orWhere('status', 'Sin Pagar')->where('user_id', Auth::User()->id)->orderBy('id', 'DESC')->get();
                $findOrders = Order::where('status', 'Completado')->where('user_id', Auth::User()->id)->where('delivery_date', 'LIKE', "%$fecha%" )->get();
                Session::flash('success', 'Pago completado');
                return view('front.order', compact('orders', 'findOrders', 'mes', 'tittleOrder'));
            break;
            default:
                return view('front.order', compact('orders', 'findOrders', 'mes', 'tittleOrder'));
            break;
        }
    }

    public function findOrder($fecha, $mes){
        $tittleOrder = Setting::where('id', 37)->get();
        $orders = Order::whereIn('status', ['Pendiente','Sin Pagar'])->where('user_id', Auth::User()->id)->orderBy('id', 'DESC')->get();
        $findOrders = Order::where('status', 'Completado')->where('user_id', Auth::User()->id)->where('delivery_date', 'LIKE', "%$fecha%" )->get();
        return view('front.order', compact('orders', 'findOrders', 'mes', 'tittleOrder'));
    }

    public function addOrder(Request $request)
    {
        if(isset($request->store_id)){

            $store = Store::find($request->store_id);
            if(!($store->horary_open <= date('H:i:s') &&
                $store->horary_close >= date('H:i:s'))){
                return back()->with('error', 'Ups! Lo sentimos el supermercado esta cerrado.');
            }
        }

        if(isset($request->address_start_id)){
            $horaryOpen = Setting::where('id', 45)->get();
            $horaryClose = Setting::where('id', 46)->get();
            
            if(!($horaryOpen[0]->value <= date('H:i:s') &&
                $horaryClose[0]->value >= date('H:i:s'))){
                return back()->with('error', 'Ups! Lo sentimos no pueden hacerse mandados en este horario.');
            }
        }

        
        $customAttributes  = [
            'delivery_date' => 'Fecha de entrega',
            'delivery_hour' => 'Hora de entrega',
        ];

        request()->validate([
            'delivery_date' => 'required',
            'delivery_hour' => 'required',
        ],[],$customAttributes);
  

        if($request->phone == null){
        return back()->with('error', 'Selecciona un teléfono.');
        }

        $order = Order::create($request->all());


        $orderU = Order::find($order->id);
        $orderU->fill($request->except('delivery-hour'));
        $orderU->status = 'Sin Pagar';
        $orderU->user_id = Auth::User()->id;
        $orderU->save();

        $paymentToken = $this->generateToken($orderU->id);

        $url = 'https://sandbox.qpaypro.com/payment/store?token='.$paymentToken;
        return Redirect::to($url);
        // return redirect('order')->with('success', 'Order agregada correctamente!');
    }


    public function generateToken($order_id){
        $order = Order::find($order_id);

        if($order->type == 'Mandado'){
            $products[] = Array('Costo del mandado', $order->total_cost, '', '1', $order->total_cost, $order->total_cost);
        }elseif($order->type == 'Super'){
            $products[] = Array('Costo del super', $order->total_cost, '', '1', $order->total_cost, $order->total_cost);
        }else{
            $products = [];
        foreach ($order->products as $key => $product) {
            $products[] = Array($product->name, $product->id, '', $product->pivot->qty, $product->price, $product->price * $product->pivot->qty);
            foreach ($product->pivot->options as $key => $option) {
                $products[] = Array($option->name, $option->id, '', $option->pivot->qty, $option->price, $option->price * $option->pivot->qty);

            }
        }
        }
        

        //Creación de productos:
 
 
        $products[] = Array('Propina', $order->tip, '', '1', $order->tip, $order->tip);
 
         // Configuración de campos personalizados
 
         $customFields = Array('idSistema'=>'1009','idCliente'=>'2025','numerodeorden'=>'2585');
 
         //URL de sandbox para prueba                        
        $base_api = "https://sandbox.qpaypro.com/payment/register_transaction_store";
 
        $http_origin = "https://sandbox.qpaypro.com/";
 
         //Estructura de parámetros al servicio                          
 
        $requestSend =
 
        [
 
             "x_login" =>  'visanetgt_qpay',
 
             "x_api_key" => '88888888888',
 
             "x_amount" => $order->total_cost + $order->dalivery_cost,
 
             "x_currency_code" => 'GTQ',
 
             "x_first_name" => $order->user->name,
 
             "x_last_name" => $order->user->last_name,
 
             "x_phone" => $order->user->phone,
 
             "x_ship_to_address" => $order->address->address,
 
             "x_ship_to_city" => $order->user->city->name,
 
             "x_ship_to_country" => 'Guatemala',
 
             "x_ship_to_state" => '',
 
             "x_ship_to_zip" => '',
 
             "x_ship_to_phone" =>'',
 
             "x_description" => "Order number:".$order->id,
 
             "x_url_success" => 'http://acasa.desarrollo/order/success/'.$order->id,
 
             "x_url_error" => 'http://acasa.desarrollo/order/error/'.$order->id,
 
             "x_url_cancel" =>'http://acasa.desarrollo/order/cancel/'.$order->id,
 
             "http_origin" =>"http://acasa.desarrollo/",
 
             "x_company" =>  isset($order->store->name) ? $order->store->name : '',
 
             "x_address" => isset($order->store->user->address) ? $order->store->user->address: $order->address1->address,
 
             "x_city" => isset($order->store->city->name) ? $order->store->city->name : $order->user->city->name,
 
             "x_country" => 'Guatemala',
 
             "x_state" => '',
 
             "x_zip" => '',
 
             "products" => json_encode($products),
 
             "x_freight" => $order->dalivery_cost,
 
             "taxes" => '0',
 
             "x_email" => 'usuarioprueba@gmail.com',
 
             "x_type" => "AUTH_ONLY",
 
             "x_method" => "CC",
 
             "x_invoice_num" => '123',
 
             "custom_fields" => json_encode($customFields),
             "x_visacuotas" => 'si',
 
             "x_relay_url" => 'http://acasa.desarrollo/order/success/'.$order->id,
 
             "origen"=>"PLUGIN",
 
             "store_type" => "LOCAL"
 
        ];
 
         //Llamada al servicio                             
 
        $headerArray = array(
 
                          'Content-Type: application/json;charset=UTF-8'
 
        );
 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $base_api);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestSend);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
 
        $resp = curl_exec($ch);
        $info = curl_getinfo($ch);
 
         //Resultado 'TOKEN GENERADO'
 
        return $resp;
 
        die;
         
     }

    

}
