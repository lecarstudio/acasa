<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Store;
use App\Order;
use App\Product;
use Auth;

class DetailController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id){
        $store = Store::where('id',$id)->with('packages','packages.products','packages.products.options','packages.products.promotions','subcategory','user')->get()->first();
        // return $store->user->address;
        $images = json_decode($store->images);
        return view('front.detail_page', compact('store', 'images'));
    }
    public function addItem($id,$store){
        $product = Product::find($id);
        $order = Order::where('status','Pendiente')->get()->first();
        if($order != null){
            $order->total_cost = $product->price;
            $order->save();
        }else{
            $order = new Order();
            $order->user_id = Auth::user()->id;
            $order->total_cost = $product->price;
            $order->store_id = $store;
            $order->save();
        }

        $order->products()->attach($product->id);
        
        return redirect()->back()->with('success', 'Producto agregado exitosamente');
    }

}
