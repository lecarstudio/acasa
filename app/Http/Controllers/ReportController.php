<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserOrder;
use App\User;
use App\City;
use App\Order;
use App\Setting;

use DB;
class ReportController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function delivery(Request $request){
        $deliveries = UserOrder::all();
        $totalDelivery = 0;
        $totalOrder = 0;
        $totalMoto = 0;
        $totalTip = 0;
        $tax = Setting::where('id', 47)->get();
        return view('front.reports.delivery', compact('deliveries','totalDelivery','totalOrder','totalTip','tax','totalMoto'));
    }

    public function searchDelivery(Request $request){
        $deliveries = UserOrder::query();
        if(isset($request->name))
            $deliveries->whereHas('user', function ($q) {
                return $q->where('name', 'LIKE', '%'.request('name').'%');
            });

        if(isset($request->dateStart) && isset($request->dateEnd))
            $deliveries->whereHas('order', function ($q)  {
                return $q->whereBetween('delivery_date',[request('dateStart'),request('dateEnd')]);
            });

        if(isset($request->cost))
            $deliveries->whereHas('order', function ($q)  {
                return $q->where('dalivery_cost', request('cost'));
            });
        if(isset($request->tip))
            $deliveries->whereHas('order', function ($q)  {
                return $q->where('tip', request('tip'));
            });
        if(isset($request->km))
            $deliveries->whereHas('order', function ($q)  {
                return $q->where('km', request('km'));
            });
        if(isset($request->time))
            $deliveries->where('time_delivery',request('time'));
        if(isset($request->address))
            $deliveries->where('address', 'LIKE', '%'.request('address').'%');
        if(isset($request->status))
            $deliveries->whereHas('order', function ($q)  {
                return $q->where('status', request('status'));
            });
        if(isset($request->type))
            $deliveries->whereHas('order', function ($q)  {
                return $q->where('type', request('type'));
            });
        if(isset($request->direction))
            $deliveries->whereHas('user', function ($q)  {
                return $q->where('address', 'LIKE', '%'.request('direction').'%');
            });
        if(isset($request->enrollment))
            $deliveries->whereHas('user', function ($q)  {
                return $q->where('enrollment', 'LIKE', '%'.request('enrollment').'%');
            });
        if(isset($request->phone))
            $deliveries->whereHas('user', function ($q)  {
                return $q->where('phone', 'LIKE', '%'.request('phone').'%');
            });
        if(isset($request->city_id))
            $deliveries->whereHas('user', function ($q)  {
                return $q->where('city_id', request('city_id'));
            });
        if(isset($request->total))
            $deliveries->whereHas('order', function ($q)  {
                return $q->where('total_cost', request('total'));
            });
        if(isset($request->last_name))
            $deliveries->whereHas('user', function ($q)  {
                return $q->where('last_name', 'LIKE', '%'.request('last_name').'%');
            });
               
        $deliveries = $deliveries->get();
        $tax = Setting::where('id', 47)->get();
        $totalDelivery = 0;
        $totalMoto = 0;
        $totalOrder = 0;
        $totalTip = 0;

        foreach ($deliveries as $key => $delivery) {
            $porcent =  $tax[0]->value / 100;
            $desc = $delivery->order->dalivery_cost * $porcent;

            $totalDelivery += $delivery->order->dalivery_cost;
            $totalOrder += $delivery->order->total_cost;
            $totalTip += $delivery->order->tip;
            if($delivery->order->status == 'Completado')
            $totalMoto += ($delivery->order->dalivery_cost - $desc) + $delivery->order->tip;
        }

        
        return view('front.reports.delivery', compact('deliveries','totalDelivery','totalOrder','totalTip','tax','totalMoto'));
    }

    public function order(Request $request){
        $orders = Order::where('type', 'Pedido')->get();
        $taxOrder = Setting::where('id', 48)->get();
        $totalDelivery = 0;
        $totalOrder = 0;
        $totalTip = 0;
        $total = 0;
        $totalAlia = 0;
        return view('front.reports.order', compact('orders','totalDelivery','totalOrder','totalTip','total','taxOrder','totalAlia'));
    }
    public function searchOrder(Request $request){
        $orders = Order::query();
        if(isset($request->client_name))
            $orders->whereHas('user', function ($q) {
                return $q->where('name', 'LIKE', '%'.request('client_name').'%');
            });
        if(isset($request->client_lastname))
        $orders->whereHas('user', function ($q) {
            return $q->where('last_name', 'LIKE', '%'.request('client_lastname').'%');
        });
        if(isset($request->destiny))
        $orders->whereHas('address', function ($q) {
            return $q->where('address', 'LIKE', '%'.request('destiny').'%');
        });
        if(isset($request->client_phone))
        $orders->whereHas('user', function ($q) {
            return $q->where('phone', request('client_phone'));
        });
        if(isset($request->status))
            $orders->where('status', request('status'));
        if(isset($request->store))
        $orders->whereHas('store', function ($q) {
            return $q->where('name', 'LIKE', '%'.request('store').'%');
        });
        if(isset($request->city_id))
            $orders->whereHas('store', function ($q)  {
            return $q->where('city_id', request('city_id'));
         });
        if(isset($request->comments))
            $orders->where('comments', 'LIKE', '%'.request('comments').'%');
        if(isset($request->total_cost))
            $orders->where('total_cost', request('total_cost'));
        if(isset($request->tip))
            $orders->where('tip', request('tip'));
        if(isset($request->delivery_name))
            $orders->whereHas('repartidor', function ($q)  {
            return $q->where('name', 'LIKE', '%'.request('delivery_name').'%');
         });
        if(isset($request->delivery_last_name))
            $orders->whereHas('repartidor', function ($q)  {
            return $q->where('last_name', 'LIKE', '%'.request('delivery_last_name').'%');
         });
         if(isset($request->delivery_phone))
            $orders->whereHas('repartidor', function ($q)  {
            return $q->where('phone', request('delivery_phone'));
         });
         if(isset($request->delivery_cost))
            $orders->where('dalivery_cost', request('delivery_cost'));
        if(isset($request->dateStart) && isset($request->dateEnd))
            $orders->whereBetween('delivery_date',[request('dateStart'),request('dateEnd')]);
        if(isset($request->final_price))
            $orders->whereRaw(('dalivery_cost+total_cost+tip='.request('final_price')));
            if(isset($request->time))
            $orders->where('delivery_hour', 'LIKE', '%'.request('time').'%');
            
        $orders = $orders->where('type', 'Pedido')->get();
        $taxOrder = Setting::where('id', 48)->get();
        $totalDelivery = 0;
        $totalOrder = 0;
        $totalTip = 0;
        $totalAlia = 0;
        $total = 0;
        foreach ($orders as $key => $order) {
            $porcentPed =  $taxOrder[0]->value / 100;     
            $descPed = $order->total_cost * $porcentPed;

            $totalDelivery += $order->dalivery_cost;
            $totalOrder += $order->total_cost;
            $totalTip += $order->tip;
            $total = $totalDelivery+$totalOrder+$totalTip;
            if($order->status == 'Completado')
            $totalAlia += $order->total_cost - $descPed;
        }

            
        return view('front.reports.order', compact('orders','totalDelivery','totalOrder','totalTip','total','taxOrder','totalAlia'));
    }
   
    public function  favor(Request $request){
        $orders = Order::where('type', 'Mandado')->get();
        $totalDelivery = 0;
        $totalOrder = 0;
        $totalTip = 0;
        $total = 0;
        return view('front.reports.favor', compact('orders','totalDelivery','totalOrder','totalTip','total'));
    }

    public function searchFavor(Request $request){
        $orders = Order::query();
        if(isset($request->client_name))
            $orders->whereHas('user', function ($q) {
                return $q->where('name', 'LIKE', '%'.request('client_name').'%');
            });
        if(isset($request->client_lastname))
            $orders->whereHas('user', function ($q) {
            return $q->where('last_name', 'LIKE', '%'.request('client_lastname').'%');
        });
        if(isset($request->start_point))
            $orders->whereHas('address1', function ($q) {
            return $q->where('address', 'LIKE', '%'.request('start_point').'%');
        });
        if(isset($request->destiny))
            $orders->whereHas('address', function ($q) {
            return $q->where('address', 'LIKE', '%'.request('destiny').'%');
        });
        if(isset($request->client_phone))
            $orders->whereHas('user', function ($q) {
            return $q->where('phone', request('client_phone'));
        });
        if(isset($request->city_id))
            $orders->whereHas('user', function ($q)  {
            return $q->where('city_id', request('city_id'));
         });
        if(isset($request->comments))
            $orders->where('comments', 'LIKE', '%'.request('comments').'%');
        if(isset($request->total_cost))
            $orders->where('total_cost', request('total_cost'));
        if(isset($request->tip))
            $orders->where('tip', request('tip'));  
        if(isset($request->status))
            $orders->where('status', request('status')); 
        if(isset($request->delivery_name))
            $orders->whereHas('repartidor', function ($q)  {
            return $q->where('name', 'LIKE', '%'.request('delivery_name').'%');
         });
        if(isset($request->delivery_last_name))
            $orders->whereHas('repartidor', function ($q)  {
            return $q->where('last_name', 'LIKE', '%'.request('delivery_last_name').'%');
         });
         if(isset($request->delivery_phone))
            $orders->whereHas('repartidor', function ($q)  {
            return $q->where('phone', request('delivery_phone'));
         });
         if(isset($request->delivery_cost))
            $orders->where('dalivery_cost', request('delivery_cost'));
        if(isset($request->dateStart) && isset($request->dateEnd))
            $orders->whereBetween('delivery_date',[request('dateStart'),request('dateEnd')]);
        if(isset($request->final_price))
            $orders->whereRaw(('dalivery_cost+total_cost+tip='.request('final_price')));
        if(isset($request->time))
            $orders->where('delivery_hour', 'LIKE', '%'.request('time').'%');
            
        $orders = $orders->where('type', 'Mandado')->get();
        $totalDelivery = 0;
        $totalOrder = 0;
        $totalTip = 0;
        $total = 0;
        foreach ($orders as $key => $order) {
            
            $totalDelivery += $order->dalivery_cost;
            $totalOrder += $order->total_cost;
            $totalTip += $order->tip;
            $total = $totalDelivery+$totalOrder+$totalTip;
        }
        return view('front.reports.favor', compact('orders','totalDelivery','totalOrder','totalTip','total'));
    }

    public function  supermarket(Request $request){
        $orders = Order::where('type', 'Super')->get();
        $totalDelivery = 0;
        $totalOrder = 0;
        $totalTip = 0;
        $total = 0;
        return view('front.reports.super', compact('orders','totalDelivery','totalOrder','totalTip','total'));
    }

    public function  searchSuper(Request $request){
        $orders = Order::query();
        if(isset($request->client_name))
            $orders->whereHas('user', function ($q) {
                return $q->where('name', 'LIKE', '%'.request('client_name').'%');
            });
        if(isset($request->client_lastname))
        $orders->whereHas('user', function ($q) {
            return $q->where('last_name', 'LIKE', '%'.request('client_lastname').'%');
        });
        if(isset($request->destiny))
        $orders->whereHas('address', function ($q) {
            return $q->where('address', 'LIKE', '%'.request('destiny').'%');
        });
        if(isset($request->client_phone))
        $orders->whereHas('user', function ($q) {
            return $q->where('phone', request('client_phone'));
        });
        if(isset($request->status))
            $orders->where('status', request('status'));
        if(isset($request->store))
        $orders->whereHas('store', function ($q) {
            return $q->where('name', 'LIKE', '%'.request('store').'%');
        });
        if(isset($request->city_id))
            $orders->whereHas('store', function ($q)  {
            return $q->where('city_id', request('city_id'));
         });
        if(isset($request->comments))
            $orders->where('comments', 'LIKE', '%'.request('comments').'%');
        if(isset($request->total_cost))
            $orders->where('total_cost', request('total_cost'));
        if(isset($request->tip))
            $orders->where('tip', request('tip'));
        if(isset($request->delivery_name))
            $orders->whereHas('repartidor', function ($q)  {
            return $q->where('name', 'LIKE', '%'.request('delivery_name').'%');
         });
        if(isset($request->delivery_last_name))
            $orders->whereHas('repartidor', function ($q)  {
            return $q->where('last_name', 'LIKE', '%'.request('delivery_last_name').'%');
         });
         if(isset($request->delivery_phone))
            $orders->whereHas('repartidor', function ($q)  {
            return $q->where('phone', request('delivery_phone'));
         });
         if(isset($request->delivery_cost))
            $orders->where('dalivery_cost', request('delivery_cost'));
        if(isset($request->dateStart) && isset($request->dateEnd))
            $orders->whereBetween('delivery_date',[request('dateStart'),request('dateEnd')]);
        if(isset($request->final_price))
            $orders->whereRaw(('dalivery_cost+total_cost+tip='.request('final_price')));
            if(isset($request->time))
            $orders->where('delivery_hour', 'LIKE', '%'.request('time').'%');
        $orders = $orders->where('type', 'Super')->get();
        $totalDelivery = 0;
        $totalOrder = 0;
        $totalTip = 0;
        $total = 0;
        foreach ($orders as $key => $order) {
            
            $totalDelivery += $order->dalivery_cost;
            $totalOrder += $order->total_cost; 
            $totalTip += $order->tip;
            $total = $totalDelivery+$totalOrder+$totalTip;
        }
        return view('front.reports.super', compact('orders','totalDelivery','totalOrder','totalTip','total'));
    }

    public function  cutDelivery(Request $request){
        $orders = Order::where('status', 'Completado')->get();
        $tax = Setting::where('id', 47)->get();
        $taxOrder =  Setting::where('id', 48)->get();
        $totalDelivery = 0;
        $totalTip = 0;
        $totalCost = 0;
        $totalAlia = 0;
        $totalMoto = 0;
        $totalAca = 0;
        return view('front.reports.cutDelivery', compact('orders','tax','totalDelivery','totalTip','totalMoto','totalAca','totalCost','taxOrder','totalAlia'));
    }
    public function searchCutDelivery(Request $request){
        $orders = Order::query();
        if(isset($request->type))
            $orders->where('type', request('type'));
           
        if(isset($request->city_id))
            $orders->whereHas('user', function ($q)  {
            return $q->where('city_id', request('city_id'));
         });
         if(isset($request->dateStart) && isset($request->dateEnd))
            $orders->whereBetween('delivery_date',[request('dateStart'),request('dateEnd')]);
            
        $orders = $orders->where('status', 'Completado')->get();
        $tax = Setting::where('id', 47)->get();
        $taxOrder = Setting::where('id', 48)->get();
        $totalDelivery = 0;
        $totalTip = 0;
        $totalCost = 0;
        $totalMoto = 0;
        $totalAlia = 0;
        $totalAca = 0;
        foreach ($orders as $key => $order) {
            $porcent =  $tax[0]->value / 100;
            $porcentPed =  $taxOrder[0]->value / 100;     
            $desc = $order->dalivery_cost * $porcent;
            $descPed = $order->total_cost * $porcentPed;

            $totalCost += $order->total_cost;
            $totalDelivery += $order->dalivery_cost;
            $totalTip += $order->tip;
            $totalMoto += ($order->dalivery_cost - $desc) + $order->tip;
            if($order->type == 'Pedido'){
                $totalAlia += $order->total_cost - $descPed;
                $totalAca += ($order->dalivery_cost * $porcent) + ($order->total_cost * $porcentPed);
            }else{
                $totalAlia += $order->total_cost;
                $totalAca += ($order->dalivery_cost * $porcent) + $order->total_cost;
            }
            
            

            
            
        }
        return view('front.reports.cutDelivery', compact('orders','tax','totalDelivery','totalTip','totalMoto','totalAca','totalCost','taxOrder','totalAca','totalAlia'));
    }
    public function  cutStore(Request $request){
        $orders = Order::where('type', 'Pedido')->where('status', 'Completado')->get();
        $tax = Setting::where('id', 48)->get();
        $totalPartner = 0;
        $costTotal = 0;
        $totalAcasa = 0;
        return view('front.reports.cutStore', compact('orders','tax','totalPartner','costTotal','totalAcasa'));
    }
    public function searchCutStore(Request $request){
        $orders = Order::query();
        if(isset($request->property_name)){
            $orders->whereHas('store', function ($q)  {
                $q->whereHas('user', function ($q2) {
                    return $q2->where('name', 'LIKE', '%'.request('property_name').'%');
                });
            });
        }
        if(isset($request->property_last_name)){
            $orders->whereHas('store', function ($q)  {
                $q->whereHas('user', function ($q2) {
                    return $q2->where('last_name', 'LIKE', '%'.request('property_last_name').'%');
                });
            });
        }
         if(isset($request->store))
         $orders->whereHas('store', function ($q)  {
             return $q->where('name', 'LIKE', '%'.request('store').'%');
         });
         if(isset($request->dateStart) && isset($request->dateEnd))
            $orders->whereBetween('delivery_date',[request('dateStart'),request('dateEnd')]);
        if(isset($request->city_id)){
            $orders->whereHas('store', function ($q)  {
                $q->whereHas('user', function ($q2) {
                    $q2->whereHas('city', function ($q3) {
                        return $q3->where('city_id', request('city_id'));
                    });
                });
            });
        }

        $orders = $orders->where('type', 'Pedido')->where('status', 'Completado')->get();
        $tax = Setting::where('id', 48)->get();
        $totalPartner = 0;
        $costTotal = 0;
        $totalAcasa = 0;

        foreach ($orders as $key => $order) {
            $porcent =  $tax[0]->value / 100;   
            $desc = $order->total_cost * $porcent;
            $costTotal += $order->total_cost;
            $totalPartner += $order->total_cost - $desc;
            $totalAcasa += $order->total_cost * $porcent;
        }
        return view('front.reports.cutStore', compact('orders','tax','costTotal','totalPartner','totalAcasa'));
    }
    public function  cutFavor(Request $request){
        $orders = Order::where('type', '!=', 'Pedido')->where('status', 'Completado')->get();
        $totalEnvio = 0;
        $totalAca = 0;
        return view('front.reports.cutFavor', compact('orders','totalEnvio','totalAca'));
    }
    public function searchCutFavor(Request $request){
        $orders = Order::query();
        if(isset($request->dateStart) && isset($request->dateEnd))
            $orders->whereBetween('delivery_date',[request('dateStart'),request('dateEnd')]);
        $orders = $orders->where('type', '!=', 'Pedido')->where('status', 'Completado')->get();
        $totalEnvio = 0;
        $totalAca = 0;
        foreach ($orders as $key => $order) {
            $totalEnvio += $order->total_cost;
            $totalAca += $order->total_cost;

            
        }
        return view('front.reports.cutFavor', compact('orders', 'totalEnvio','totalAca'));
    }
}
