<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;

class ListController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index(Request $request){
        
        $stores = Store::where('city_id', $request->city_id)->get();
        // dd($products);
        return view('front.list', compact('stores'));
    }
}
