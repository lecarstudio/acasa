<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use App\Address;
use App\Setting;
class FavorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id){
        
        $weekMap = [
            0 => 'Domingo',
            1 => 'Lunes',
            2 => 'Martes',
            3 => 'Miercoles',
            4 => 'Jueves',
            5 => 'Viernes',
            6 => 'Sabado',
        ];

        //Calcula el día
        $days = collect([]);
        $dayOfTheWeek = Carbon::now()->dayOfWeek;
        $weekMap[$dayOfTheWeek] = 'Hoy';
        $weekMap[$dayOfTheWeek+1] = 'Mañana';
        for ($i=$dayOfTheWeek; $i < count($weekMap) ; $i++) { 
            $days->push([ 
                'label' => $weekMap[$i],
                'value' => Carbon::now()
                ->next($i)
                ->toDateString(),
                'day' => Carbon::now()
                ->next($i)->format('d')
                ]
            );
        }
        //Calculas las horas
        $hours = collect([]);
        $mytime =Carbon::now()->format('H:i:s');
        $hour =Carbon::now()->format('H');
        for ($i=8; $i < 23 ; $i++) { 
            $hours->push([ 
                'label' => $i.':00 - '.($i+1).':00',
                'value' => $i.':00 - '.($i+1).':00'
                ]
            );
        }
        $addresses = Address::where('user_id', Auth::User()->id)->where('type', 'Entrega')->get();
        $addressStarts = Address::where('user_id', Auth::User()->id)->where('type', 'Partida')->get();
        $tittle = Setting::where('id', 38)->get();
        $time = Setting::where('id', 39)->get();
        $cost = Setting::where('id', 40)->get();
        $costKm = Setting::where('id', 11)->get();
        return view('front.favor', compact('days','hours','addresses', 'addressStarts', 'tittle', 'time', 'cost', 'costKm'));
    }
}
