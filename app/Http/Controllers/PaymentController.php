<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Redirect;

class PaymentController extends Controller
{
   
    public function payment($order_id){
        $order = Order::find($order_id);
        $paymentToken = $this->generateToken($order->id);
        $url = 'https://sandbox.qpaypro.com/payment/store?token='.$paymentToken;
        return Redirect::to($url);
    }

    public function generateToken($order_id){
        $order = Order::find($order_id);

        if($order->type == 'Mandado'){
            $products[] = Array('Costo del mandado', $order->total_cost, '', '1', $order->total_cost, $order->total_cost);
        }elseif($order->type == 'Super'){
            $products[] = Array('Costo del super', $order->total_cost, '', '1', $order->total_cost, $order->total_cost);
        }else{
            $products = [];
        foreach ($order->products as $key => $product) {
            $products[] = Array($product->name, $product->id, '', $product->pivot->qty, $product->price, $product->price * $product->pivot->qty);
            foreach ($product->pivot->options as $key => $option) {
                $products[] = Array($option->name, $option->id, '', $option->pivot->qty, $option->price, $option->price * $option->pivot->qty);

            }
        }
        }
        

        //Creación de productos:
 
 
        $products[] = Array('Propina', $order->tip, '', '1', $order->tip, $order->tip);
 
         // Configuración de campos personalizados
 
         $customFields = Array('idSistema'=>'1009','idCliente'=>'2025','numerodeorden'=>'2585');
 
         //URL de sandbox para prueba                        
        $base_api = "https://sandbox.qpaypro.com/payment/register_transaction_store";
 
        $http_origin = "https://sandbox.qpaypro.com/";
 
         //Estructura de parámetros al servicio                          
 
        $requestSend =
 
        [
 
            "x_login" =>  'visanetgt_qpay',
 
            "x_api_key" => '88888888888',
 
             "x_amount" => $order->total_cost + $order->dalivery_cost,
 
             "x_currency_code" => 'GTQ',
 
             "x_first_name" => $order->user->name,
 
             "x_last_name" => $order->user->last_name,
 
             "x_phone" => $order->user->phone,
 
             "x_ship_to_address" => $order->address->address,
 
             "x_ship_to_city" => $order->user->city->name,
 
             "x_ship_to_country" => 'Guatemala',
 
             "x_ship_to_state" => '',
 
             "x_ship_to_zip" => '',
 
             "x_ship_to_phone" =>'',
 
             "x_description" => "Order number:".$order->id,
 
             "x_url_success" => 'http://acasa.desarrollo/order/success/'.$order->id,
 
             "x_url_error" => 'http://acasa.desarrollo/order/error/'.$order->id,
 
             "x_url_cancel" =>'http://acasa.desarrollo/order/cancel/'.$order->id,
 
             "http_origin" =>"http://acasa.desarrollo/",
 
             "x_company" =>  isset($order->store->name) ? $order->store->name : '',
 
             "x_address" => isset($order->store->user->address) ? $order->store->user->address: $order->address1->address,
 
             "x_city" => isset($order->store->city->name) ? $order->store->city->name : $order->user->city->name,
 
             "x_country" => 'Guatemala',
 
             "x_state" => '',
 
             "x_zip" => '',
 
             "products" => json_encode($products),
 
             "x_freight" => $order->dalivery_cost,
 
             "taxes" => '0',
 
             "x_email" => 'usuarioprueba@gmail.com',
 
             "x_type" => "AUTH_ONLY",
 
             "x_method" => "CC",
 
             "x_invoice_num" => '123',
 
             "custom_fields" => json_encode($customFields),
             "x_visacuotas" => 'si',
 
             "x_relay_url" => 'http://acasa.desarrollo/order/success/'.$order->id,
 
             "origen"=>"PLUGIN",
 
             "store_type" => "LOCAL"
 
        ];
 
         //Llamada al servicio                             
 
        $headerArray = array(
 
                          'Content-Type: application/json;charset=UTF-8'
 
        );
 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $base_api);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestSend);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
 
        $resp = curl_exec($ch);
        $info = curl_getinfo($ch);
 
         //Resultado 'TOKEN GENERADO'
 
        return $resp;
 
        die;
         
     }
}
