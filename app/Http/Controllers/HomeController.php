<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Advertisement;
use App\Store;
use App\Promotion;
use App\City;
use App\Bank;
use App\Setting;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $banks = Bank::all();
        $categories = Category::orderBy('id')->get();
       
        if(isset(Auth::User()->city_id)){
            $advertisements = Advertisement::validatesByCity();
        }else{
            $advertisements = Advertisement::validates();
        }
        
        if(isset(Auth::User()->city_id)){
            $stores = Store::where('top',1)->where('city_id', Auth::User()->city_id)->get();
        }else{
            $stores = Store::where('top',1)->get();
        }

        if(isset(Auth::User()->city_id)){
            $promotions = Promotion::validatesByCity();
        }else{
            $promotions = Promotion::validates();
        }
        
        $tittlefinder = Setting::where('id', 14)->get();
        $sloganfinder = Setting::where('id', 15)->get();
        $textfinder = Setting::where('id', 16)->get();
        $imageHome = str_replace('\\','/',Setting::where('id', 17)->get()[0]->value);
        $topTittleStore = Setting::where('id', 18)->get(); 
        $tittlePromotion = Setting::where('id', 19)->get();
        $imageApp = Setting::where('id', 20)->get();
        $tittleApp = Setting::where('id', 21)->get();
        $tittlePartner = Setting::where('id', 23)->get();
        $imagePartner = Setting::where('id', 24)->get();
        $tittleDelivery = Setting::where('id', 25)->get();
        $imageDelivery = Setting::where('id', 26)->get();
        $sloganPartner = Setting::where('id', 27)->get();
        $sloganDelivery = Setting::where('id', 28)->get();
        $bottomPartner = Setting::where('id', 29)->get();
        $bottomDelivery = Setting::where('id', 30)->get();
        return view('welcome', compact('categories', 'advertisements', 'stores', 'promotions', 'banks',
        'tittlefinder','sloganfinder','textfinder','imageHome','topTittleStore','tittlePromotion','imageApp',
        'tittleApp','tittlePartner','imagePartner','tittleDelivery','imageDelivery','sloganPartner','sloganDelivery',
        'bottomPartner','bottomDelivery'));
    }
}
