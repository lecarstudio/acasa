<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Favorite;
use App\Setting;
use Auth;
class FavoriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $tittleFavorite = Setting::where('id', 36)->get();
        $favorites = Favorite::where('user_id', Auth::User()->id)->get();
        return view('front.favorite', compact('favorites','tittleFavorite'));
    }

    public function create($store_id){

        Favorite::create([
            'user_id' => Auth::User()->id,
            'store_id' => $store_id,
           ]);
          
        return back()->with('success', 'Se agrego a favoritos correctamente!');
    }

    public function delete($id){
        Favorite::destroy($id);
        return back()->with('success', 'Se eliminó correctamente!');
    }
}
