<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notification;

class NotificationController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function view($id){
        
        $notify = Notification::find($id);
        $notify->view = true;
        $notify->save();

        return redirect($notify->url);
       
    }
}
