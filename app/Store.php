<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Store extends Model {
    public function subCategory(){
        return $this->belongsTo(SubCategory::class);
    }
    public function products(){
        return $this->hasMany(Product::class);
    }
    public function packages(){
        return $this->hasMany(Package::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function city(){
        return $this->belongsTo(City::class);
    }
}