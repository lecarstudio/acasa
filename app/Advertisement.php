<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Auth;
class Advertisement extends Model {
	
    public static function validates(){
        return Advertisement::where('status',1)
                            ->where('validity_start', '<=' ,date("Y-m-d"))
                            ->where('validity_end', '>=' ,date("Y-m-d"))
                            ->get();
    }

    public static function filter($categoryID){
        return Advertisement::where('status',1)
                            ->where('category_id', $categoryID)
                            ->where('validity_start', '<=' ,date("Y-m-d"))
                            ->where('validity_end', '>=' ,date("Y-m-d"))
                            ->get();
    }

    public static function validatesByCity(){
        return Advertisement::where('status',1)
        ->where('validity_start', '<=' ,date("Y-m-d"))
        ->where('validity_end', '>=' ,date("Y-m-d"))
        ->where('city_id', Auth::User()->city_id)
        ->get();
    }

    
}