<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    protected $fillable = [
        'user_id', 'name','description','url'
    ];
   
    public function user(){
        return $this->belongsTo(User::class);
    }
}