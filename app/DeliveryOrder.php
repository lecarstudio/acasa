<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class DeliveryOrder extends Model {

    protected $fillable = [
        'user_id', 'order_id', 'status', 'arrival_time', 'arrival_date'
    ];


    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }


}