<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class OptionOrderProduct extends Model {

    public function option(){
        return $this->belongsTo(Option::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }
}