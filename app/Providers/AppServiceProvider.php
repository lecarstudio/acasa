<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use Auth;
use App\City;
use App\Bank;
use App\Notification;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
            $cities = City::all();
            View::share('cities', $cities);
            $banks  = Bank::all();
            View::share('banks', $banks);
            if(Auth::check()){
                $notifications  = Notification::where('user_id',Auth::user()->id)->where('view',false)->get();
                View::share('notifications', $notifications);
            }
        });
    }
}
