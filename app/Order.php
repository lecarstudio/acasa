<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Order extends Model {
    protected $fillable = [
        'store_id', 
        'user_id', 
        'product_id', 
        'address_id',
        'address_start_id', 
        'card_id', 
        'comments', 
        'direction', 
        'delivery_date', 
        'delivery_hour',
        'dalivery_cost', 
        'payment_type',
        'phone', 
        'tip', 
        'status', 
        'total_cost',
        'type',
        'km'
    ];
    public function products(){
        return $this->belongsToMany(Product::class,'order_products')
        ->using(OrderProduct::class)
        ->withPivot('id','qty');
    }

    public function store(){
        return $this->belongsTo(Store::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function repartidor(){
        return $this->belongsToMany(User::class, 'user_orders')
        ->withPivot('time_start','time_end');
    }

    public function address(){
        return $this->belongsTo(Address::class);
    } 

    public function address1(){
        return $this->belongsTo(Address::class, 'address_start_id');
    } 
   
    // public function productOptions(){
    //     return $this->hasManyThrough(
    //         'App\OptionOrderProduct',
    //         'App\OrderProduct',
    //         'order_id', // Foreign key on cars table...
    //         'order_product_id', // Foreign key on owners table...
    //         'id', // Local key on mechanics table...
    //         'id' // Local key on cars table...
    //     );
    // }

    
}