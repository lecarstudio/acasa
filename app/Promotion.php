<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class Promotion extends Model {
    public function product(){
        return $this->belongsTo(Product::class);
    }

    public static function validates(){
        return Promotion::where('status',1)
        ->where('validity_start', '<=' ,date("Y-m-d"))
        ->where('validity_end', '>=' ,date("Y-m-d"))
        ->get();
    }
    public static function validatesByCity(){
        return Promotion::where('status',1)
        ->where('validity_start', '<=' ,date("Y-m-d"))
        ->where('validity_end', '>=' ,date("Y-m-d"))
        ->where('city_id', Auth::User()->city_id)
        ->get();
    }
}