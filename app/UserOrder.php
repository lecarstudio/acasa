<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class UserOrder extends Model {
    protected $fillable = [
        'user_id',
        'order_id',
        'time_start',
        'time_end',
        'address',
        'latitude',
        'longitude'
    ];
    public function products(){
        return $this->belongsToMany(Product::class,'order_products')
        ->using(OrderProduct::class)
        ->withPivot('id','qty');
    }

    public function store(){
        return $this->belongsTo(Store::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    // public function repartidor(){
    //     return $this->belongsToMany(User::class);
    // }

    public function order(){
        return $this->belongsTo(Order::class);
    }

    // public function address(){
    //     return $this->belongsTo(Address::class);
    // }

    
}