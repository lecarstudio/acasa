<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'city_id', 'bank_id', 'name', 'last_name', 'phone', 'email', 'password', 'remember_token','nit','fiscal_direction' ,
        'bank_account','coin','enrollment', 'count_type', 'company', 'address', 'latitude', 'longitude', 'user_client_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function city(){
        return $this->belongsTo(City::class);
    } 
    public function bank(){
        return $this->belongsTo(Bank::class);
    } 
    public function address(){
        return $this->belongsTo(Address::class);
    } 
}
