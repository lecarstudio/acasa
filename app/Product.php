<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'store_id', 'category_id', 'name', 'image', 'price', 'stars', 'description', 'short_description', 'status','created_at','updated_at' ,
        'city_id','limit_options'
    ];

    public function store(){
        return $this->belongsTo(Store::class);
    }
    public function packages(){
        return $this->belongsToMany(Package::class);
    }
    public function options(){
        return $this->hasMany(Option::class);
    }
   
    public function subCategory(){
        return $this->belongsTo(SubCategory::class);
    }
    public function promotions(){
        return $this->hasMany(Promotion::class)->where('validity_start', '<=' ,date("Y-m-d"))
        ->where('validity_end', '>=' ,date("Y-m-d"))
        ->where('status', 1);
    }
   
}
