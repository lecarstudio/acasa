<?php

namespace App;
use Illuminate\Database\Eloquent\Relations\Pivot;

class OrderProduct extends Pivot {

    protected $table = 'order_products';

    public function options()
    {
        return $this->belongsToMany(Option::class,'option_order_products','order_product_id')->withPivot('qty','id');
    }

}


