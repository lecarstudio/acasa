export const storeModel ={
    id: 0,
    user_id: 0,
    address_store_id: '',
    name: "",
    logo: "",
    presentation_cover: "",
    cover: "",
    images: "",
    description: "",
    short_description: "",
    horary: "",
    top: 0,
    created_at: "",
    updated_at: "",
    close_horary: "",
    horary_open: Date,
    horary_close: Date,
    sub_category_id: 0,
    stars: 0,
    category_id: 0,
    packages: [],
    products: [
        {promotions:[]}
    ],
    subcategory: [],
    address_store: [],
    user:{
        address: ''
    }
}