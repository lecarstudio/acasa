import React, { useEffect, useState } from 'react'
import ReactDOM from 'react-dom';
import { storeModel } from '../models/store';
import Rating from '@material-ui/lab/Rating';
import Axios from 'axios';
import apiConfig from '../api-config';
import AlertDialog from './AlertDialog';

function OrderDetail(props) {
    const [store, setStore] = useState(storeModel);
    const [order, setOrder] = useState({
        id: 0,
        products: [],
        total_cost: 0
    });
    const [options, setOptions] = useState([]);
    const [userID, setUserID] = useState(0);
    const [openAlert, setOpenAlert] = useState(false);
    const [openProductAlert, setOpenProductAlert] = useState(false);

    const [alert, setAlert] = useState({
        title: '',
        text: ''
    })

    useEffect(() => {
        setStore(JSON.parse(props.tsid));
        setUserID(props.userid);
        if (props.userid != '-1') {
            Axios.post(`${apiConfig.urlApi}get-order`, {
                user_id: props.userid,
                store_id: JSON.parse(props.tsid).id
            })
                .then(res => {
                    
                    if(Object.keys(res.data.order).length > 0) {
                        setOrder(res.data.order);
                    }
                });
        }
    }, [])

    const addToOder = (productID, packageID) => {
        if (props.userid == '-1') {
            setOpenAlert(true);
        } else {
            Axios.post(`${apiConfig.urlApi}order`, {
                product_id: productID,
                user_id: userID,
                store_id: store.id,
                options: options,
                qty: 1
            })
                .then(res => {
                    if (res.data.status)
                        setOrder(res.data.order)
                    else {
                        setAlert({
                            title: res.data.title,
                            text: res.data.text
                        })
                        setOpenProductAlert(true);
                    }
                });
            setOptions([]);
        }
    }
    const handleChangeOption = (event) => {
        if (event.target.checked)
            setOptions([...options, event.target.value])
        else {
            setOptions(
                options.filter((item) =>
                    item !== event.target.value)
            )
        }
    };

    const handleClose = () => {
        setOpenAlert(false);
    };
    const handleSignIn = () => {
        window.open('/login', "_self");
    }

    const handleProductClose = () => {
        setOpenProductAlert(false);
    };

    const makeOrder = (e) => {
        if (order.id == 0) {
            e.preventDefault();
            setAlert({
                title: 'Ups! Sin productos',
                text: 'Agrega productos para continuar'
            })
            setOpenProductAlert(true);
        }
    }
    const removeItem = (productID) => {
        Axios.post(`${apiConfig.urlApi}order/delete`, {
            product_id: productID,
            order_id: order.id
        })
            .then(res => {
                if (res.data.status)
                    setOrder(res.data.order)
                else {
                    setAlert({
                        title: res.data.title,
                        text: res.data.text
                    })
                    setOpenProductAlert(true);
                }
            });
    }
    const removeOption = (optionID) => {
        Axios.post(`${apiConfig.urlApi}order/option/delete`, {
            id: optionID,
            order_id: order.id
        })
            .then(res => {
                if (res.data.status)
                    setOrder(res.data.order)
                else {
                    setAlert({
                        title: res.data.title,
                        text: res.data.text
                    })
                    setOpenProductAlert(true);
                }
            });
    }

    const qtyProduct = (productID, qty) => {
        Axios.post(`${apiConfig.urlApi}order/update/qty`, {
            product_id: productID,
            order_id: order.id,
            qty: qty
        })
            .then(res => {
                if (res.data.status)
                    setOrder(res.data.order)
                else {
                    setAlert({
                        title: res.data.title,
                        text: res.data.text
                    })
                    setOpenProductAlert(true);
                }
            });
    }

    const qtyOption = (optionID, qty) => {
        Axios.post(`${apiConfig.urlApi}order/option/update/qty`, {
            id: optionID,
            order_id: order.id,
            qty: qty
        })
            .then(res => {
                if (res.data.status)
                    setOrder(res.data.order)
                else {
                    setAlert({
                        title: res.data.title,
                        text: res.data.text
                    })
                    setOpenProductAlert(true);
                }
            });
    }

    const calculatePriceOption = (option) => {
        let price = 0;

        console.log(option);
        if (option.pivot.qty < option.limiter) {
            price = option.price * option.pivot.qty;
        } else {
            price = (parseFloat(option.price) + parseFloat(option.limiter_price)) * (option.pivot.qty - option.limiter);
        }
        return price

    }

    return (
        <div className="row">
            <AlertDialog
                open={openAlert}
                handleClose={handleClose}
                handleAction={handleSignIn}
                title='Inicia Sesión'
                text='Debes iniciar sesión para continuar'
                textBtn="Iniciar Sesión"
            />
            <AlertDialog
                open={openProductAlert}
                handleClose={handleProductClose}
                handleAction={handleProductClose}
                title={alert.title}
                text={alert.text}
                textBtn="Entendido"
            />
            <div className="col-sm-12">
                <div className="col-md-8">
                    <div className="box_style_2" id="main_menu">
                        <div>
                            <h2 className="inner">{store.name}
                                <a href={`/favorite/add/${store.id}`}><i class="icon-heart"></i></a>
                            </h2>
                            <hr className="more_margin" />

                            <div className="row">

                                <div className="col-md-8">
                                    <div className="rating">
                                        <Rating name="read-only" value={store.stars} readOnly />
                                    </div>
                                </div>
                            </div>


                            <div><em>{store.subcategory.name}</em></div>
                            <div><i className="icon_pin"></i>
                                {store.user.address}
                            </div>

                            <div class="location">
                                Abre a las {store.horary_open} <span class="opening">    Cierra a las  {store.horary_close}</span>
                            </div>
                        </div>


                        {
                            store.packages &&
                            store.packages.length > 0 && (
                                store.packages.map((item, index) => <div key={index}>
                                    <h3 id="main_courses">{item.name}</h3>
                                    <table className="table table-striped">
                                        <tbody>
                                            {
                                                item.products.map((product, index) =>
                                                    product.promotions.length > 0 && (
                                                        <>
                                                        <tr key={index} >
                                                            <td  className="fix-border"  style={{ width: '80%' }} className="fix-border">
                                                                <figure className="thumb_menu_list_4">

                                                                    <img className="retaurante-min2" src={`/storage/${product.image}`} alt="thumb" alt="" />

                                                                </figure>
                                                                <h5 className="black-word">{product.name}</h5>
                                                                <br />
                                                            </td>
                                                            <td  className="fix-border" style={{ width: '10%' }}>
                                                                <strong> <span className="promotion-text">{`Q ${product.price}`}</span> {`Q ${product.promotions[0].price}`}</strong>
                                                            </td>
                                                            <td className="options fix-border">
                                                                <div className="dropdown dropdown-options">
                                                                    <a href="#" className="dropdown-toggle" data-toggle="dropdown"
                                                                        aria-expanded="true"><i className="icon_plus_alt2"></i></a>
                                                                    <div className="dropdown-menu" >
                                                                        <h5 className="black-word">Tienes más opciones</h5>
                                                                        {
                                                                            product.options.map((option, index) =>
                                                                                <label key={index}>
                                                                                    <label>
                                                                                        <input onChange={handleChangeOption} type="checkbox" value={option.id} />{option.name}<span>+ Q {option.price}</span>
                                                                                    </label>
                                                                                </label>
                                                                            )
                                                                        }

                                                                        <a onClick={() => addToOder(product.id, item.id)} className="add_to_basket">Agregar al pedido</a>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                         <tr style={{borderTop:"white" ,borderBottom: "1px solid #ddd"}}>
                                                         <td colSpan={3} style={{borderTop:"white" }}>
                                                             {product.short_description}
                                                         </td>
                                                     </tr>
                                                 </>
                                                    )
                                                )
                                            }
                                            {
                                                item.products.map((product, index) =>
                                                    product.promotions.length == 0 && (
                                                        <>
                                                            <tr key={index}>
                                                                <td style={{ width: '80%' }} className="fix-border">
                                                                    <figure className="thumb_menu_list_4">
                                                                        <img className="retaurante-min2" src={`/storage/${product.image}`} alt="thumb" alt="" />
                                                                    </figure>

                                                                    <h5 className="black-word">{product.name}</h5>
                                                                    <br />
                                                                    {/* <p>
                                                                {product.short_description}
                                                            </p> */}
                                                                </td>
                                                                <td className="fix-border" style={{ width: '10%' }}>
                                                                    <strong>Q {product.price}</strong>
                                                                </td>
                                                                <td className="options fix-border" style={{ width: '10%' }}>
                                                                    <div className="dropdown dropdown-options">
                                                                        <a href="#" className="dropdown-toggle" data-toggle="dropdown"
                                                                            aria-expanded="true"><i className="icon_plus_alt2"></i></a>
                                                                        <div className="dropdown-menu" >
                                                                            <h5 className="black-word">Tienes más opciones</h5>
                                                                            {
                                                                                product.options.map((option, index) =>
                                                                                    <label key={index}>
                                                                                        <label>
                                                                                            <input onChange={handleChangeOption} type="checkbox" value={option.id} />{option.name}<span>+ Q {option.price}</span>
                                                                                        </label>
                                                                                    </label>
                                                                                )
                                                                            }

                                                                            <a onClick={() => addToOder(product.id, item.id)} className="add_to_basket">Agregar al pedido</a>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr style={{borderTop:"white" ,borderBottom: "1px solid #ddd"}}>
                                                                <td colSpan={3} style={{borderTop:"white" }}>
                                                                    {product.short_description}
                                                                </td>
                                                            </tr>
                                                        </>
                                                    )
                                                )
                                            }

                                            {
                                                item.products.length == 0 && (
                                                    <li>Sin Productos</li>
                                                )
                                            }
                                        </tbody>
                                    </table>
                                </div>
                                )
                            )
                        }
                        <hr />
                    </div>
                </div>

                <div className="col-md-4" id="sidebar">
                    <div className="theiaStickySidebar">
                        <div id="cart_box">
                            <h3 className="title-cost">Costo del pedido <i className="icon_cart_alt pull-right"></i></h3>
                            <table className="table table_summary">
                                <tbody>
                                    {

                                        order.products.length > 0 && (
                                            order.products.map((product) => <>
                                                <tr key={product.id}>
                                                    <td width="25%" style={{ fontWeight: 'bold' }}>
                                                        <a onClick={() => qtyProduct(product.id, -1)} className="remove_item"><i className="icon_minus_alt"></i></a>
                                                        <strong>{` ${product.pivot.qty} `}</strong>
                                                        <a onClick={() => qtyProduct(product.id, 1)} className="remove_item"><i className="icon_plus_alt"></i></a>

                                                    </td>
                                                    <td style={{ fontWeight: 'bold' }}>
                                                        {product.name}
                                                    </td>
                                                    <td>
                                                        {
                                                            product.promotions.length > 0 ? (
                                                                <strong className="pull-right"><span className="promotion-text">{`Q ${product.price * product.pivot.qty}`}</span> {`Q ${product.promotions[0].price * product.pivot.qty}`}</strong>
                                                            ) :
                                                                <strong className="pull-right">{`Q ${product.price * product.pivot.qty}`}</strong>
                                                        }

                                                    </td>
                                                    <td><a className="pull-right" onClick={() => removeItem(product.id)} className="remove_item"><i className="icon_trash"></i></a></td>
                                                </tr>
                                                {
                                                    product.pivot.options &&
                                                    product.pivot.options.length > 0 &&
                                                    product.pivot.options.map((option) =>
                                                        <tr>
                                                            <td width="25%" style={{ width: 50, textAlign: 'end' }}>
                                                                <a onClick={() => qtyOption(option.pivot.id, -1)} className="remove_item"><i className="icon_minus_alt"></i></a>
                                                                {` ${option.pivot.qty} `}
                                                                <a onClick={() => qtyOption(option.pivot.id, 1)} className="remove_item"><i className="icon_plus_alt"></i></a>
                                                            </td>
                                                            <td>
                                                                {option.name}
                                                            </td>
                                                            <td class="pull-right">
                                                                Q{calculatePriceOption(option)}
                                                            </td>
                                                            <td>
                                                                <a onClick={() => removeOption(option.pivot.id)} className="remove_item"><i className="icon_trash"></i></a>
                                                            </td>
                                                        </tr>
                                                    )
                                                }
                                            </>
                                            ))
                                    }
                                </tbody>
                            </table>

                            <table className="table table_summary">
                                <tbody>
                                    <tr>
                                        <td className="total">
                                            TOTAL <span className="pull-right">{`Q ${order.total_cost}`}</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr />
                            <a onClick={makeOrder} className="btn_2" href={`/resume/${order.id}`}>Hacer pedido</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}
export default OrderDetail;

if (document.getElementById('orderDetail')) {
    const element = document.getElementById('orderDetail');
    const props = Object.assign({}, element.dataset)
    ReactDOM.render(<OrderDetail {...props} />, element);
}
