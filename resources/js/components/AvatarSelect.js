import React, { useRef, useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import apiConfig from '../api-config';
import InputAvatar from './InputAvatar';
import Axios from 'axios';

function AvatarSelect(props) {
    const [avatar, setAvatar] = useState(props.avatar);
    const [formData, setFormData] = useState({});

    useEffect(() => {
        setFormData({
            ...formData,
            ['user_id']: props.userid,
        });
    }, [])
    const handleChange = (event) => {
        event.stopPropagation();
        event.preventDefault();
        var file = event.target.files[0];
      
        sendForm(file);
    };

    const sendForm = (file) => {
        let formDataSend = new FormData();
        for (var key in formData) {
            formDataSend.append(key, formData[key]);
        }
        formDataSend.append('avatar', file);
        Axios.post(`${apiConfig.urlApi}user/update/avatar`, formDataSend, {
            headers: {
                "Content-Type": "multipart/form-data",
            }
        })
        .then(res => {
            console.log(res.data.msgs);
        })
    }

    return (
        <div className="center">
            <InputAvatar onChange={handleChange} default={avatar} />
        </div>

    );
}

export default AvatarSelect;

if (document.getElementById('avatarSelect')) {
    const element = document.getElementById('avatarSelect');
    const props = Object.assign({}, element.dataset);
    ReactDOM.render(<AvatarSelect   {...props}/>, element);
}
