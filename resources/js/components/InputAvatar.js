import React, { useRef, useState,useEffect } from 'react';
import Badge from '@material-ui/core/Badge';
import Avatar from '@material-ui/core/Avatar';
import { Button } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import apiConfig from '../api-config';


export default function InputAvatar(props) {

  const inputFile = useRef(null)
  const [src, setSrc] = useState('');

  useEffect(() => {
    setSrc(apiConfig.urlMedia+props.default);
  }, [props.default])

  const changeAvatar = (event) => {
    event.stopPropagation();
    event.preventDefault();
    var file = event.target.files[0];
    props.onChange(event); /// if you want to upload latter
    if (file)
      setSrc(URL.createObjectURL(file)); /// if you want to upload latter

  }

  const onButtonClick = () => {
    inputFile.current.click();
  };

  return (
    <div className="center">
      <input type="file" id="file" ref={inputFile} style={{ display: "none" }} onChange={changeAvatar} />

      <Badge
        overlap="circle"
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        badgeContent={
          <IconButton onClick={onButtonClick} style={{ backgroundColor: 'black' }}>
            <EditIcon style={{ color: 'white' }} />
          </IconButton>
        }
      >
        <Avatar src={src} className="avatar" />
      </Badge>
    </div>

  );
}