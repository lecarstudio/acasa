import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import apiConfig from '../api-config';
import Axios from 'axios';

function Payment(props) {
    const [showModal, setShowModal] = useState(false);
    const [formData, setFormData] = useState(
        {
            x_login: "visanetgt_qpay",
            x_private_key: 88888888888,
            x_api_secret: 99999999999,
            user_client_id: 419,
            cc_number: 4111111111111111,
            x_exp_month: 12,
            x_exp_year: 22,
            x_card_type: "001",
            cc_cvv2: 123
        }
    );
    useEffect(() => {
        // setFormData({
        //     ...formData,
        //     ['user_client_id']: props.userClientID,
        // });
    }, [])

    const openModal = () => {
      setShowModal(true);
    };

    const sendForm = () => {
        Axios.post(`https://sandbox.qpaypro.com/payment/tokenization/associate_card_token_user_validations`, formData,{
            headers: {
                "Content-Type": "multipart/form-data",
            }
        })
        .then(res => {
            console.log(res.data);
        })
    }

    return (<div>
        <div className="center">
           <button id="pay" onClick={openModal}  type="button" className="btn_2">Pagar</button>
        </div>

<div  id="pagosModal" className={showModal ? "modal fade in show" : "modal fade"} id="exampleModalCenter" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div className="modal-dialog" >
        <div className="modal-content modal-popup">
            <a href="#0" className="close-link" type="button"  className="close" data-dismiss="modal" aria-label="Close"><i
                    className="icon_close_alt2"></i></a>
                    <div className="col-md-6"></div>
                    <hr/> 
                    <h3 className="title-cost">Agregar método de pago</h3>
                            
                           
       
            <div className="card-body">
                    <div className="form-group">
                        <label for="name-on-Card">Nombre de la tarjeta de crédito ó el poseedor de la cuenta</label>
                        <input id="name-on-Card" type="text" className="form-control @error('name-on-Card') is-invalid @enderror"
                            name="name-on-Card"  required autocomplete="name-on-Card" autofocus placeholder="Nombre y apellidos" />
                    </div>

                    <div className="form-group">
                        <label for="card-number">Número de tarjeta</label>
                        <input id="card-number" type="text"
                            className="form-control @error('card-number') is-invalid @enderror" name="card-number"
                             required autocomplete="card-number" autofocus placeholder="Número de tarjeta" />
                    </div>

                    <div className="row">
                        <div className="col-md-6">
                            <label for="expiration-date">Fecha de expiración</label>
                            <div className="row">
                                <div className="col-md-6 col-sm-6">
                                    <div className="form-group">
                                        <input id="month" type="text"
                                        className="form-control @error('month') is-invalid @enderror" name="month"
                                        required autocomplete="month" autofocus placeholder="mm" />
                                        
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-6">
                                    <div className="form-group">
                                        <input id="expire_year" type="text"
                                        className="form-control @error('expire_year') is-invalid @enderror" name="expire_year"
                                        required autocomplete="expire_year" autofocus placeholder="yyyy" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-sm-12">
                            <div className="form-group">
                                <label for="expiration-date">Código de seguridad</label>
                                <div className="row">
                                    <div className="col-md-8 col-sm-6">
                                        <div className="form-group">
                                            <input id="ccv" type="text"
                                            className="form-control @error('ccv') is-invalid @enderror" name="ccv"
                                             required autocomplete="ccv" autofocus placeholder="CCV" />
                                        </div>
                                    </div>
                                    <div className="col-md-4 col-sm-6">
                                        <small>Ultimos 3 digitos</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <button onClick={sendForm} className="btn_1">Guardar
                    </button>
            </div>
        </div>
    </div>
</div>
    </div>
      

    );
}

export default Payment;

if (document.getElementById('paymentBtn')) {
    const element = document.getElementById('paymentBtn');
    const props = Object.assign({}, element.dataset);
    ReactDOM.render(<Payment   {...props}/>, element);
}
