@extends('layouts.app')

@section('content')
<br><br><br><br><br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">

            <!-- Content ================================================== -->

            <div id="tabs" class="tabs">
                <nav>
                    <ul>
                        <li><a href="#0" onclick="showProfile()" class="icon-profile"><span><strong>Datos del
                                        perfil</strong></span></a>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="#section-1" class=""><span></span></a>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="#0" onclick="showDirections()" class="icon-location"><span><strong>Mis
                                        direcciones</strong></span></a>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="#section-1" class=""><span></span></a>
                        </li>
                    </ul>
                    {{-- <ul>
                        <li><a href="#0" onclick="showPayments()" class="icon-tarjeta"><span><strong>Métodos de
                                        pago</strong></span></a>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="#section-1" class=""><span></span></a>
                        </li>
                    </ul> --}}
                    <ul>
                        <li><a href="#0" data-toggle="modal" data-target="#closeSession"
                                class="icon-cerrar"><span><strong>Cerrar sesión</strong></span></a>
                        </li>
                    </ul>
                    <ul>
                        <li><a href="#section-1" class=""><span></span></a>
                        </li>
                    </ul>
                </nav>
            </div>

            <!-- End Content =============================================== -->

        </div>

        <div class="col-md-8">
            <div class="content">
                <div id="profile" style="display: block;" class="wrapper_indent">
                    <hr>
                    <div class="indent_title_in">
                        <h3 class="title-cost">Mi cuenta {{auth()->user()->name}}</h3>
                    </div>
                    @include('common.msg')
                    <div class="card-body">
                        <form action="{{route('profile.update')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <div class="form-group">
                                <div>
                                    <div class="center-aling">
                                        <div id="avatarSelect"  data-avatar="{{ Auth::user() ? Auth::user()->avatar : ''}}"  data-userid="{{ Auth::user() ? Auth::user()->id : -1}}" ></div>
                                    </div>
                                </div>

                                @error('avatar')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="name">{{ __('Nombre') }}</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{auth()->user()->name}}" required autocomplete="name" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="last_name">{{ __('Apellidos') }}</label>
                                <input id="last_name" type="text"
                                    class="form-control @error('last_name') is-invalid @enderror" name="last_name"
                                    value="{{auth()->user()->last_name}}" required autocomplete="last_name" autofocus>
                                @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="email">{{ __('E-Mail') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{auth()->user()->email}}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password">{{ __('Contraseña') }}</label>
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    autocomplete="new-password" placeholder="********">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="phone">{{ __('Télefono') }}</label>
                                        <input id="phone" type="number"
                                            class="form-control @error('phone') is-invalid @enderror" name="phone"
                                            value="{{auth()->user()->phone}}" required autocomplete="phone" autofocus>
                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!--End row -->

                            <div class="form-group">
                                <label for="city_id">{{ __('Ciudad') }}</label>
                                <select class="form-control @error('city_id') is-invalid @enderror" name="city_id"
                                    required autocomplete="city_id" autofocus>
                                    @foreach($cities as $city)
                                    @if ($city->id == auth()->user()->city_id)
                                    <option value="{{auth()->user()->city_id}}" selected>{{$city->name}}</option>
                                    @else
                                    <option value="{{$city->id}}">{{$city->name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                                @error('city_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="nit">{{ __('Nit') }}</label>
                                <input id="nit" type="text" class="form-control @error('nit') is-invalid @enderror"
                                    name="nit" value="{{auth()->user()->nit}}" autocomplete="nit" autofocus>
                                @error('nit')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="fiscal_direction">{{ __('Dirección fiscal') }}</label>
                                <input id="fiscal_direction" type="text"
                                    class="form-control @error('fiscal_direction') is-invalid @enderror"
                                    name="fiscal_direction" value="{{auth()->user()->fiscal_direction}}"
                                    autocomplete="fiscal_direction" autofocus>
                                @error('fiscal_direction')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <button type="submit" class="btn_2">{{ __('Actualizar') }}
                            </button>

                        </form>
                    </div>
                </div>

                <div id="my-directions" style="display: none;" class="wrapper_indent">
                    <hr>
                    <div class="indent_title_in">
                        <h3 class="title-cost">Mis direcciones</h3>
                    </div>

                    <div class="card-body">

                        <div class="widget">
                            <div id="custom-search-input-blog">
                                <div class="input-group col-md-12">
                                    <span class="input-group-btn">
                                        <button class="btn btn-info btn-lg" type="button">
                                            <a id="directionsBtn" href="#0" data-toggle="modal"
                                                data-target="#direcionesModal">
                                                <i class="icon-direction"></i>
                                            </a>
                                        </button>
                                    </span>
                                    <input id="direction" type="text" class="form-control input-lg"
                                        placeholder="Agregar una dirección" onclick="muestraModal()">
                                </div>
                            </div>
                        </div><!-- End Search -->
                        @include('common.msg')
                        <hr>
                        <div class="form-group">
                            <table class="table" >
                                <tbody id="tablePather">
                                @foreach($addresses as $address)
                                <tr id="table1" class="table-tr">
                                    <td class="table-th-td">
                                        <i class="icon-location-1"></i>
                                    </td>
                                    <th class="table-th-td">
                                        {{$address->name}}
                                    </th>
                                    <td class="table-th-td">{{$address->address}}</td>
                                    <td class="table-th-td">
                                        <a href="/address/delete/{{$address->id}}">
                                            <i class="icon-cancel"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div id="payment-metods" style="display: none;" class="wrapper_indent">
                    <hr>
                    <div class="indent_title_in">
                        <h3 class="title-cost">Métodos de pago</h3>
                    </div>

                    <div class="card-body">
                        <div class="form-group">
                            <table class="table">
                                <tr class="table-tr">
                                    <th class="table-th-td_3">
                                        <strong>Mis tarjetas</strong>
                                    </th>
                                </tr>
                                <tr class="table-tr">
                                    <td class="table-th-td_2">
                                        Banamex
                                        ***********7290
                                    </td>
                                    <td class="table-th-td_2">
                                        <a href="#0">
                                            <i class="icon-cancel"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr class="table-tr">
                                    <td class="table-th-td_3">
                                        <a href="#0" data-toggle="modal" data-target="#pagosModal">
                                            <button type="submit" class="btn_1"><i
                                                    class="icon-plus-circled"></i>{{ __('Agregar tarjeta Crédito/Débito') }}
                                            </button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<br><br><br>

<!-- Direcciones modal -->

<div class="modal" id="direcionesModal" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 191px;">
        <div class="modal-content modal-popup">
            <a id="address-close" href="#0" class="close-link" type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                    class="icon_close_alt2"></i></a>
            <h3 class="title-cost">Mis direcciones</h3>           
            <form action="{{route('register.address')}}" method="POST" class="popup-form" id="formAddAdress">
                @csrf
                <div id="errorMsgRegisterPartner"></div>
                <div class="form-group">
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                    <input type="hidden" name="type" value="Entrega">
                    <input id="name-direction" type="text"
                        class="form-control @error('name-direction') is-invalid @enderror" name="name"
                        value="{{ old('name-direction') }}" required autocomplete="name-direction" autofocus
                        placeholder="Alias de dirección">
                    @error('name-direction')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div> 
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-9 col-sm-9">
                            <input id="latitude" type="hidden" name="latitude">
                            <input id="longitude" type="hidden" name="longitude">
                            <input  class="form-control" id="address" name="address" type="text" required/>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <span class="input-group-btn">
                                <button id="submit" type="button" class="form-control btn_2" style="color: #FFF;">Buscar</button>
                            </span>
                        </div>
                    </div>                        
                    <div id="myMap"></div>
                </div>
                <div class="form-group">
                    <input id="references" type="text"
                        class="form-control @error('references') is-invalid @enderror" name="references"
                        value="{{ old('references') }}" autocomplete="references" autofocus
                        placeholder="Referencias">
                    @error('references')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <button type="submit" class="btn_2">Guardar</button>
            </form>
        </div>
    </div>
</div><!-- End modal -->

<!-- Pagos modal -->

<div class="modal" id="pagosModal" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 191px;">
        <div class="modal-content modal-popup">
            <a href="#0" class="close-link" type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                    class="icon_close_alt2"></i></a>
            <hr>

            <h3 class="title-cost">Métodos de pago</h3>



            <div class="card-body">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="payment_select">
                        <label for="payment">
                            <input id="payment" type="radio" class="@error('payment') is-invalid @enderror"
                                name="payment" value="{{ old('credit') }}" required autocomplete="payment" autofocus>
                            Tarjeta de crédito
                        </label>
                        <i class="icon_creditcard"></i>
                    </div>

                    <div class="payment_select">
                        <label for="payment">
                            <input id="payment" type="radio" class="@error('payment') is-invalid @enderror"
                                name="payment" value="{{ old('debit') }}" required autocomplete="payment" autofocus>
                            Tarjeta de dédito
                        </label>
                        <i class="icon_creditcard"></i>
                    </div>

                    <div class="form-group">
                        <label
                            for="name-on-Card">{{ __('Nombre de la tarjeta de crédito ó el poseedor de la cuenta') }}</label>
                        <input id="name-on-Card" type="text"
                            class="form-control @error('name-on-Card') is-invalid @enderror" name="name-on-Card"
                            value="{{ old('name-on-Card') }}" required autocomplete="name-on-Card" autofocus
                            placeholder="Nombre y apellidos">
                        @error('name-on-Card')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="card-number">{{ __('Número de tarjeta') }}</label>
                        <input id="card-number" type="text"
                            class="form-control @error('card-number') is-invalid @enderror" name="card-number"
                            value="{{ old('card-number') }}" required autocomplete="card-number" autofocus
                            placeholder="Número de tarjeta">
                        @error('card-number')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label for="expiration-date">{{ __('Fecha de expiración') }}</label>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <input id="month" type="text"
                                            class="form-control @error('month') is-invalid @enderror" name="month"
                                            value="{{ old('month') }}" required autocomplete="month" autofocus
                                            placeholder="mm">

                                    </div>
                                    @error('month')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <input id="expire_year" type="text"
                                            class="form-control @error('expire_year') is-invalid @enderror"
                                            name="expire_year" value="{{ old('expire_year') }}" required
                                            autocomplete="expire_year" autofocus placeholder="yyyy">
                                    </div>
                                    @error('expire_year')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="expiration-date">{{ __('Código de seguridad') }}</label>
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="form-group">
                                            <input id="ccv" type="text"
                                                class="form-control @error('ccv') is-invalid @enderror" name="ccv"
                                                value="{{ old('ccv') }}" required autocomplete="ccv" autofocus
                                                placeholder="CCV">
                                        </div>
                                        @error('ccv')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-8 col-sm-6">
                                        <img src="img/icon_ccv.gif" width="50" height="29" alt="ccv"><small>Last 3
                                            digits</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End row -->

                    <button type="submit" class="btn_2">{{ __('Guardar') }}
                    </button>

                </form>
            </div>
        </div>
    </div>
</div><!-- End modal -->

<!-- Sesion modal -->

<div class="modal" id="closeSession" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 191px;">
        <div class="modal-content modal-popup">
            <a href="#0" class="close-link" type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                    class="icon_close_alt2"></i></a>
            <hr>
            <h3 class="title-cost">Cerrar sesión</h3>
            <div class="login_icon"><i class="icon-lock"></i></div>
            <hr>
            <p>¿Seguro qué quieres
                cerrar tu sesión?</p>
            <hr>
            <a href="{{route('logout')}}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
                <div class="btn_2">Aceptar</div>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </a>
            <br>
            <a href="#0" data-dismiss="modal" aria-label="Close">
                <div class="btn_2">Cancelar</div>
            </a>



        </div>
    </div>
</div><!-- End modal -->

<script>
    function muestraModal() {
        $('#directionsBtn').trigger('click');
    }

    function showDirections() {
        $('#profile').hide();
        $('#my-directions').show();
        $('#payment-metods').hide();
    }

    function showProfile() {
        $('#profile').show();
        $('#my-directions').hide();
        $('#payment-metods').hide();
    }

    function showPayments() {
        $('#profile').hide();
        $('#my-directions').hide();
        $('#payment-metods').show();
    }
</script>


@endsection

@section('css')
<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endsection