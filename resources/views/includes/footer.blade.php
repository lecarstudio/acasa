<!-- Footer ================================================== -->
<footer>
    <div class="container">
        <div class="row" style="padding: 32px 0;">
            <div class="col-md-4 col-sm-3"> 
                <p>
                    <img src="{{ asset('img/logofooter.png') }}" style="width: 100%;" >
                </p>
            </div> 
            <div class="col-md-3 col-sm-3">
                <ul>
                    <li>
                        <div class="high_light_2">
                              <a href="index.html"><i class="icon-apple"></i> App Store</a>
                        </div>        
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3" id="newsletter">
                <ul>
                    <li>
                        <div class="high_light_2">
                            <a href="index.html"><i class="icon-android"></i>Google Play</a>
                       </div> 
                    </li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-3">
                <div id="social_footer_2">
                    <ul>
                        <li><a href="#0"><i class="icon-facebook"></i></a></li>
                        <li><a href="#0"><i class="icon-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- End row -->
        <div class="row">
            <div class="col-md-12">
                <div id="social_footer">
                    <p>
                        <a href="/pp"> Políticas de privacidad </a> | 
                        <a href="/pp"> Términos y condiciones </a>
                        <p>
                            © Acasa 2020
                        </p>
                    </p>
                </div>
            </div>
        </div><!-- End row -->
    </div><!-- End container -->
    </footer>
    <!-- End Footer =============================================== -->

<div class="layer"></div><!-- Mobile menu overlay mask -->


<!-- Login modal -->  
<div class="modal" id="login_2" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content modal-popup">
                <a href="#" class="close-link" onclick="closeModal()"><i class="icon_close_alt2"></i></a>
                <div id="user-login">
                <div class="card-body">
                    <form action="{{route('loginApp')}}" method="POST" id="formLogin">
                        @csrf
                        <div class="login_icon"><i class="icon-user-outline"></i></div>
                        <div class="alert alert-danger" id="errorMsg" style="display: none"></div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="email" type="text" class="form-control-2 @error('email') is-invalid @enderror"
                                name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="E-mail">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control-2 @error('password') is-invalid @enderror"
                                name="password" value="{{ old('password') }}" required autocomplete="password" autofocus placeholder="password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-12">
                                <button type="submit" class="btn_2">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                        <hr>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 offset-md-12">
                                <a href="#" data-toggle="modal" data-target="#registerModal"><h5 class="black-word">¿No tienes cuenta de usuario? registrate</h5></a>
                                </div>
                            </div>
                    </form>
                </div>
                </div>

			</div>
		</div>
	</div><!-- End modal -->     

<!-- Register  modal -->   
<div class="modal" id="registerModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-popup">
            <a href="#0" onclick="showLogin()" class="close-link-2"><i class="icon-right-big"></i></a>
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
            <div class="card-body">
                <div class="indent_title_in">
                    <h3 class="title-cost">QUIERO HACER PEDIDOS</h3>
                </div>
                <form action="{{route('registerApp')}}" method="POST" id="formRegister">
                    @csrf
                    <div class="alert alert-danger" id="errorMsgRegister" style="display: none"></div>

                    <div class="form-group">
                        <label for="name">{{ __('Nombre') }}</label>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                            name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="last_name">{{ __('Apellidos') }}</label>
                        <input id="last_name" type="text"
                            class="form-control @error('last_name') is-invalid @enderror" name="last_name"
                            value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>
                        @error('last_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="email">{{ __('E-Mail') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="password">{{ __('Contraseña') }}</label>
                        <input id="password" type="password"
                            class="form-control @error('password') is-invalid @enderror" name="password"
                            required autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="password-confirm">{{ __('Confirmar Contraseña') }}</label>
                        <input id="password-confirm" type="password" class="form-control"
                            name="password_confirmation" required autocomplete="new-password">
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="phone">{{ __('Télefono') }}</label>
                                <input id="phone" type="number"
                                    class="form-control @error('phone') is-invalid @enderror" name="phone"
                                    value="{{ old('phone') }}" required autocomplete="phone" autofocus>
                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <!--End row -->
                    <div class="form-group">
                        <label for="city_id">{{ __('Ciudad') }}</label>
                        <select class="form-control @error('city_id') is-invalid @enderror"  name="city_id" required autocomplete="city_id" autofocus>
                          @foreach($cities as $city)
                            <option value="{{$city->id}}">{{$city->name}}</option>
                          @endforeach
                        </select>
                        @error('city_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 offset-md-12">
                            <div class="form-check">
                                <input class="" type="checkbox" name="conditions" id="conditions" {{ old('conditions') ? 'checked' : '' }} required>
                                <label class="form-check-label" for="conditions">
                                    {{ __('Acepto los términos y condiciones') }}
                                </label>
                            </div>
                        </div>
                    </div>
                    

                    <button type="submit" class="btn_2">{{ __('Registrarme') }}
                    </button>

                </form>
            </div>

        </div>
    </div>
</div><!-- End modal --> 

 


    <!-- COMMON SCRIPTS -->
<script src="{{ asset('js/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('js/common_scripts_min.js') }}"></script>
<script src="{{ asset('js/functions.js') }}"></script>
<!-- SPECIFIC SCRIPTS -->
<script src="{{ asset('js/jquery.flexslider.js') }}"></script>
<script type="text/javascript" charset="utf-8">
    jQuery(document).ready(function($){
      $('.flexslider').flexslider({
          touch: true,
          pauseOnAction: false,
          pauseOnHover: false
      });

      $('#formLogin').submit(function(e) {
               
            e.preventDefault(); // avoid to execute the actual submit of the form.

            var form = $(this);
            var url = form.attr('action');
            var token = $('#csrf-token-id').attr('content');

            //var fd = new FormData(form.serialize());
            var fd = new FormData(this);
            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': token},
                url: url,
                data: fd,
                contentType:false,
                processData: false,
                success: function(response)
                {
                    if(response.status){
                        location.reload();
                    }else{
                        $('#errorMsg').css('display','block');
                        $('#errorMsg').text(response.title);
                    }
                }
            });
            
        });

        $('#formRegister').submit(function(e) {
               
               e.preventDefault(); // avoid to execute the actual submit of the form.
   
               var form = $(this);
               var url = form.attr('action');
               var token = $('#csrf-token-id').attr('content');
   
               //var fd = new FormData(form.serialize());
               var fd = new FormData(this);
               $.ajax({
                   type: "POST",
                   headers: {'X-CSRF-TOKEN': token},
                   url: url,
                   data: fd,
                   contentType:false,
                   processData: false,
                   success: function(response)
                   {
                       if(response.status){
                           location.reload();
                       }else{
                           $('#errorMsgRegister').css('display','block');
                           $('#errorMsgRegister').empty();
                           $.each( response.errors, function( key, value ) {
                            $('#errorMsgRegister').append('<p>'+value+'</p>');
                           })
                        //    $('#errorMsgRegister').text(response.errors);
                       }
                   }
               });
               
           });

           $('#formRegisterPartner').submit(function(e) {
               
               e.preventDefault(); // avoid to execute the actual submit of the form.
   
               var form = $(this);
               var url = form.attr('action');
               var token = $('#csrf-token-id').attr('content');
   
               //var fd = new FormData(form.serialize());
               var fd = new FormData(this);
               $.ajax({
                   type: "POST",
                   headers: {'X-CSRF-TOKEN': token},
                   url: url,
                   data: fd,
                   contentType:false,
                   processData: false,
                   success: function(response)
                   {
                       if(response.status){
                           location.reload();
                       }else{
                           $('#errorMsgRegisterPartner').css('display','block');
                           $('#errorMsgRegisterPartner').empty();
                           $.each( response.errors, function( key, value ) {
                            $('#errorMsgRegisterPartner').append('<p>'+value+'</p>');
                           })
                      
                       }
                   }
               });
               
           });

    });
  </script>
  <script>
      function showPartner(){
        $('#user-login').hide();
        $('#user-partner').show();
      }
      
      function showUser(){
        $('#user-login').show();
        $('#user-partner').hide();
      }

      function showLogin(){
        $('#registerModal').modal('hide');
        $('#login_2').modal('show');
      }

      function closeModal(){
        $('#login_2').modal('hide');
      }

      function showLogin2(){
        $('#registerPartnerModal').modal('hide');
        $('#login_2').modal('show');
      }

  </script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsdjNpaH_JIEg5yZNEtpMYoh3mMRnTOA4"></script>

<script>
    jQuery(document).ready(function($){
        var map, infoWindow;

        function initialize() {
            map = new google.maps.Map(document.getElementById('myMap'), {
                center: { lat: -34.397, lng: 150.644 },
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            infoWindow = new google.maps.InfoWindow;
            var geocoder = new google.maps.Geocoder();

            var pos = {
                lat: 0,
                lng: 0
            };


            marker = new google.maps.Marker({
                map: map,
                position: pos,
                draggable: true,
            });

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {

                    pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    marker.setPosition(pos);
                    infoWindow.open(map);
                    map.setCenter(pos);


                    geocoder.geocode({ 'latLng': pos }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $('#address').val(results[0].formatted_address);
                                $('#addressForm').val(results[0].formatted_address);
                                $('#latitude').val(marker.getPosition().lat());
                                $('#longitude').val(marker.getPosition().lng());
                                infoWindow.setContent(results[0].formatted_address);
                                infoWindow.open(map, marker);
                            }
                        }
                    });



                }, function () {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
            }

            document.getElementById('submit').addEventListener('click', function () {
                geocodeAddress(geocoder, map);
            });

            function geocodeAddress(geocoder, resultsMap) {
                var address = document.getElementById('address').value;
                geocoder.geocode({ 'address': address }, function (results, status) {
                    if (status === 'OK') {
                        $('#address').val(results[0].formatted_address);
                        $('#addressForm').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        resultsMap.setCenter(results[0].geometry.location);
                        infoWindow.setContent(results[0].formatted_address);
                        marker.setPosition(results[0].geometry.location);
                        infoWindow.open(map, marker);

                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }



            google.maps.event.addListener(marker, 'dragend', function () {

                geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').val(results[0].formatted_address);
                            $('#addressForm').val(results[0].formatted_address);
                            $('#latitude').val(marker.getPosition().lat());
                            $('#longitude').val(marker.getPosition().lng());
                            infoWindow.setContent(results[0].formatted_address);
                            infoWindow.open(map, marker);
                        }
                    }
                });
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);


        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }
    });

</script>
  


</body>
</html>