<!-- Footer ================================================== -->
<footer>
    <div class="container">
        <div class="row" style="padding: 32px 0;">
            <div class="col-md-4 col-sm-3"> 
                <p>
                    <img src="{{ asset('img/logofooter.png') }}" style="width: 100%;" >
                </p> 
            </div>
            <div class="col-md-3 col-sm-3">
                <ul>
                    <li>
                        <div class="high_light_2">
                              <a href="index.html"><i class="icon-apple"></i> App Store</a>
                        </div>        
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3" id="newsletter">
                <ul>
                    <li>
                        <div class="high_light_2">
                            <a href="index.html"><i class="icon-android"></i>Google Play</a>
                       </div> 
                    </li>
                </ul>
            </div>
            <div class="col-md-2 col-sm-3">
                <div id="social_footer_2">
                    <ul>
                        <li><a href="#0"><i class="icon-facebook"></i></a></li>
                        <li><a href="#0"><i class="icon-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- End row -->
        <div class="row">
            <div class="col-md-12">
                <div id="social_footer">
                    <a href="/pp"> Políticas de privacidad </a> | 
                    <a href="/pp"> Términos y condiciones </a>
                    <p>
                        © Acasa 2020
                    </p>
                </div>
            </div>
        </div><!-- End row -->
    </div><!-- End container -->
    </footer>
    <!-- End Footer =============================================== -->

<div class="layer"></div><!-- Mobile menu overlay mask -->


<!-- Login modal -->  
<div class="modal" id="login_2" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-popup">
            <a href="#" class="close-link" onclick="closeModal()"><i class="icon_close_alt2"></i></a>
            <div id="user-login">
            <div class="card-body">
                <form action="{{route('loginApp')}}" method="POST" id="formLogin">
                    @csrf
                    <div class="login_icon"><i class="icon-user-outline"></i></div>
                    <div class="alert alert-danger" id="errorMsg" style="display: none"></div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <input id="email" type="text" class="form-control-2 @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="E-mail">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <input id="password" type="password" class="form-control-2 @error('password') is-invalid @enderror"
                            name="password" value="{{ old('password') }}" required autocomplete="password" autofocus placeholder="password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-12">
                            <button type="submit" class="btn_2">
                                {{ __('Login') }}
                            </button>

                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>
                    </div>
                    <hr>
                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-12">
                            <a href="#" data-toggle="modal" data-target="#registerModal"><h5 class="black-word">¿No tienes cuenta de usuario? registrate</h5></a>
                            </div>
                        </div>
                </form>
            </div>
            </div>

        </div>
    </div>
</div><!-- End modal -->     

<!-- Register  modal -->   
<div class="modal" id="registerModal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content modal-popup">
        <a href="#0" onclick="showLogin()" class="close-link-2"><i class="icon-right-big"></i></a>
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
        <div class="card-body">
            <div class="indent_title_in">
                <h3 class="title-cost">QUIERO HACER PEDIDOS</h3>
            </div>
            <form action="{{route('registerApp')}}" method="POST" id="formRegister">
                @csrf
                <div class="alert alert-danger" id="errorMsgRegister" style="display: none"></div>

                <div class="form-group">
                    <label for="name">{{ __('Nombre') }}</label>
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                        name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="last_name">{{ __('Apellidos') }}</label>
                    <input id="last_name" type="text"
                        class="form-control @error('last_name') is-invalid @enderror" name="last_name"
                        value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>
                    @error('last_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="email">{{ __('E-Mail') }}</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                        name="email" value="{{ old('email') }}" required autocomplete="email">

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="password">{{ __('Contraseña') }}</label>
                    <input id="password" type="password"
                        class="form-control @error('password') is-invalid @enderror" name="password"
                        required autocomplete="new-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="password-confirm">{{ __('Confirmar Contraseña') }}</label>
                    <input id="password-confirm" type="password" class="form-control"
                        name="password_confirmation" required autocomplete="new-password">
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="phone">{{ __('Télefono') }}</label>
                            <input id="phone" type="number"
                                class="form-control @error('phone') is-invalid @enderror" name="phone"
                                value="{{ old('phone') }}" required autocomplete="phone" autofocus>
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <!--End row -->
                <div class="form-group">
                    <label for="city_id">{{ __('Ciudad') }}</label>
                    <select class="form-control @error('city_id') is-invalid @enderror"  name="city_id" required autocomplete="city_id" autofocus>
                      @foreach($cities as $city)
                        <option value="{{$city->id}}">{{$city->name}}</option>
                      @endforeach
                    </select>
                    @error('city_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group row">
                    <div class="col-md-12 offset-md-12">
                        <div class="form-check">
                            <input class="" type="checkbox" name="conditions" id="conditions" {{ old('conditions') ? 'checked' : '' }} required>
                            <label class="form-check-label" for="conditions">
                                {{ __('Acepto los términos y condiciones') }}
                            </label>
                        </div>
                    </div>
                </div>
                

                <button type="submit" class="btn_2">{{ __('Registrarme') }}
                </button>

            </form>
        </div>

    </div>
</div>
</div><!-- End modal --> 

{{-- <!-- Pagos modal -->

<div class="modal" id="pagosModal" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 191px;">
        <div class="modal-content modal-popup">
            <a href="#0" class="close-link" type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                    class="icon_close_alt2"></i></a>
                    <div class="col-md-6"></div>
                    <hr>
                    <h3 class="title-cost">Agregar método de pago 2</h3>
                            
                           
       
            <div class="card-body">
                <form method="POST" action="/payment/order/{id}" id="formPay">
                    <input type="hidden" name="user_client_id" value="{{ isset(auth()->user()->user_client_id) ? auth()->user()->user_client_id : '' }}">
                    <div class="payment_select">
                        <label for="payment">
                            <input id="x_card_type" type="radio" class="@error('x_card_type') is-invalid @enderror"
                                name="x_card_type" value="001" required autocomplete="x_card_type" autofocus>
                            VISA
                        </label>
                        <i class="icon_creditcard"></i>
                    </div>

                    <div class="payment_select">
                        <label for="x_card_type">
                            <input id="x_card_type" type="radio" class="@error('x_card_type') is-invalid @enderror"
                                name="x_card_type" value="002" required autocomplete="x_card_type" autofocus>
                            MASTERCARD
                        </label>
                        <i class="icon_creditcard"></i>
                    </div>                  

                    <div class="form-group">
                        <label for="name-on-Card">{{ __('Nombre de la tarjeta de crédito ó el poseedor de la cuenta') }}</label>
                        <input id="name-on-Card" type="text" class="form-control @error('name-on-Card') is-invalid @enderror"
                            name="name-on-Card" value="{{ old('name-on-Card') }}" required autocomplete="name-on-Card" autofocus placeholder="Nombre y apellidos">
                    </div>

                    <div class="form-group">
                        <label for="card-number">{{ __('Número de tarjeta') }}</label>
                        <input id="card-number" type="text"
                            class="form-control @error('cc_number') is-invalid @enderror" name="cc_number"
                            value="{{ old('cc_number') }}" required autocomplete="card-number" autofocus placeholder="Número de tarjeta">
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label for="expiration-date">{{ __('Fecha de expiración') }}</label>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <input id="month" type="text"
                                        class="form-control @error('x_exp_month') is-invalid @enderror" name="x_exp_month"
                                        value="{{ old('x_exp_month') }}" required autocomplete="x_exp_month" autofocus placeholder="mm">
                                        
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <input id="expire_year" type="text"
                                        class="form-control @error('x_exp_year') is-invalid @enderror" name="x_exp_year"
                                        value="{{ old('x_exp_year') }}" required autocomplete="x_exp_year" autofocus placeholder="yyyy">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="expiration-date">{{ __('Código de seguridad') }}</label>
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="form-group">
                                            <input id="cc_cvv2" type="text"
                                            class="form-control @error('cc_cvv2') is-invalid @enderror" name="cc_cvv2"
                                            value="{{ old('cc_cvv2') }}" required autocomplete="cc_cvv2" autofocus placeholder="CCV">
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-6">
                                        <img src="{{ asset('img/icon_ccv.gif') }}" width="50" height="29" alt="ccv"><small>Last 3 digits</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--End row -->

                    <button type="submit" class="btn_1">{{ __('Guardar') }}
                    </button>

                </form>
            </div>
        </div>
    </div>
</div><!-- End modal --> --}}


<!-- COMMON SCRIPTS -->
<script src="{{ asset('js/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('js/common_scripts_min.js') }}"></script>
<script src="{{ asset('js/functions.js') }}"></script>
<!-- SPECIFIC SCRIPTS -->
<script src="{{ asset('js/jquery.flexslider.js') }}"></script>
<script type="text/javascript" charset="utf-8">
jQuery(document).ready(function($){
    var dateSelected = '';
    var timeSelected = '';

  $('.flexslider').flexslider({
      touch: true,
      pauseOnAction: false,
      pauseOnHover: false
  });

  $('.selectHour').click(function(){
            timeSelected = $(this).attr('data-value');
            $.each(  $('.selectHour'), function( key, element ) {
                $(element).removeClass('selected');
            })
            $(this).addClass('selected');
  });
  $('.selectDate').click(function(){
            dateSelected = $(this).attr('data-value');
            currentDate = $(this).attr('data-current');


            if(dateSelected == currentDate){
                console.log('dateSelected');

                $('#hours').css('display','block');
                $('#hoursOpen').css('display','none');

            }else{
                $('#hours').css('display','none');
                $('#hoursOpen').css('display','block');
            }
            $.each($('.selectDate'), function( key, element ) {
                $(element).removeClass('selected');
            })
            $(this).addClass('selected');
  });

  $('#saveSelectedH').click(function(){
            $('#deliveryDate').val(dateSelected);
            $('#deliveryHour').val(timeSelected);
         
  });
        
  $('#formLogin').submit(function(e) {
           
        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url = form.attr('action');
        var token = $('#csrf-token-id').attr('content');

        //var fd = new FormData(form.serialize());
        var fd = new FormData(this);
        $.ajax({
            type: "POST",
            headers: {'X-CSRF-TOKEN': token},
            url: url,
            data: fd,
            contentType:false,
            processData: false,
            success: function(response)
            {
                if(response.status){
                    location.reload();
                }else{
                    $('#errorMsg').css('display','block');
                    $('#errorMsg').text(response.title);
                }
            }
        });
        
    });

    $('#formRegister').submit(function(e) {
           
           e.preventDefault(); // avoid to execute the actual submit of the form.

           var form = $(this);
           var url = form.attr('action');
           var token = $('#csrf-token-id').attr('content');

           //var fd = new FormData(form.serialize());
           var fd = new FormData(this);
           $.ajax({
               type: "POST",
               headers: {'X-CSRF-TOKEN': token},
               url: url,
               data: fd,
               contentType:false,
               processData: false,
               success: function(response)
               {
                   if(response.status){
                       location.reload();
                   }else{
                       $('#errorMsgRegister').css('display','block');
                       $('#errorMsgRegister').empty();
                       $.each( response.errors, function( key, value ) {
                        $('#errorMsgRegister').append('<p>'+value+'</p>');
                       })
                    //    $('#errorMsgRegister').text(response.errors);
                   }
               }
           });
           
       });


       $('#formPay').submit(function(e) {
           
           e.preventDefault(); // avoid to execute the actual submit of the form.

           var form = $(this);
           var url = form.attr('action');
           var fd = new FormData(this);

           $.ajax({
               type: "POST",
               url: url,
               data: fd,
               contentType:false,
               processData: false,
               success: function(response)
               {
                   if(response){
                       console.log(response);
                   }else{
                       $('#errorMsgRegister').css('display','block');
                       $('#errorMsgRegister').empty();
                       $.each( response.errors, function( key, value ) {
                        $('#errorMsgRegister').append('<p>'+value+'</p>');
                       })
                    //    $('#errorMsgRegister').text(response.errors);
                   }
               }
           });
           
       });

});
</script>

<script>

    function showPartner(){
      $('#user-login').hide();
      $('#user-partner').show();
    }
    
    function showUser(){
      $('#user-login').show();
      $('#user-partner').hide();
    }

</script>

<script>
    function showPartner(){
      $('#user-login').hide();
      $('#user-partner').show();
    }
    
    function showUser(){
      $('#user-login').show();
      $('#user-partner').hide();
    }

    function showLogin(){
      $('#registerModal').modal('hide');
      $('#login_2').modal('show');
    }

    function closeModal(){
      $('#login_2').modal('hide');
    }

    function showLogin2(){
      $('#registerPartnerModal').modal('hide');
      $('#login_2').modal('show');
    }

</script>

<script>
    $( document ).ready(function() {
        $("#tipq5").prop("checked", true);
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsdjNpaH_JIEg5yZNEtpMYoh3mMRnTOA4"></script>

<script>
    jQuery(document).ready(function($){
        var map, infoWindow;

        function initialize() {
            map = new google.maps.Map(document.getElementById('myMap'), {
                center: { lat: -34.397, lng: 150.644 },
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            infoWindow = new google.maps.InfoWindow;
            var geocoder = new google.maps.Geocoder();

            var pos = {
                lat: 0,
                lng: 0
            };


            marker = new google.maps.Marker({
                map: map,
                position: pos,
                draggable: true,
            });

            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {

                    pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    marker.setPosition(pos);
                    infoWindow.open(map);
                    map.setCenter(pos);


                    geocoder.geocode({ 'latLng': pos }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $('#address').val(results[0].formatted_address);
                                $('#addressForm').val(results[0].formatted_address);
                                $('#latitude').val(marker.getPosition().lat());
                                $('#longitude').val(marker.getPosition().lng());
                                infoWindow.setContent(results[0].formatted_address);
                                infoWindow.open(map, marker);
                            }
                        }
                    });



                }, function () {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
            }

            document.getElementById('submit').addEventListener('click', function () {
                geocodeAddress(geocoder, map);
            });

            function geocodeAddress(geocoder, resultsMap) {
                var address = document.getElementById('address').value;
                geocoder.geocode({ 'address': address }, function (results, status) {
                    if (status === 'OK') {
                        $('#address').val(results[0].formatted_address);
                        $('#addressForm').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        resultsMap.setCenter(results[0].geometry.location);
                        infoWindow.setContent(results[0].formatted_address);
                        marker.setPosition(results[0].geometry.location);
                        infoWindow.open(map, marker);

                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }



            google.maps.event.addListener(marker, 'dragend', function () {

                geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').val(results[0].formatted_address);
                            $('#addressForm').val(results[0].formatted_address);
                            $('#latitude').val(marker.getPosition().lat());
                            $('#longitude').val(marker.getPosition().lng());
                            infoWindow.setContent(results[0].formatted_address);
                            infoWindow.open(map, marker);
                        }
                    }
                });
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);


        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }
    });

</script>
<script>
    
    $("#getStoredirection").change(function () {
        var lat1 = $(this).find(':selected').data('latitude');
        var lon1 = $(this).find(':selected').data('longitude');
        $('#latitude-store').val(lat1);
        $('#longitude-store').val(lon1);

        var lat2 = $('#latitude-destinity').val();
        var lon2 = $('#longitude-destinity').val();
        var costkm = $('#costkm').val();
        rad = function(x) {return x*Math.PI/180;}
        var R = 6378.137; //Radio de la tierra en km
        var dLat = rad( lat2 - lat1 );
        var dLong = rad( lon2 - lon1 );
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c;
        var km = d.toFixed(3);
        var total = km*costkm;
        
        console.log(lat1, lon1,lat2,lon2,costkm,km);
        $('#dalivery_cost').val(total.toFixed(2));
        $('#km').val(km);
    });


    $("#address_id").change(function () {
        
        var lat2 = $(this).find(':selected').data('latitude');
        var lon2 = $(this).find(':selected').data('longitude');
        $('#latitude-destinity').val(lat2);
        $('#longitude-destinity').val(lon2);

        var lat1 = $('#latitude-store').val();
        var lon1 = $('#longitude-store').val();
        var costkm = $('#costkm').val();
      
        rad = function(x) {return x*Math.PI/180;}
        var R = 6378.137; //Radio de la tierra en km
        var dLat = rad( lat2 - lat1 );
        var dLong = rad( lon2 - lon1 );
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c;
        var km = d.toFixed(3);
        var total = km*costkm;

        console.log(lat1, lon1,lat2,lon2,costkm,km);

        $('#dalivery_cost').val(total.toFixed(2));
        $('#km').val(km);
    });

    $('#formAddAdress').submit(function(e) {
               
               e.preventDefault(); // avoid to execute the actual submit of the form.
   
               var form = $(this);
               var url = form.attr('action');
               var token = $('#csrf-token-id').attr('content');
   
               //var fd = new FormData(form.serialize());
               var fd = new FormData(this);
               $.ajax({
                   type: "POST",
                   headers: {'X-CSRF-TOKEN': token},
                   url: url,
                   data: fd,
                   contentType:false,
                   processData: false,
                   success: function(response)
                   {
                       if(response.status){
                            // $('#table1').css('display','none');
                            // $('#table2').css('display','block');
                            // $('#table2').empty();
                            $('#tablePather').append(
                                `<tr id="table1" class="table-tr">
                                    <td class="table-th-td">
                                        <i class="icon-location-1"></i>
                                    </td>
                                    <th class="table-th-td">
                                        ${response.address.name}
                                    </th>
                                    <td class="table-th-td">${response.address.address}</td>
                                    <td class="table-th-td">
                                        <a href="/address/delete/${response.address.id}">
                                            <i class="icon-cancel"></i>
                                        </a>
                                    </td>
                                </tr>`);  
                            $("#address-close").trigger("click");
                       
                            // console.log(value.name);
                         
                       }else{
                           $('#errorMsgRegisterPartner').css('display','block');
                           $('#errorMsgRegisterPartner').empty();
                           $.each( response.errors, function( key, value ) {
                                $('#errorMsgRegisterPartner').append('<p>'+value+'</p>');
                            })
                       }
                   }
               });
               
           });
    
</script>
<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.19.1/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/7.19.1/firebase-analytics.js"></script>

<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyCEAy5xfcjIExh8i7I5WAYqqfLVN9KNSqU",
    authDomain: "acasaapp-932c4.firebaseapp.com",
    databaseURL: "https://acasaapp-932c4.firebaseio.com",
    projectId: "acasaapp-932c4",
    storageBucket: "acasaapp-932c4.appspot.com",
    messagingSenderId: "515035374538",
    appId: "1:515035374538:web:5a97254f8ac3a9ab97e533",
    measurementId: "G-RFQSG9K4LJ"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
</script>
</body>
</html>