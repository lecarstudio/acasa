<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="pizza, delivery food, fast food, sushi, take away, chinese, italian food">
    <meta name="description" content="">
    <meta name="author" content="Ansonika">
    <title>QuickFood - Quality delivery or take away food</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114"
        href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144"
        href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/menu.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('css/elegant_font/elegant_font.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fontello/css/fontello.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('css/pop_up.css') }}" rel="stylesheet">

    <!-- SPECIFIC CSS -->
    <link href="{{asset('css/blog.css')}}" rel="stylesheet">
    <link href="{{asset('css/grey.css')}}" rel="stylesheet">
	<link href="{{asset('css/admin.css')}}" rel="stylesheet">
	<link href="{{asset('css/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/dropzone.css')}}" rel="stylesheet">
    <link href="{{asset('css/carrusel.css')}}" rel="stylesheet">
    <link href="{{ asset('css/flexslider.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <!-- YOUR CUSTOM CSS -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    {{-- <script src="{{ asset('js/goglemap.js') }}" defer></script>
	<script src="https://maps.googleapis.com/maps/api/js?key={{config('googlemap')['map_apikey']}}&callback=initMap" async defer></script> --}}
</head>

<body>

    <div id="preloader">
        <img class="image-size" class="image-size" src="{{ asset('img/logo.png') }}" alt="">
        <div class="sk-spinner sk-spinner-wave" id="status">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
            
        </div>
    </div><!-- End Preload -->

    <!-- Header ================================================== -->
    <header class="header2">
        <div class="container-fluid">
            <div class="row">
                <div class="col--md-4 col-sm-4 col-xs-4">
                    <a href="/home" id="logo">
                        <img src="{{ asset('img/logo.png') }}" width="190" height="23" alt="" data-retina="true" class="hidden-xs logopage">
                        <img src="{{ asset('img/logo_mobile.png') }}" width="59" height="23" alt="" data-retina="true"
                            class="hidden-lg hidden-md hidden-sm"> 
                    </a>
                </div>
                <nav class="col--md-8 col-sm-8 col-xs-8">
                    <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu
                            mobile</span></a>
                    <div class="main-menu">
                        <div id="header_menu">
                            <img src="{{ asset('img/logo.png') }}" width="190" height="23" alt="" data-retina="true">
                        </div>
                        <a href="#" class="open_close" id="close_in"><i class="icon_close"></i></a>
                        <ul>
                            <li><a href="/home">Home</a></li>
                            <li><a href="/order">Pedidos</a></li>
                            <li><a href="/favorite">Favoritos</a></li>
                            <li class="submenu">
                                <a href="javascript:void(0);" class="show-submenu"><i class="icon-bell-3"></i><i
                                        class="icon-down-open-mini"></i></a>
                                <ul>
                                    @if (isset($notifications))
                                    @foreach ($notifications as $notify)
                                    <li><a href="{{route('notify.view',$notify->id)}}" >{{$notify->name}}</a></li>
                                    @endforeach
                                    @if (count($notifications) == 0)
                                        <li>Sin notificaciones</li>
                                    @endif
                                    @else
                                        <li>Sin notificaciones</li>
                                    @endif
                                </ul>
                            </li>
                            @auth
                            @if (Auth::user()->role_id != 2)
                                <li><a href="/admin" >Mi cuenta</a></li>  
                            @else
                                <li><a href="{{route('profile')}}" >Mi cuenta</a></li>
                            @endif
                            
                            @endauth
                            @guest
                            <li class="submenu">
                                <a href="javascript:void(0);" class="show-submenu"><i class="icon-user-4"></i><i
                                        class="icon-down-open-mini"></i></a>
                                <ul>
                                    <li><a href="#0" data-toggle="modal" data-target="#login_2">Iniciar sesión</a></li>
                                    <li><a href="{{route('register')}}">Registro</a></li>
                                </ul>
                            </li> 
                            @endguest
                        </ul>
                    </div><!-- End main-menu -->
                </nav>
            </div><!-- End row -->
        </div><!-- End container -->
    </header>
    <!-- End Header =============================================== -->