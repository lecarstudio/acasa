@extends('layouts.app')

@section('content')
<br><br><br><br><br>
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-8">
            <div class="content">
                <div id="profile" style="display: block;" class="wrapper_indent">
                    <hr>
                    <div class="indent_title_in">
                            <h3 class="title-cost">Hola!</h3>                       
                    </div>

                    <div class="card-body">
                        <form action="{{route('register')}}" method="POST"> 
                            @csrf

                            <div class="form-group">
                                <label for="name">{{ __('Nombre') }}</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name')}}" required autocomplete="name" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="last_name">{{ __('Apellidos') }}</label>
                                <input id="last_name" type="text"
                                    class="form-control @error('last_name') is-invalid @enderror" name="last_name"
                                    value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>
                                @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="email">{{ __('E-Mail') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password">{{ __('Contraseña') }}</label>
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password_confirmation">{{ __('Confirmar Contraseña') }}</label>
                                <input id="password_confirmation" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password">
                                @error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="phone">{{ __('Télefono') }}</label>
                                        <input id="phone" type="number"
                                            class="form-control @error('phone') is-invalid @enderror" name="phone"
                                            value="{{ old('phone') }}" required autocomplete="phone" autofocus>
                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div> 
                            </div>
                            <!--End row -->

                            <div class="form-group">
                                <label for="city_id">{{ __('Ciudad') }}</label>
                                <select class="form-control @error('city_id') is-invalid @enderror"  name="city_id" required autocomplete="city_id" autofocus>
                                  @foreach($cities as $city)
                                    <option value="{{$city->id}}">{{$city->name}}</option>
                                  @endforeach
                                </select>
                                @error('city_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                 <div style="display: flex;
                                 justify-content: center;
                                 align-items: center;">
                                    <input  type="checkbox" name="conditions" id="conditions" {{ old('conditions') ? 'checked' : '' }} required>
                                    <label class="form-check-label" for="conditions">
                                        {{ __('  Acepto los términos y condiciones') }}
                                    </label>
                                </div>                                 
                            </div>

                            <button type="submit" class="btn_2">{{ __('Guardar') }}
                            </button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br><br>

@endsection

@section('css')
<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endsection