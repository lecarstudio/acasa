@extends('layouts.app')

@section('content')
<div class="container">
    <br><br><br><br><br><br><br>
    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="card">
                

                <div class="card-body">
                    <form action="{{route('login')}}" method="POST">
                        @csrf
                        <div class="login_icon_2"><i class="icon-user-outline"></i></div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="email" type="text" class="form-control-2 @error('email') is-invalid @enderror"
                                name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="E-mail">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control-2 @error('password') is-invalid @enderror"
                                name="password" value="{{ old('password') }}" required autocomplete="password" autofocus placeholder="password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        

                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-12">
                                <button type="submit" class="btn_2">
                                    {{ __('Login') }}
                                </button>
                            </div>
                            <div class="col-md-12 offset-md-12">
                                <div style="display: flex;
                                justify-content: center;
                                align-items: center;">
                                    @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                </div>
                            </div>
                            <hr>
                            <div class="col-md-12 offset-md-12">
                                <div style="display: flex;
                                justify-content: center;
                                align-items: center;">
                                    <a href="/register"><h5 class="black-word">¿No tienes cuenta de usuario? registrate</h5></a>

                                </div>
                            </div>
                          
                        @endif
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>
    <br><br><br><br>
</div>
@endsection
