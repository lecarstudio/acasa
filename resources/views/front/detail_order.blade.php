@extends('layouts.app')

@section('content')
 
<!-- Content ================================================== -->
<div class="white_bg">
    <div class="container margin_60_35">
        <div class="main_title">
            <h2 class="nomargin_top">MI PEDIDO</h2>
        </div>
        <div class="row">
            @foreach ($orders as $order)
                
            <div class="col-md-8">
                <div class="box_style_2" id="main_menu">
                    <div>
                        @if (isset($order->store->name))
                        <h2 class="inner">{{$order->store->name}}</h2>
                        @else
                        <h2 class="inner">{{$order->type}}</h2>
                        @endif
                        
                    </div>
                    <div class="card-body">
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="black-word-3"> Dirección de entrega</h5>
                                </div>
                                <div class="col-md-6">
                                    <p class="center-letter">{{$order->address->address}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="black-word-3"> Nombre de dirección</h5>
                                </div>
                                <div class="col-md-6">
                                    <p class="center-letter">{{$order->address->name}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="black-word"> Hora de entrega</h5>
                                </div>
                                <div class="col-md-6">
                                    <p class="center-letter">{{$order->delivery_hour}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="black-word"> Fecha de entrega</h5>
                                </div>
                                <div class="col-md-6">
                                    <p class="center-letter">{{$order->delivery_date}}</p>
                                </div>
                            </div>
                            <hr>
                            @if(count($order->repartidor) > 0)
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="black-word-3"> Repartidor</h5>
                                </div>
                                <div class="col-md-6">
                                    <div>
                                        <div>
                                        <img class="icono-welcome-3" src="{{asset('/storage/'.$order->repartidor[0]->avatar)}}" alt="icono">
                                        </div>                
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="black-word"> Nombre</h5>
                                </div>
                                <div class="col-md-6">
                                    <p class="center-letter">{{$order->repartidor[0]->name}} {{$order->repartidor[0]->last_name}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="black-word"> Teléfono</h5>
                                </div>
                                <div class="col-md-6">
                                    <p class="center-letter">{{$order->repartidor[0]->phone}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="black-word"> Email</h5>
                                </div>
                                <div class="col-md-6">
                                    <p class="center-letter">{{$order->repartidor[0]->email}}</p>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="black-word"> Placa</h5>
                                </div>
                                <div class="col-md-6">
                                    <p class="center-letter">{{$order->repartidor[0]->enrollment}}</p>
                                </div>
                            </div>
                            @else
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="black-word-3"> Repartidor</h5>
                                </div>
                                <div class="col-md-6">
                                    <p class="center-letter"> Sin repartidor por el momento.</p>
                                </div>
                            </div>
                            @endif
                            
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="black-word-3"> Comentarios</h5>
                                </div>
                                <div class="col-md-6">
                                    <p class="center-letter">{{$order->comments}}</p>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="black-word-3"> Estado del pedido</h5>
                                </div>
                                <div class="col-md-6">
                                    <p class="center-letter" style="color: #ed1c24;">{{$order->status}}</p>
                                </div>
                            </div>
                            <hr>


                     
                    </div>



                </div><!-- End box_style_1 -->
            </div><!-- End col-md-6 -->

            <div class="col-md-4" id="sidebar">
                <div class="theiaStickySidebar">
                    <div id="cart_box" >
                        <h3 class="title-cost">Costo del pedido <i class="icon_cart_alt pull-right"></i></h3>
                        <table class="table table_summary">
                        <tbody>
                            @foreach ($order->products as $product)
                            <tr>
                                <td style="font-weight: bold;">
                                    x{{$product->pivot->qty}}
                                </td>
                                <td style="font-weight: bold;">
                                     {{$product->name}}
                                </td>
                                <td>
                                    <strong class="pull-right"  style="text-align: end">Q {{$product->price}}</strong>
                                </td>
                            </tr>
                            @foreach ($product->pivot->options as $option)
                            <tr>
                                <td style="width: 50px; text-align: end;">
                                    x{{$option->pivot->qty}}
                                </td>
                                <td>
                                    {{$option->name}}
                                </td>
                                <td class="pull-right">
                                     Q {{$option->price}}
                                </td>
                            </tr>
                            @endforeach
                            @endforeach
                        </tbody>
                        </table>
                    
                        <table class="table table_summary">
                        <tbody>
                        <tr>
                            <td class="total">
                            Total <span class="pull-right">Q{{$order->total_cost}}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="total">
                                Envio <span class="pull-right">Q{{$order->dalivery_cost}}</span>
                            </td> 
                        </tr>
                        <tr>
                            <td class="total">
                                Propina <span class="pull-right">Q{{$order->tip}}</span>
                            </td>
                        </tr>
                        </tbody>
                        </table>
                        
                    </div><!-- End cart_box -->
                </div><!-- End theiaStickySidebar -->
            </div><!-- End col-md-3 -->

        </div><!-- End row -->
        @endforeach
    </div><!-- End container -->
    <!-- End Content =============================================== -->
</div>

@endsection
@section('css')
<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endsection