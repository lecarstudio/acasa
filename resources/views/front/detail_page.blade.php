@extends('layouts.app')

@section('content')
<!-- Content ================================================== -->
<div class="white_bg">
    <div class="container margin_60_35">
        <div class="row">
            @include('common.msg')
                <div class="flexslider" style="margin-top: 55px">
                    <ul class="slides">
                        @if (isset($images))
                        @foreach($images as $image)
                        <li>
                            <img id="anuncio-min" src="{{ asset('/storage/'.$image) }}" alt="">
                            <section class="flex-caption">
                                <p></p>
                            </section> 
                        </li>
                        @endforeach
                        @else
                        <h2></h2>
                        @endif
                        
                    </ul>
                </div>
        </div>
        <div class="col-md-12 col-sm-12">
           <div id="orderDetail" data-tsid="{{ $store }}"  data-userid="{{ Auth::user() ? Auth::user()->id : -1}}" ></div>
        </div><!-- End row -->
    </div><!-- End container -->
    <!-- End Content =============================================== -->
</div>
@endsection

@section('css')
<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endsection