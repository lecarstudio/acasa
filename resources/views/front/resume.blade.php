@extends('layouts.app')

@section('content')
<!-- Content ================================================== -->
<div class="white_bg">
<div class="container margin_60_35">
    <div class="main_title">
        <h2 class="nomargin_top">RESUMEN DE TU PEDIDO</h2>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="box_style_2" id="main_menu">
                <div>
                <h2 class="inner">{{$order->store->name}}</h2>
                <input id="costkm" type="hidden" name="costkm" value="{{$costKm[0]->value}}">
                <input id="latitude-store" type="hidden" name="latitude-store" value="{{$order->store->user->latitude}}">
                <input id="longitude-store" type="hidden" name="longitude-store" value="{{$order->store->user->longitude}}">
                </div>
                <div class="card-body">
    <form action="/payment/order" method="POST">
    @csrf
    <input type="hidden" name="order_id" value="{{$order->id}}">
    <input id="km" type="hidden" name="km" value="">
    <hr>
    @include('common.msg')
    <div class="form-group">
        <label for="comments">{{ __('Datos adicionales') }}</label>
        <textarea id="comments" type="textarea" class="form-control @error('comments') is-invalid @enderror"
        name="comments" value="{{ old('comments') }}"  autocomplete="comments" autofocus 
        placeholder="Datos adicionales o información que creas necesarias para tu pedido. Si otra persona lo recibe (nombre/teléfono)." 
        cols="30" rows="4"></textarea>
        
        @error('comments')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <hr>
    <label>{{ __('Dirección de entrega') }}</label>
    <div class="row">
        <div class="col-md-1">
            <div class="form-group">
                <i class="icon-location-1 icon_resume"></i>
            </div>
        </div>
        <div class="col-md-7">
            <div class="form-group">
                <select id="address_id" class="form-control @error('address_id') is-invalid @enderror"  name="address_id"  autocomplete="address_id" autofocus onclick="pickAddress()" required>
                <option value="">Selecciona una dirección</option>
                  @foreach($addresses as $address)
                  <option value="{{$address->id}}" data-latitude="{{$address->latitude}}" data-longitude="{{$address->longitude}}">{{$address->name}} / {{$address->address}} </option>
                  @endforeach
                </select>
                @error('address_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <a href="#0" data-toggle="modal" data-target="#direcionesModal">
                    <div class="btn_2"><i class="icon-plus-circled"></i>Agregar ubicación</div>
                </a>
            </div>
        </div>
    </div>
    <hr>
    <label>{{ __('Hora y fecha de entrega') }}</label>
    <div class="row">
        <div class="col-md-1">
            <div class="form-group">
                <i class="icon-clock icon_resume"></i>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <input id="deliveryDate" type="text"
                    class="form-control @error('delivery_date') is-invalid @enderror" name="delivery_date"
                    value="{{ old('delivery_date') }}" required autocomplete="delivery_date" autofocus readonly>
                @error('delivery_date')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <input id="deliveryHour" type="text"
                    class="form-control @error('delivery_hour') is-invalid @enderror" name="delivery_hour"
                    value="{{ old('delivery_hour') }}" required autocomplete="delivery_hour" autofocus readonly>
                @error('delivery_hour')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <a href="#0" data-toggle="modal" data-target="#horaModal">
                    <div class="btn_2">Programar pedido <i class="icon-right-open-big"></i></div>
                </a>
            </div>
        </div>
    </div>
    <hr>
    <label>{{ __('Teléfono de contacto') }}</label>
    <div class="row">
        <div class="col-md-1">
            <div class="form-group">
                <i class="icon-flag icon_resume"></i>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                
                <input id="phone" type="number"
                    class="form-control @error('phone') is-invalid @enderror" name="phone"
                    value="{{ old('phone') }}"  autocomplete="phone" autofocus onclick="pickphone()">
                @error('phone')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="col-md-6">
            <div class="payment_select">
                    <input id="definide-phone" type="radio" class="@error('phone') is-invalid @enderror"
                    name="phone" value="{{auth()->user()->phone}}" autocomplete="phone" autofocus onclick="pickdefinidephone()">
                    <label>{{auth()->user()->phone}}</label> 
                    <i class="icon_phone"></i>
                    @error('phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
        </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <label>{{ __('Precio de envío') }}</label>
        </div>
        <div class="col-md-6">
            <label>{{ __('Tiempo estimado') }}</label>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-1">
            <div class="form-group">
                <i class="icon-money icon_resume"></i>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <input id="dalivery_cost" type="text"
                    class="form-control @error('dalivery_cost') is-invalid @enderror" name="dalivery_cost"
                    value="" required autocomplete="dalivery_cost" autofocus readonly>
                @error('dalivery_cost')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <i class=" icon-stopwatch icon_resume"></i>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <input id="delivery_hour" type="text"
                    class="form-control @error('delivery_hour') is-invalid @enderror" name="delivery_hour"
                    value="<?php echo $setting[0]->value; ?>" required autocomplete="delivery_hour" autofocus readonly>
                @error('delivery_hour')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
    </div>
    <hr>                               
    <label for="expiration-date">{{ __('Propina') }}</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2">
                                        <div class="form-group">
                                             <div class="payment_select">
                                                <label for="tip">
                                                    <input id="tip" type="radio" class="@error('tip') is-invalid @enderror"
                                                    name="tip" value="{{ old('0') }}" required autocomplete="tip" autofocus>
                                                    {{ __('Q0') }}
                                                </label>
                                            </div>                                     
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="form-group">
                                           <div class="payment_select">
                                            <label for="tip">
                                                <input id="tipq5" type="radio" class="@error('tip') is-invalid @enderror"
                                                name="tip" value="5" required autocomplete="tip" autofocus> 
                                                {{ __('Q5') }}
                                            </label>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="form-group">
                                            <div class="payment_select">
                                                <label for="tip">
                                                    <input id="tip" type="radio" class="@error('tip') is-invalid @enderror"
                                                    name="tip" value="10" required autocomplete="tip" autofocus>
                                                    {{ __('Q10') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="form-group">
                                            <div class="payment_select">
                                                <label for="tip">
                                                    <input id="tip" type="radio" class="@error('tip') is-invalid @enderror"
                                                    name="tip" value="15" required autocomplete="tip" autofocus>
                                                    {{ __('Q15') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="form-group">
                                            <div class="payment_select">
                                                <label for="tip">
                                                    <input id="tip" type="radio" class="@error('tip') is-invalid @enderror"
                                                    name="tip" value="20" required autocomplete="tip" autofocus>
                                                    {{ __('Q20') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="form-group">
                                            <div class="payment_select">
                                                <label for="tip">
                                                    <input id="tip" type="radio" class="@error('tip') is-invalid @enderror"
                                                    name="tip" value="25" required autocomplete="tip" autofocus>
                                                    {{ __('Q25') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--End row -->
    
    
                    
                </div>
               
                
 
            </div><!-- End box_style_1 -->
        </div><!-- End col-md-6 -->
        
        <div class="col-md-4" id="sidebar">
        <div class="theiaStickySidebar">
            <div id="cart_box" >
                <h3 class="title-cost">Costo del pedido <i class="icon_cart_alt pull-right"></i></h3>
                <table class="table table_summary">
                <tbody>
                    @foreach ($order->products as $product)
                    <tr>
                        <td style="font-weight: bold;">
                            x{{$product->pivot->qty}}
                        </td>
                        <td style="font-weight: bold;">
                             {{$product->name}}
                        </td>
                        <td>
                            <strong class="pull-right"  style="text-align: end">Q {{$product->price}}</strong>
                        </td>
                    </tr>
                    @foreach ($product->pivot->options as $option)
                    <tr>
                        <td style="width: 50px; text-align: end;">
                            x{{$option->pivot->qty}}
                        </td>
                        <td>
                            {{$option->name}}
                        </td>
                        <td class="pull-right">
                             Q {{$option->price}}
                        </td>
                    </tr>
                    @endforeach
                    @endforeach
                </tbody>
                </table>
            
                <table class="table table_summary">
                <tbody>
                <tr>
                    <td class="total">
                    TOTAL <span class="pull-right">Q{{$order->total_cost}}</span>
                    </td>
                </tr>
                </tbody>
                </table>
                <hr>
                <a href="#0" data-toggle="modal" data-target="#pagosModal"  className="btn_2">
                    <div class="btn_2">Pagar<i class="icon-cash"></i></div>
                </a>
                {{-- <div id="paymentBtn"  data-userClientID="{{ Auth::user() ? Auth::user()->user_client_id : -1}}"></div> --}}
                
                <!-- Pagos modal -->
<div class="modal" id="pagosModal" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 191px;">
        <div class="modal-content modal-popup">
            <a href="#0" class="close-link" type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                    class="icon_close_alt2"></i></a>
                    <div class="col-md-6"></div>
                    <hr>
                    <h3 class="title-cost">Completar Compra</h3>
       
            <div class="card-body">
                    <div class="payment_select">
                        <label for="payment">
                            <input id="x_card_type" type="radio" class="@error('x_card_type') is-invalid @enderror"
                                name="x_card_type" value="001" required autocomplete="x_card_type" autofocus>
                            VISA
                        </label>
                        <i class="icon_creditcard"></i>
                    </div>

                    <div class="payment_select">
                        <label for="x_card_type">
                            <input id="x_card_type" type="radio" class="@error('x_card_type') is-invalid @enderror"
                                name="x_card_type" value="002" required autocomplete="x_card_type" autofocus>
                            MASTERCARD
                        </label>
                        <i class="icon_creditcard"></i>
                    </div>                  

                    <div class="form-group">
                        <label for="name-on-Card">{{ __('Nombre de la tarjeta de crédito ó el poseedor de la cuenta') }}</label>
                        <input id="name-on-Card" type="text" class="form-control @error('name-on-Card') is-invalid @enderror"
                            name="name-on-Card" value="{{ old('name-on-Card') }}" required autocomplete="name-on-Card" autofocus placeholder="Nombre y apellidos">
                    </div>

                    <div class="form-group">
                        <label for="card-number">{{ __('Número de tarjeta') }}</label>
                        <input id="card-number" type="text"
                            class="form-control @error('cc_number') is-invalid @enderror" name="cc_number"
                            value="{{ old('cc_number') }}" required autocomplete="card-number" autofocus placeholder="Número de tarjeta">
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label for="expiration-date">{{ __('Fecha de expiración') }}</label>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <input id="month" type="text"
                                        class="form-control @error('x_exp_month') is-invalid @enderror" name="x_exp_month"
                                        value="{{ old('x_exp_month') }}" required autocomplete="x_exp_month" autofocus placeholder="mm">
                                        
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <input id="expire_year" type="text"
                                        class="form-control @error('x_exp_year') is-invalid @enderror" name="x_exp_year"
                                        value="{{ old('x_exp_year') }}" required autocomplete="x_exp_year" autofocus placeholder="yyyy">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="expiration-date">{{ __('Código de seguridad') }}</label>
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="form-group">
                                            <input id="cc_cvv2" type="text"
                                            class="form-control @error('cc_cvv2') is-invalid @enderror" name="cc_cvv2"
                                            value="{{ old('cc_cvv2') }}" required autocomplete="cc_cvv2" autofocus placeholder="CCV">
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-6">
                                        <img src="{{ asset('img/icon_ccv.gif') }}" width="50" height="29" alt="ccv"><small>Últimos 3 dígitos</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--End row -->

                    <button type="submit" class="btn_1">{{ __('Guardar') }}
                    </button>

            </div>
        </div>
    </div>
</div><!-- End modal -->


                
            </div><!-- End cart_box -->
            </div><!-- End theiaStickySidebar -->
        </div><!-- End col-md-3 -->
        </form>
    </div><!-- End row -->
</div><!-- End container -->
<!-- End Content =============================================== -->
</div>


<!-- Direcciones modal -->

<div class="modal" id="direcionesModal" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 191px;">
        <div class="modal-content modal-popup">
            <a href="#0" class="close-link" type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                    class="icon_close_alt2"></i></a>
                    <h3 class="title-cost">Mis direcciones</h3>
                    <form action="{{route('profile.address')}}" method="POST" class="popup-form" id="myLogin">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <input type="hidden" name="type" value="Entrega">
                            <input id="name-direction" type="text"
                                class="form-control @error('name-direction') is-invalid @enderror" name="name"
                                value="{{ old('name-direction') }}" required autocomplete="name-direction" autofocus
                                placeholder="Alias de dirección">
                            @error('name-direction')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-9 col-sm-9">
                                    <input id="latitude" type="hidden" name="latitude">
                                    <input id="longitude" type="hidden" name="longitude">
                                    <input  class="form-control" id="address" name="address" type="text" required/>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <span class="input-group-btn">
                                        <button id="submit" type="button" class="form-control btn_2" style="color: #FFF;">Buscar</button>
                                    </span>
                                </div>
                            </div>                        
                            <div id="myMap"></div>
                        </div>
                        <div class="form-group">
                            <input id="references" type="text"
                                class="form-control @error('references') is-invalid @enderror" name="references"
                                value="{{ old('references') }}" autocomplete="references" autofocus
                                placeholder="Referencias">
                            @error('references')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn_2">Guardar</button>
                    </form>
        </div>
    </div>
</div><!-- End modal -->

<!-- Hora modal -->

<div class="modal" id="horaModal" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 191px;">
        <div class="modal-content modal-popup">
            <a  href="#0" class="close-link closeModalH" type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                    class="icon_close_alt2"></i></a>
                    <div class="col-md-6"></div>
                    <hr>
                    <h3 class="title-cost">Programar pedido</h3>
                            
                           
                <hr>
                <div class="card-body">
                    <div class="row">
                       
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="dia">Día</label> 
                                <div class="scroll-day">
                                    <ul>
                                        @foreach ($days as $day)
                                        <li data-current="{{date('Y-m-d')}}"  class="select-scale cursor selectDate" data-value="{{$day['value']}}" >{{$day['label'].' '.$day['day']}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @error('dia')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="hora">Hora</label> 
                                <div class="scroll-time">
                                    <ul>
                                        <div id="hours">
                                            @foreach ($hours as $hour)
                                                <li class="select-scale cursor selectHour "  data-value="{{$hour['value']}}">{{$hour['label']}}</li>
                                            @endforeach    
                                        </div>
                                        <div  id="hoursOpen" style="display: none" >

                                        @foreach ($hoursOpen as $hour)
                                            <li class="select-scale cursor selectHour"  data-value="{{$hour['value']}}">{{$hour['label']}}</li>
                                        @endforeach     
                                    </div>

                                    </ul>
                                </div>
                                @error('hora')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        
                    </div>
                    <hr>

                    <button type="button" id="saveSelectedH" class="btn_1" class="close" data-dismiss="modal" >{{ __('Guardar') }}
                    </button>
            </div>
        </div>
    </div>
</div><!-- End modal -->

<!-- Direcciones modal -->

<div class="modal" id="paymentsModal" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 191px;">
        <div class="modal-content modal-popup">
            <a href="#0" class="close-link" type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                    class="icon_close_alt2"></i></a>
                    <hr>
                    <h3 class="title-cost">Método de pago</h3>
            <hr>
            <div class="card-body">
            <table class="table">
                <tr class="table-tr">
                   
                    <td class="table-th-td">
                        <i class="icon-credit-card icon_resume"></i>
                    </td>
                    <td class="table-th-td_2">
                        <a href="#0" class="black-word">
                        <strong>Banamex ***********7290</strong>
                        </a>
                    </td>
                </tr>
                <tr class="table-tr">
                   
                    <td class="table-th-td">
                        <i class="icon-credit-card icon_resume"></i>
                    </td>
                    <td class="table-th-td_2">
                        <a href="#0" class="black-word">
                        <strong>Bancomer ***********3215</strong>
                        </a>
                    </td>
                </tr>
            </table>
            </div>
            <hr>
            <a href="#0" data-toggle="modal" data-target="#pagosModal">
                <div class="btn_2"><i class="icon-plus-circled"></i>Agregar tarjeta</div>
            </a>
            <hr>
        </div>
    </div>
</div><!-- End modal -->




<script type="text/javascript">
  
        function pickphone(){
            document.getElementById("definide-phone").checked = false;
        }
        function pickTarjeta(){
            document.getElementById("cash").checked = false;
        }
        function pickdefinidephone(){
            $('#phone').val('');
        }
        function pickAddress(){
            $('#direction').val('');
        }
        function pickDirection(){
            $('#address_id').val('');
        }
        
  
</script>
@endsection 

@section('css')
<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endsection 