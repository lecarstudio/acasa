@extends('layouts.app')

@section('content')
<!-- Content ================================================== -->
<div class="white_bg">
    <div class="container margin_60_35">
<div class="row">
        <div class="flexslider" style="margin-top: 55px">
            <ul class="slides">
                @foreach($images as $image)
                    <li>
                        <img src="{{ asset('/storage/'.$image) }}" alt="">
                        <section class="flex-caption">
                            <p></p>
                        </section> 
                    </li>
                @endforeach
            </ul>
        </div>
</div>
        <div class="row">
            <div class="col-md-8">
                <div class="box_style_2" id="main_menu">
                    @include('common.msg')
                    <div>
                        <h2 class="inner">{{$store->name}}
                            @auth
                            <a href="/favorite/add/{{$store->id}}/{{Auth::user()->id}}"><i class="icon-heart"></i></a>
                            @endauth                        
                        </h2>
                        
                       
                        <hr class="more_margin">

                        <div class="row">

                            <div class="col-md-8">
                                <div class="rating">
                                    @for ($i = 0; $i < $store->stars; $i++)
                                        <i class="icon_star voted"></i>
                                    @endfor
                                </div>
                            </div>
                        </div>


                        <div><em>{{$store->subcategory->name}}</em></div>
                        <div><i class="icon_pin"></i>
                            {{$store->addressStore->address}}
                        </div>
                    </div>

                    

                    @foreach ($store->packages as $package)
                    <h3 id="main_courses">{{$package->name}}</h3>
                    <table class="table table-striped">

                        <tbody>
                            @foreach ($package->products as $product)
                            <tr>
                                <td>
                                    <figure class="thumb_menu_list_2"><img src="{{ asset('/storage/'.$product->image) }}" alt="thumb">
                                    </figure>
                                    <h5 class="black-word">{{$product->name}}</h5>
                                    <br>
                                    <p>
                                        {{$product->short_description}}
                                    </p>
                                </td>
                                <td>
                                    {{-- <strong class="promo">Q 60.00</strong>
                                    <br> --}}
                                    <strong>Q {{$product->price}}</strong>
                                </td>
                                <td class="options">
                                    <div class="dropdown dropdown-options">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="true"><i class="icon_plus_alt2"></i></a>
                                        <div class="dropdown-menu" style>
                                            <h5 class="black-word">Tienes más opciones</h5>
                                            @foreach ($product->options as $option)
                                                <label>
                                                    <label>
                                                        <input type="checkbox" value="{{$option->id}}">{{$option->name}}<span>+ Q {{$option->price}}</span>
                                                    </label>
                                                </label>
                                            @endforeach
                                            
                                            
                                            <a href="{{route('addProduct',[$product->id,$store->id])}}" class="add_to_basket">Agregar al pedido</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            @if (count($package->products) == 0)
                                <li>Sin Productos</li>
                            @endif
                           
                        </tbody>
                    </table>
                    @endforeach
                    <hr>
            

                </div><!-- End box_style_1 -->
            </div><!-- End col-md-6 -->

            <div class="col-md-4" id="sidebar">
                <div class="theiaStickySidebar">
                    <div id="cart_box">
                        <h3 class="title-cost">Costo del pedido <i class="icon_cart_alt pull-right"></i></h3>
                        <table class="table table_summary">
                            <tbody>
                                <tr>
                                    <td>
                                        <a href="#0" class="remove_item"><i class="icon_minus_alt"></i></a>
                                        <strong>1x</strong> Taco de pollo gigante
                                    </td>
                                    <td>
                                        <strong class="pull-right">Q 11</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#0" class="remove_item"><i class="icon_minus_alt"></i></a>
                                        <strong>2x</strong> Ensalada mexicana con pollo

                                    </td>
                                    <td>
                                        <strong class="pull-right">Q 14</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#0" class="remove_item"><i class="icon_minus_alt"></i></a>
                                        <strong>1x</strong> Pure de patata
                                    </td>
                                    <td>
                                        <strong class="pull-right">Q 20</strong>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table_summary">
                            <tbody>
                                <tr>
                                    <td class="total">
                                        TOTAL <span class="pull-right">Q 66</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <a class="btn_2" href="/resume">Hacer pedido</a>
                    </div><!-- End cart_box -->
                </div><!-- End theiaStickySidebar -->
            </div><!-- End col-md-3 -->

        </div><!-- End row -->
    </div><!-- End container -->
    <!-- End Content =============================================== -->
</div>
@endsection

@section('css')
<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endsection