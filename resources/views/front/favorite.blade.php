@extends('layouts.app')
@section('content')
<div class="white_bg">
    <div class="container margin_60_35">
        <div class="main_title">
            <h2 class="nomargin_top">{{$tittleFavorite[0]->value}}</h2>
            @include('common.msg')
        </div>
        <div class="row">
            
            @auth
            @foreach ($favorites as $favorite)
            <div class="col-md-6 col-sm-6 wow zoomIn" data-wow-delay="0.1s">
                <a class="strip_list grid" href="/detail_page/{{$favorite->store->id}}">
                            <div class="contenedor-image">
                                <img class="image-fav" src="{{ asset('/storage/'.$favorite->store->cover) }}" alt="" />
                                <div class="centrado">{{$favorite->store->name}}</div>
                                <div class="texto-encima">
                                    <a href="/favorite/delete/{{$favorite->id}}"><i class="icon_close_alt2"></i></a>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-md-7">
                                    <div class="rating">
                                        @for ($i = 0; $i < $favorite->store->stars; $i++)
                                        <i class="icon_star voted"></i>
                                        @endfor
                                    </div>
                                </div>
                            </div>               
                           
                </a><!-- End strip_list-->
            </div><!-- End col-md-6-->
            @endforeach
            @if (count($favorites) == 0)
            <div class="col-md-12 sol-sm-12">
            <p style="margin-top: 10%; text-align: center;">Aún no tienes favoritos!</p>
            </div>
            @endif
            @endauth
            
        </div>
    </div>
</div>
@endsection
@section('css')
<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endsection