
@extends('layouts.app')

@section('content')


<!-- END QPAYRADAR FINGERPRINT CODE -->
<?php

$base_api = "https://sandbox.qpaypro.com/payment/api_v1";

$testMode = true;
$sessionID = uniqid();
$orgID = $testMode ? '1snn5n9w' : 'k8vif92e';
$mechantID = 'visanetgt_qpay';

if(isset($_GET["fingerId"])){
$fingerId = $_GET["fingerId"];
}else{
$fingerId = '';
}

$array = [
"x_login"=> "visanetgt_qpay",
"x_private_key"=> "88888888888",
"x_api_secret"=> "99999999999",
"x_product_id"=> "1",
"x_audit_number"=> "20",
"x_fp_sequence"=> "20",
"x_fp_timestamp"=> "154681351",
"x_invoice_num"=> "001",
"x_currency_code"=> "GTQ",
"x_amount"=> "50.00",
"x_line_item"=> "T-Shirt Live Dreams<|>w01<|><|>1<|>85.00<|>N",
"x_freight"=> "0",
"x_email"=> "email@tuempresa.com",
"cc_number"=> "40000000000059424",
"cc_exp"=> "01/22",
"cc_cvv2"=> "456",
"cc_name"=> "Pedro Quino",
"x_first_name"=> "Pedro",
"x_last_name"=> "Quino",
"x_company"=> "1234567-8",
"x_address"=> "1 calle 2 ave",
"x_city"=> "Guatemala",
"x_state"=> "Guatemala",
"x_country"=> "Guatemala",
"x_zip"=> "01011",
"x_relay_response"=> "none",
"x_relay_url"=> "none",
"x_type"=> "AUTH_ONLY",
"x_method"=> "CC",
"http_origin"=> "tuempresa.com",
"cc_type"=> 'visa',
"visaencuotas"=> 0,
"device_fingerprint_id"=> $sessionID,
"finger"=>$fingerId
];


$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $base_api);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $array);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
$resp = curl_exec($ch);

$info = curl_getinfo($ch);
$resp = strstr($resp, "{", false);
$json = json_decode($resp);

dd($json);


?>




@endsection 