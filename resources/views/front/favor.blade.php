@extends('layouts.app')

@section('content')
<!-- Content ================================================== -->
<div class="white_bg">
<div class="container margin_60_35">
    <div class="main_title">
        <h2 class="nomargin_top">RESUMEN DE TU MANDADO</h2>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box_style_2" id="main_menu">
                <div>
                <h2 class="inner">{{$tittle[0]->value}}</h2>
                </div>
                <div class="card-body">
    <form action="{{route('order.create')}}" method="POST">
    @csrf
    <input type="hidden" name="type" value="Mandado">
    <input id="km" type="hidden" name="km" value="">
    @include('common.msg')
    <hr>
    <label>{{ __('Dirección de partida') }}</label>
    <div class="row">
        <div class="col-md-1">
            <div class="form-group">
                <i class="icon-location-1 icon_resume"></i>
            </div>
        </div>
        <input id="costkm" type="hidden" name="costkm" value="{{$costKm[0]->value}}">
        <input id="latitude-store" type="hidden" name="latitude-store" value="">
        <input id="longitude-store" type="hidden" name="longitude-store" value="">
        <input id="latitude-destinity" type="hidden" name="latitude-destinity" value="">
        <input id="longitude-destinity" type="hidden" name="longitude-destinity" value="">

        <div class="col-md-7">
            <div class="form-group">
                <select id="getStoredirection" class="form-control @error('address_start_id') is-invalid @enderror"  name="address_start_id"  autocomplete="address_start_id" autofocus  required>
                <option value="">Direcciones de partida registradas</option>
                  @foreach($addressStarts as $address)
                  <option value="{{$address->id}}" data-latitude="{{$address->latitude}}" data-longitude="{{$address->longitude}}">{{$address->name}} / {{$address->address}} </option>
                  @endforeach
                </select>
                @error('address_start_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <a href="#0" data-toggle="modal" data-target="#direcionesModal" onclick="setTypePartida()">
                    <div class="btn_2"><i class="icon-plus-circled"></i>Agregar ubicación</div>
                </a>
            </div>
        </div>
    </div>
    <hr>
    <label>{{ __('Dirección de entrega') }}</label>
    <div class="row">
        <div class="col-md-1">
            <div class="form-group">
                <i class="icon-location-1 icon_resume"></i>
            </div>
        </div>
        <div class="col-md-7">
            <div class="form-group">
                <select required id="address_id" class="form-control @error('address_id') is-invalid @enderror"  name="address_id"  autocomplete="address_id" autofocus required>
                <option value="">Selecciona una dirección</option>
                  @foreach($addresses as $address)
                  <option value="{{$address->id}}" data-latitude="{{$address->latitude}}" data-longitude="{{$address->longitude}}">{{$address->name}} / {{$address->address}} </option>
                  @endforeach
                </select>
                @error('address_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <a href="#0" data-toggle="modal" data-target="#direcionesModal" onclick="setTypeEntrega()">
                    <div class="btn_2"><i class="icon-plus-circled"></i>Agregar ubicación</div>
                </a>
            </div>
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label for="comments">{{ __('Descripcion del mandado') }}</label>
        <textarea id="comments" type="textarea" class="form-control @error('comments') is-invalid @enderror"
        name="comments" value="{{ old('comments') }}"  autocomplete="comments" autofocus 
        placeholder="Descripción o información que creas necesaria para tu mandado. Si otra persona lo recibe (nombre/teléfono)." 
        cols="30" rows="6" required></textarea>
        
        @error('comments')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>   
    <hr>
    <label>{{ __('Hora y fecha de entrega') }}</label>
    <div class="row">
        <div class="col-md-1">
            <div class="form-group">
                <i class="icon-clock icon_resume"></i>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <input id="deliveryDate" type="text"
                    class="form-control @error('delivery_date') is-invalid @enderror" name="delivery_date"
                    value="{{ old('delivery_date') }}" required autocomplete="delivery_date" autofocus readonly>
                @error('delivery_date')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <input id="deliveryHour" type="text"
                    class="form-control @error('delivery_hour') is-invalid @enderror" name="delivery_hour"
                    value="{{ old('delivery_hour') }}" required autocomplete="delivery_hour" autofocus readonly>
                @error('delivery_hour')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <a href="#0" data-toggle="modal" data-target="#horaModal">
                    <div class="btn_2">Programar pedido <i class="icon-right-open-big"></i></div>
                </a>
            </div>
        </div>
    </div>
    <hr>
    <label>{{ __('Teléfono de contacto') }}</label>
    <div class="row">
        <div class="col-md-1">
            <div class="form-group">
                <i class="icon-flag icon_resume"></i>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                
                <input id="phone" type="number"
                    class="form-control @error('phone') is-invalid @enderror" name="phone"
                    value="{{ old('phone') }}"  autocomplete="phone" autofocus onclick="pickphone()">
                @error('phone')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="col-md-6">
            <div class="payment_select">
                    <input id="definide-phone" type="radio" class="@error('phone') is-invalid @enderror"
                    name="phone" value="{{auth()->user()->phone}}" autocomplete="phone" autofocus onclick="pickdefinidephone()">
                    <label>{{auth()->user()->phone}}</label> 
                    <i class="icon_phone"></i>
                    @error('phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
        </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <label>{{ __('Costo de envío') }}</label>
        </div>
        <div class="col-md-4">
            <label>{{ __('Tarifa base') }}</label>
        </div>
        <div class="col-md-4">
            <label>{{ __('Tiempo estimado') }}</label>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-1">
            <div class="form-group">
                <i class="icon-bus icon_resume"></i>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <input id="dalivery_cost" type="text"
                    class="form-control @error('dalivery_cost') is-invalid @enderror" name="dalivery_cost"
                    value="<?php echo "120.00"; ?>" required autocomplete="dalivery_cost" autofocus readonly>
                @error('dalivery_cost')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <i class="icon-money icon_resume"></i>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <input id="total_cost" type="text"
                    class="form-control @error('total_cost') is-invalid @enderror" name="total_cost"
                    value="<?php echo $cost[0]->value; ?>" required autocomplete="total_cost" autofocus readonly>
                @error('total_cost')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <i class=" icon-stopwatch icon_resume"></i>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <input id="delivery-hour" type="text"
                    class="form-control @error('delivery-hour') is-invalid @enderror" name="delivery-hour"
                    value="<?php echo $time[0]->value; ?>" required autocomplete="delivery-hour" autofocus readonly>
                @error('delivery-hour')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
    </div>
    <hr>                               
    <label for="expiration-date">{{ __('Propina') }}</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2">
                                        <div class="form-group">
                                             <div class="payment_select">
                                                <label for="tip">
                                                    <input id="tip" type="radio" class="@error('tip') is-invalid @enderror"
                                                    name="tip" value="0" required autocomplete="tip" autofocus>
                                                    {{ __('Q0') }}
                                                </label>
                                            </div>                                     
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="form-group">
                                           <div class="payment_select">
                                            <label for="tip">
                                                <input id="tipq5" type="radio" class="@error('tip') is-invalid @enderror"
                                                name="tip" value="5" required autocomplete="tip" autofocus> 
                                                {{ __('Q5') }}
                                            </label>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="form-group">
                                            <div class="payment_select">
                                                <label for="tip">
                                                    <input id="tip" type="radio" class="@error('tip') is-invalid @enderror"
                                                    name="tip" value="10" required autocomplete="tip" autofocus>
                                                    {{ __('Q10') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="form-group">
                                            <div class="payment_select">
                                                <label for="tip">
                                                    <input id="tip" type="radio" class="@error('tip') is-invalid @enderror"
                                                    name="tip" value="15" required autocomplete="tip" autofocus>
                                                    {{ __('Q15') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="form-group">
                                            <div class="payment_select">
                                                <label for="tip">
                                                    <input id="tip" type="radio" class="@error('tip') is-invalid @enderror"
                                                    name="tip" value="20" required autocomplete="tip" autofocus>
                                                    {{ __('Q20') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="form-group">
                                            <div class="payment_select">
                                                <label for="tip">
                                                    <input id="tip" type="radio" class="@error('tip') is-invalid @enderror"
                                                    name="tip" value="25" required autocomplete="tip" autofocus>
                                                    {{ __('Q25') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--End row -->
                        <hr>
                        <button type="submit" class="btn_2">Hacer pedido</button>
                    
                </div>
               
                
 
            </div><!-- End box_style_1 -->
        </div><!-- End col-md-8 -->
        </form>
    </div><!-- End row -->
</div><!-- End container -->
<!-- End Content =============================================== -->
</div>

<!-- Direcciones modal -->

<div class="modal" id="direcionesModal" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 191px;">
        <div class="modal-content modal-popup">
            <a href="#0" class="close-link" type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                    class="icon_close_alt2"></i></a>
                    <h3 class="title-cost">Mis direcciones</h3>
                    <form action="{{route('profile.address')}}" method="POST" class="popup-form" id="myLogin">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <input id="type-direction" type="hidden" name="type" value="">
                            <input id="name-direction" type="text"
                                class="form-control @error('name-direction') is-invalid @enderror" name="name"
                                value="{{ old('name-direction') }}" required autocomplete="name-direction" autofocus
                                placeholder="Alias de dirección">
                            @error('name-direction')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-9 col-sm-9">
                                    <input id="latitude" type="hidden" name="latitude">
                                    <input id="longitude" type="hidden" name="longitude">
                                    <input  class="form-control" id="address" name="address" type="text" required/>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <span class="input-group-btn">
                                        <button id="submit" type="button" class="form-control btn_2" style="color: #FFF;">Buscar</button>
                                    </span>
                                </div>
                            </div>                        
                            <div id="myMap"></div>
                        </div>
                        <div class="form-group">
                            <input id="references" type="text"
                                class="form-control @error('references') is-invalid @enderror" name="references"
                                value="{{ old('references') }}" autocomplete="references" autofocus
                                placeholder="Referencias">
                            @error('references')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn_2">Guardar</button>
                    </form>
        </div>
    </div>
</div><!-- End modal -->

<!-- Hora modal -->

<div class="modal" id="horaModal" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 191px;">
        <div class="modal-content modal-popup">
            <a  href="#0" class="close-link closeModalH" type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                    class="icon_close_alt2"></i></a>
                    <div class="col-md-6"></div>
                    <hr>
                    <h3 class="title-cost">Programar pedido</h3>
                            
                           
                <hr>
                <div class="card-body">
                    <div class="row">
                       
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="dia">Día</label> 
                                <div class="scroll-day">
                                    <ul>
                                        @foreach ($days as $day)
                                         <li class="select-scale cursor selectDate" data-value="{{$day['value']}}" >{{$day['label'].' '.$day['day']}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @error('dia')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="hora">Hora</label> 
                                <div class="scroll-time">
                                    <ul>
                                        @foreach ($hours as $hour)
                                            <li class="select-scale cursor selectHour"  data-value="{{$hour['value']}}">{{$hour['label']}}</li>
                                        @endforeach                                         
                                    </ul>
                                </div>
                                @error('hora')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        
                    </div>
                    <hr>

                    <button type="button" id="saveSelectedH" class="btn_1" class="close" data-dismiss="modal" >{{ __('Guardar') }}
                    </button>
            </div>
        </div>
    </div>
</div><!-- End modal -->

<!-- Direcciones modal -->

<div class="modal" id="paymentsModal" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 191px;">
        <div class="modal-content modal-popup">
            <a href="#0" class="close-link" type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                    class="icon_close_alt2"></i></a>
                    <hr>
                    <h3 class="title-cost">Método de pago</h3>
            <hr>
            <div class="card-body">
            <table class="table">
                <tr class="table-tr">
                   
                    <td class="table-th-td">
                        <i class="icon-credit-card icon_resume"></i>
                    </td>
                    <td class="table-th-td_2">
                        <a href="#0" class="black-word">
                        <strong>Banamex ***********7290</strong>
                        </a>
                    </td>
                </tr>
                <tr class="table-tr">
                   
                    <td class="table-th-td">
                        <i class="icon-credit-card icon_resume"></i>
                    </td>
                    <td class="table-th-td_2">
                        <a href="#0" class="black-word">
                        <strong>Bancomer ***********3215</strong>
                        </a>
                    </td>
                </tr>
            </table>
            </div>
            <hr>
            <a href="#0" data-toggle="modal" data-target="#pagosModal">
                <div class="btn_2"><i class="icon-plus-circled"></i>Agregar tarjeta</div>
            </a>
            <hr>
        </div>
    </div>
</div><!-- End modal -->

<!-- Pagos modal -->

<div class="modal" id="pagosModal" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
    <div class="modal-dialog" style="margin-top: 191px;">
        <div class="modal-content modal-popup">
            <a href="#0" class="close-link" type="button" class="close" data-dismiss="modal" aria-label="Close"><i
                    class="icon_close_alt2"></i></a>
                    <div class="col-md-6"></div>
                    <hr>
                    <h3 class="title-cost">Agregar método de pago</h3>
                            
                           
       
            <div class="card-body">
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="payment_select">
                            <label for="payment">
                                <input id="payment" type="radio" class="@error('payment') is-invalid @enderror"
                                name="payment" value="{{ old('credit') }}" required autocomplete="payment" autofocus>
                                Tarjeta de crédito
                            </label>
                            <i class="icon_creditcard"></i>
                    </div>

                    <div class="payment_select">
                        <label for="payment">
                            <input id="payment" type="radio" class="@error('payment') is-invalid @enderror"
                            name="payment" value="{{ old('bebit') }}" required autocomplete="payment" autofocus>
                            Tarjeta de dédito
                        </label>
                        <i class="icon_creditcard"></i>
                    </div>

                    <div class="form-group">
                        <label for="name-on-Card">{{ __('Nombre de la tarjeta de crédito ó el poseedor de la cuenta') }}</label>
                        <input id="name-on-Card" type="text" class="form-control @error('name-on-Card') is-invalid @enderror"
                            name="name-on-Card" value="{{ old('name-on-Card') }}" required autocomplete="name-on-Card" autofocus placeholder="Nombre y apellidos">
                        @error('name-on-Card')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="card-number">{{ __('Número de tarjeta') }}</label>
                        <input id="card-number" type="text"
                            class="form-control @error('card-number') is-invalid @enderror" name="card-number"
                            value="{{ old('card-number') }}" required autocomplete="card-number" autofocus placeholder="Número de tarjeta">
                        @error('card-number')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label for="expiration-date">{{ __('Fecha de expiración') }}</label>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <input id="month" type="text"
                                        class="form-control @error('month') is-invalid @enderror" name="month"
                                        value="{{ old('month') }}" required autocomplete="month" autofocus placeholder="mm">
                                        
                                    </div>
                                    @error('month')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <input id="expire_year" type="text"
                                        class="form-control @error('expire_year') is-invalid @enderror" name="expire_year"
                                        value="{{ old('expire_year') }}" required autocomplete="expire_year" autofocus placeholder="yyyy">
                                    </div>
                                    @error('expire_year')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="expiration-date">{{ __('Código de seguridad') }}</label>
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="form-group">
                                            <input id="ccv" type="text"
                                            class="form-control @error('ccv') is-invalid @enderror" name="ccv"
                                            value="{{ old('ccv') }}" required autocomplete="ccv" autofocus placeholder="CCV">
                                        </div>
                                        @error('ccv')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-8 col-sm-6">
                                        <img src="img/icon_ccv.gif" width="50" height="29" alt="ccv"><small>Last 3 digits</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--End row -->

                    <button type="submit" class="btn_1">{{ __('Guardar') }}
                    </button>

                </form>
            </div>
        </div>
    </div>
</div><!-- End modal -->

<script type="text/javascript">
  
        function pickphone(){
            document.getElementById("definide-phone").checked = false;
        }
        function pickTarjeta(){
            document.getElementById("cash").checked = false;
        }
        function pickdefinidephone(){
            $('#phone').val('');
        }
        function pickAddress(){
            $('#direction').val('');
        }
        function pickDirection(){
            $('#address_id').val('');
        }

        function setTypePartida(){
            $('#type-direction').val('Partida');
        }
        function setTypeEntrega(){
            $('#type-direction').val('Entrega');
        }
        
  
</script>

@endsection 

@section('css')
<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endsection 