@extends('layouts.app')

@section('content')
<div>
    <iframe style="width:100%;height:100vh;" src="https://sandbox.qpaypro.com/payment/store?token={{$paymentToken}}" frameborder="0"></iframe>
</div>
@endsection
@section('css')
<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endsection