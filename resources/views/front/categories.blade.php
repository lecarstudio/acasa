@extends('layouts.app')
@section('content')
<div class="white_bg">
<!-- Content ================================================== -->
<div class="container margin_60_35">

    <div class="row">
        @include('common.msg')
            <div class="flexslider" style="margin-top: 55px">
                <ul class="slides">
                    @foreach($advertisements as $advertisement)
                        <li>
                            <a href="/detail_page/{{$advertisement->store_id}}">
                            <img id="anuncio-min" src="{{ asset('/storage/'.$advertisement->images) }}" alt="">
                            <section class="flex-caption">
                                <p></p>
                            </section> 
                            </a>
                        </li>
                    @endforeach
                    @if (count($advertisements) == 0)
                        <li></li>
                    @endif
                </ul>
            </div>
    </div>

	<div class="row">
		<div class="col-md-3">
			<div id="filters_col">
                <h6 class="filters_col_h6">Subcategorías</h6>
                <hr>
			 	<div class="collapse" id="collapseFilters">
					<div class="filter_type">
						<ul>
                            @if (isset($subCategories))
                                @foreach ($subCategories as $subCategory)
                                <li class="top-categorie"><label style="color: #e91e63;"><a href="/sub-categories/{{$subCategory->id}}/{{$subCategory->category_id}}" >{{' '.$subCategory->name}}</label><i class="color_1"></i></a></li>
                                @endforeach                              
                            @else
                            <li>Sin Subcategorías</li>
                            @endif
     
                        </ul>
					</div>
				</div><!--End collapse -->
			</div><!--End filters col-->
		</div><!--End col-md -->
        
		<div class="col-md-9">
            <div class="row"> 
                @foreach ($stores as $store)
                <div class="col-md-6 col-sm-6 wow zoomIn" data-wow-delay="0.1s">
                @if ($store->category_id == 3)
                <a class="strip_list grid" href="/favor/{{$store->id}}">    
                {{-- @elseif($store->category_id == 6)
                <a class="strip_list grid" href="/supermarket/{{$store->id}}">    --}}
                @else
                <a class="strip_list grid" href="/detail_page/{{$store->id}}">
                @endif
                                <div class="contenedor-image">
                                    <img class="image-fav" src="{{ asset('/storage/'.$store->cover) }}" alt="" />
                                    <div class="centrado">{{$store->name}}</div>
                                </div>               
                        <div class="rating">
                            @for ($i = 0; $i < $store->stars; $i++)
                                <i class="icon_star voted"></i>
                            @endfor
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
		</div><!-- End col-md-9-->
        
	</div><!-- End row -->
</div><!-- End container -->
<!-- End Content =============================================== -->
</div>
@endsection
@section('css')
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endsection