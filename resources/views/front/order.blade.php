@extends('layouts.app')

@section('content')
<div class="white_bg">
    <div class="container margin_60_35">
        <div class="main_title">
            <h2 class="nomargin_top">{{$tittleOrder[0]->value}}</h2>
        </div>
        @include('common.msg')
        <div style="border: 1px solid #ccc; margin-top: 30px;">
        <div class="strip_list">
            @foreach ($orders as $order)
            <div class="row">
                <div class="col-md-1 col-sm-1"></div>
                <div class="col-md-8 col-sm-8">
                    <h5 class="black-word">Pedido actual</h5>
                </div>
                <div class="col-md-2 col-sm-2" style="margin-top: 10px;">
                    @if ($order->status == 'Sin Pagar')
                    <a href="/payment/order/{{$order->id}}">
                        <button type="submit" class="btn_3"><i class="icon-money"></i>{{ __('Pagar') }}</button>
                    </a>
                    @else
                    <a href="/detail_order/{{$order->id}}">
                        <button type="submit" class="btn_3"><i class="icon-plus-circled"></i>{{ __('Ver más') }}
                        </button>
                        </a>
                    @endif
              
                </div>
                <div class="col-md-1 col-sm-1"></div>
                <div class="col-md-1 col-sm-1"></div>
                @if(count($order->repartidor) > 0)
                    <div class="col-md-5 col-sm-5">
                        <div class="desc">
                           
                            <div style="isplay: inline-block;
                            float: left;">
                                <div>
                                <img class="icono-welcome-3" src="{{asset('/storage/'.$order->repartidor[0]->avatar)}}" alt="icono">
                                </div>                
                            </div>
                                {{-- <a href="/detail_order/{{$order->id}}"><img  src="{{ asset('/storage/'.$order->repartidor[0]->avatar) }}" alt=""></a> --}}
                          
                                <h3>{{$order->repartidor[0]->name}}</h3>
                            <div class="rating">
                                @for ($i = 0; $i < $order->repartidor[0]->stars; $i++)
                                <i class="icon_star voted"></i>
                                @endfor
                            </div>
                            <div class="type">
                                {{$order->repartidor[0]->enrollment}}
                            </div>
                        </div>
                    </div>                  
                @else
                <div class="col-md-5 col-sm-5">
                    <div class="desc">
                            <h3>Sin Confirmar repartidor</h3>
                    </div> 
                </div>  
                @endif
                
              
                  <div class="col-md-6 col-sm-6">
                    <div class="desc">
                        <div style="isplay: inline-block;
                            float: left;">
                                <div>
                                @if (isset($order->store->presentation_cover))
                                <img class="icono-welcome-3" src="{{asset('/storage/'.$order->store->presentation_cover)}}" alt="icono">
                                @else
                                <img class="icono-welcome-3" src="{{asset('img/favores.png')}}" alt="icono">   
                                @endif
                                </div>                
                        </div>
                        @if (isset($order->store->name))
                        <h3>{{$order->store->name}}</h3>
                        @else
                        <h3>{{$order->type}}</h3>
                        @endif
                      
                        @if (isset($order->store->stars))
                        <div class="rating">
                            @for ($i = 0; $i < $order->store->stars; $i++)
                            <i class="icon_star voted"></i>
                            @endfor
                        </div>
                        @else
                        <div class="rating">
                            @for ($i = 0; $i < 5; $i++)
                            <i class="icon_star voted"></i>
                            @endfor
                        </div>
                        @endif
                       
                    </div>
                </div>
      
                
                
                
                <div class="col-md-8 col-sm-8">
        
                </div>
                <div class="col-md-4 col-sm-4" style="margin-top: 4%">
                    <p>Descarga la app para tener contacto con ambos</p>
                </div>
            </div><!-- End row-->
            @endforeach
            @if (count($orders) == 0)
                <h1 style="text-align: center; margin-top: 20px; margin-bottom: 20px;">Sin pedidos actuales</h1>
            @endif
        </div><!-- End strip_list-->
        </div>
    </div>

    <div class="flexslider">
		<ul class="slides">
           
            <li>
             
				<section class="flex-caption">
					<div class="row" style="margin-top: 50px">
                        <div class="col-md-4 col-sm-4">
                        </div>
                        <div class="col-md-1 col-sm-1">
                           
                        <a href="/find-order/{{ date("Y").'-01' }}/Enero">
                            <button class="btn_2">{{ __('Enero') }}
                            </button> 
                            </a>
                            
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a href="/find-order/{{ date("Y").'-02' }}/Febrero">
                            <button class="btn_2">{{ __('Febrero') }}
                            </button>
                            </a>
                            
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a href="/find-order/{{ date("Y").'-03' }}/Marzo">
                            <button class="btn_2">{{ __('Marzo') }}
                            </button>
                            </a>
                            
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a href="/find-order/{{ date("Y").'-04' }}/Abril">
                            <button class="btn_2">{{ __('Abril') }}
                            </button>
                            </a>
                            
                        </div>
                        <div class="col-md-4 col-sm-4">
                        </div>
                    </div>
                </section>
                
            </li>  
            
            <li>
             
				<section class="flex-caption">
					<div class="row" style="margin-top: 50px">
                        <div class="col-md-4 col-sm-4">
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a href="/find-order/{{ date("Y").'-05' }}/Mayo">
                            <button class="btn_2">{{ __('Mayo') }}
                            </button>
                            </a>
                            
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a href="/find-order/{{ date("Y").'-06' }}/Junio">
                            <button class="btn_2">{{ __('Junio') }}
                            </button>
                            </a>
                            
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a href="/find-order/{{ date("Y").'-07' }}/Julio">
                            <button class="btn_2">{{ __('Julio') }}
                            </button>
                            </a>
                            
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a href="/find-order/{{ date("Y").'-08' }}/Agosto">
                            <button class="btn_2">{{ __('Agosto') }}
                            </button>
                            </a>
                            
                        </div>
                        <div class="col-md-4 col-sm-4">
                        </div>
                    </div>
                </section>
                
            </li>
            
            <li>
             
				<section class="flex-caption">
					<div class="row" style="margin-top: 50px">
                        <div class="col-md-4 col-sm-4">
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a href="/find-order/{{ date("Y").'-09' }}/Septiembre">
                            <button class="btn_2">{{ __('Septiembre') }}
                            </button>
                            </a>
                            
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a href="/find-order/{{ date("Y").'-10' }}/Octubre">
                            <button class="btn_2">{{ __('Octubre') }}
                            </button>
                            </a>
                            
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a href="/find-order/{{ date("Y").'-11' }}/Noviembre">
                            <button class="btn_2">{{ __('Noviembre') }}
                            </button>
                            </a>
                            
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a href="/find-order/{{ date("Y").'-12' }}/Diciembre">
                            <button class="btn_2">{{ __('Diciembre') }}
                            </button>
                            </a>
                            
                        </div>
                        <div class="col-md-4 col-sm-4">
                        </div>
                    </div>
                </section>
                
			</li>
           
		</ul>
    </div>
    <br><br><br>

    <div class="container margin_60_35_2">
        <div class="strip_list wow fadeIn" data-wow-delay="0.1s" style="border: none;">
            <div class="main_title">
                <h2 class="nomargin_top">{{$mes}}</h2>
            </div>
            <div class="row">
                @foreach ($findOrders as $findOrder)
                    <div class="col-md-4 col-sm-4" style="margin-top: 20px;">
                        <div class="desc">
                            <div class="thumb_strip">
                                @if (isset($findOrder->store->presentation_cover))
                                <a href="/detail_order/{{$findOrder->id}}"><img id="retaurante-min" src="{{ asset('/storage/'.$findOrder->store->presentation_cover) }}" alt=""></a>
                                @else
                                <a href="/detail_order/{{$findOrder->id}}"><img  id="retaurante-min" src="{{asset('img/favores.png')}}" alt="icono"></a> 
                                @endif
                            </div>
                            <div class="type">
                                {{$findOrder->delivery_date}}
                            </div>
                            @if (isset($findOrder->store->name))
                            <h3> {{$findOrder->store->name}}</h3>
                            @else
                            <h3>{{$findOrder->type}}</h3>
                            @endif
                            
                            <div class="type">
                                <h5 class="black-word"> Total ${{$findOrder->total_cost}} Envio ${{$findOrder->dalivery_cost}}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-12" style="margin-top: 10px;">
                        <a href="/detail_order/{{$findOrder->id}}">
                            <button type="submit" class="btn_3"><i class="icon-plus-circled "></i>{{ __('Ver más') }}
                            </button>
                        </a>
                    </div>
                @endforeach                          
            </div><!-- End row-->
            
        </div><!-- End strip_list-->
        @if (count($findOrders) == 0)
            <div class="col-md-12 sol-sm-12">
                <p style="text-align: center;">No hay pedidos en este mes.</p>
            </div>
        @endif
    </div>

</div>
@endsection
@section('css')
<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endsection