@extends('layouts.app')
@section('content')


<div class="white_bg">
    <div class="container margin_60_35">
        <!-- Content ================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div id="position">
                    <div class="container">
                        <a href="#0" class="search-overlay-menu-btn"><i class="icon-search-6"></i> Buscar</a>
                    </div>
                </div><!-- Position -->
                @foreach ($stores as $store)
                <div class="col-sm-6" style="padding-top: 5%;">
                    <div class="strip_list wow fadeIn" data-wow-delay="0.1s">
                            <div class="col-md-8 col-sm-8">
                                <div class="desc">
                                    <div class="thumb_strip">
                                        <a href="/detail_page/{{$store->id}}"><img id="retaurante-min" src="{{ asset('/storage/'.$store->logo) }}" alt=""></a>
                                    </div>
                                    <h3 class="text-food">{{$store->name}}</h3>
                                    @if (isset($store->subCategory->name))
                                        <small>{{$store->subCategory->name}}</small>
                                    @else
                                    <small>Sin subcategoria</small>    
                                    @endif
                                    <div class="rating">
                                        @for ($i = 0; $i < $store->stars; $i++)
                                            <i class="icon_star voted"></i>
                                        @endfor
                                        @for ($i = $store->stars; $i < 5; $i++)
                                            <i class="icon_star "></i>
                                        @endfor
                                    </div>
                                    <div class="type">
                                        {{substr($store->description, 0, 64)}}...
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="go_to"  style="min-width: 120px">
                                    <a href="/detail_page/{{$store->id}}" class="btn_1">Ver menú</a>
                                </div>
                            </div>
                    </div><!-- End strip_list-->
                </div>
                @endforeach
                @if (count($stores) == 0)
                <h1 style="text-align: center; margin-top: 20px; margin-bottom: 20px;">Sin productos actuales</h1>
                @endif
            </div><!-- End col-md-9-->
        </div><!-- End row -->
    </div><!-- End container -->
</div><!-- End white -->
<!-- End Content =============================================== -->

     <!-- Search Menu -->
     <div class="search-overlay-menu">
		<span class="search-overlay-close"><i class="icon_close"></i></span>
		{{-- <form role="search" id="searchform" method="get">
			<input value="" name="q" type="search" placeholder="Buscar..." />
			<button type="submit"><i class="icon-search-6"></i>
			</button>
        </form> --}}
        <form action="{{route('list.show')}}" method="POST">
            @csrf
            <div id="custom-search-input">
                <div class="input-group ">
                    <select class="search-query" required name="city_id" required autocomplete="city_id" autofocus>
                        @foreach($cities as $city)
                          <option value="{{$city->id}}">{{$city->name}}</option>
                        @endforeach
                    </select>
                    <span class="input-group-btn">
                    <input type="submit" class="btn_search" value="submit">
                    </span>
                </div>
            </div>
        </form>
    </div>
    
@endsection
@section('css')
<link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endsection