@extends('voyager::master')
@section('page_title', 'Reporte de Pedidos')
@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-receipt"></i>
        Reporte Pedidos
    </h1>
@stop


@section('javascript')
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
<script type="text/javascript" class="init">
      $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            
            buttons: [
                'excel', 
            ]
        });
} );
</script>
@endsection


@section('content')
<style>
    .dataTables_wrapper tfoot {
    display: table-footer-group !important;
}
</style>
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <form action="{{route('report.orders.search')}}" method="GET">
        @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3">
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Cliente" name="client_name" value="{{isset($_GET['client_name']) ? $_GET['client_name'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Apellidos" name="client_lastname" value="{{isset($_GET['client_lastname']) ? $_GET['client_lastname'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Dirección de entrega" name="destiny" value="{{isset($_GET['destiny']) ? $_GET['destiny'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Teléfono cliente" name="client_phone" value="{{isset($_GET['client_phone']) ? $_GET['client_phone'] : '' }}" />
                <select style="margin-top: 10px" class="form-control" name="status">
                    <option value="" selected>Selecciona un estatus</option>
                    <option value="Sin Pagar">Sin Pagar</option> 
                    <option value="Pendiente">Pendiente</option> 
                    <option value="Entregado">Entregado</option>
                    <option value="Completado">Completado</option>
                    <option value="Atendida">Atendida</option>
                    <option value="En Camino">En Camino</option>
                    <option value="Carrito">Carrito</option>
                  </select>
            </div>
            <div class="col-md-3">
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Establecimiento" name="store" value="{{isset($_GET['store']) ? $_GET['store'] : '' }}" />
                <select style="margin-top: 10px" class="form-control"  name="city_id"> 
                    <option value="" selected>Selecciona una ciudad</option>                    
                    @foreach($cities as $city)
                    <option value="{{$city->id}}">{{$city->name}}</option>
                    @endforeach
                </select>
                <input  style="margin-top: 10px" class="form-control" type="text" placeholder="Comentarios" name="comments" value="{{isset($_GET['comments']) ? $_GET['comments'] : '' }}" />
                <input  style="margin-top: 10px" class="form-control" type="text" placeholder="Costo del pedido" name="total_cost" value="{{isset($_GET['total_cost']) ? $_GET['total_cost'] : '' }}" />
                <input  style="margin-top: 10px" class="form-control" type="text" placeholder="Propina" name="tip" value="{{isset($_GET['tip']) ? $_GET['tip'] : '' }}" /> 
            </div>
            <div class="col-md-3">
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Repatidor" name="delivery_name" value="{{isset($_GET['delivery_name']) ? $_GET['delivery_name'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Apellidos" name="delivery_last_name" value="{{isset($_GET['delivery_last_name']) ? $_GET['delivery_last_name'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Repartidor Teléfono" name="delivery_phone" value="{{isset($_GET['delivery_phone']) ? $_GET['delivery_phone'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="envio" name="delivery_cost" value="{{isset($_GET['delivery_cost']) ? $_GET['delivery_cost'] : '' }}" />
            </div>
            <div class="col-md-3">
                <input style="margin-top: 10px" class="form-control" type="date" placeholder="Fecha Inicio" name="dateStart" value="{{isset($_GET['dateStart']) ? $_GET['dateStart'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="date" placeholder="Fecha Fin" name="dateEnd" value="{{isset($_GET['dateEnd']) ? $_GET['dateEnd'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Costo Total" name="final_price" value="{{isset($_GET['final_price']) ? $_GET['final_price'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Horario 15:00 - 16:00" name="time" value="{{isset($_GET['time']) ? $_GET['time'] : '' }}" />
                
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <button class="btn_1" type="submit">Buscar</button>
            </div>
        </div>
    </div>
    </form>
<table id="example" class="display" class="table table-hover table-striped table-bordered">
    <thead>
        <tr>
            <th>Folio</th>
            <th>Cliente</th>
            <th>Dirección de entrega</th>
            <th>Teléfono</th>
            <th>Establecimiento</th>
            <th>Ciudad</th>
            <th style="text-align: center">Productos</th>
            <th>Comentarios</th>
            <th>Fecha de entrega</th>
            <th>Hora de entega</th>
            <th>Repartidor</th>
            <th>Teléfono</th>
            <th>Costo del pedido</th>
            <th>Costo de envio</th>
            <th>Propina</th>
            <th>Costo Total</th>
            <th>Pago Aliado</th>
            <th>Tiempo</th>
            <th>Estatus</th>
            <th>Tipo</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($orders as $order)
            <tr>
                @if(isset($order->id))
                <td>{{$order->id}}</td>
                @else
                    <td>Sin Id</td>
                @endif
                @if(isset($order->user->name) || isset($order->user->last_name))
                <td>{{$order->user->name}} {{$order->user->last_name}}</td>
                @else
                    <td>Sin cliente</td>
                @endif
                @if(isset($order->address))
                    <td>{{$order->address->address}}</td>
                @else
                    <td>Sin dirección</td>
                @endif    
                @if(isset($order->phone))
                    <th>{{$order->phone}}</th>
                @else
                    <td>Sin teléfono</td>
                @endif
                @if(isset($order->store->name))
                    <th>{{$order->store->name}}</th>
                @else
                    <td>Sin establecimiento</td>
                @endif
                @if(isset($order->store->city))
                    <td>{{$order->store->city->name}}</td>
                @else
                    <td>Sin ciudad</td>
                @endif
                @if(count($order->products) > 0)
                <td WIDTH="600">
                    @foreach ($order->products as $product)
                        x{{$product->pivot->qty}} {{$product->name}} Q{{$product->price}} <br>
                            @foreach ($product->pivot->options as $option)                          
                                    <p style="margin-left: 15px;">x{{$option->pivot->qty}} {{$option->name}} Q {{$option->price}}</p>
                            @endforeach
                    @endforeach
                </td>
                @else
                <td>Sin productos</td>
                @endif
                @if(isset($order->comments))
                    <td>{{$order->comments}}</td>
                @else
                    <td>Sin comentarios</td>
                @endif
                @if(isset($order->delivery_date))
                <td>{{$order->delivery_date}}</td>
                @else
                    <td>Sin fecha de entrega</td>
                @endif
                @if(isset($order->delivery_hour))
                <td>{{$order->delivery_hour}}</td>
                @else
                    <td>Sin hora de entrega</td>
                @endif
                @if(count($order->repartidor) > 0)
                <td>{{$order->repartidor[0]->name}} {{$order->repartidor[0]->last_name}}</td>
                <td>{{$order->repartidor[0]->phone}}</td>
                @else
                <td>Sin repartidor</td>
                <td>Sin teléfono</td>
                @endif
                @if(isset($order->total_cost))
                <td>{{$order->total_cost}}</td>
                @else
                    <td>Sin costo del pedido</td>
                @endif
                @if(isset($order->dalivery_cost))
                    <td>{{$order->dalivery_cost}}</td>
                @else
                    <td>Sin costo de envío</td>
                @endif
                @if(isset($order->tip))
                    <td>{{$order->tip}}</td>
                @else
                    <td>Sin propina</td>
                @endif
                @if(isset($order->total_cost) && isset($order->dalivery_cost) && isset($order->tip))
                <td><?php echo $order->total_cost + $order->dalivery_cost + $order->tip; ?></td>
                @else
                    <td>No se puede calcular total</td>
                @endif
                @if(isset($order->total_cost)  && $order->status == 'Completado')
                <td><?php 
                     $porcent =  $taxOrder[0]->value / 100;   
                     $desc = $order->total_cost * $porcent;
                     echo "Q".$tot = $order->total_cost - $desc;
                ?></td>
                @else
                    <td>Sin pago, orden no completada</td>
                @endif
                @if(count($order->repartidor) > 0)
                <td>
                    <?php $horaInicio  = new DateTime( $order->repartidor[0]->pivot->time_start); 
                          $horaTermino = new DateTime( $order->repartidor[0]->pivot->time_end); 
                          $interval = $horaInicio->diff($horaTermino);
                          echo $interval->format('%H horas %i minutos %s segundos');
                    ?>
                </td>
                @else
                <td>Aún no esta completado el pedido</td>
                @endif
                @if(isset($order->status))
                <td>{{$order->status}}</td> 
                @else
                    <td>Sin estatus</td>
                @endif
                @if(isset($order->type))
                <td>{{$order->type}}</td> 
                @else
                    <td>Sin tipo</td>
                @endif
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td>Total:</td>
            <td></td>
            <td></td>
            <td></td> 
            <td></td>
            <td></td> 
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalOrder}}</td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalDelivery}}</td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalTip}}</td>
            <td style="padding: 10px 10px 6px !important;">Q {{$total}}</td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalAlia}}</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
      </tfoot>
</table>
@stop
