@extends('voyager::master')
@section('page_title', 'Reporte de Ventas')

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-dollar"></i>
        Ventas 
    </h1>
@stop


@section('javascript')
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
<script type="text/javascript" class="init">
      $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            
            buttons: [
                'excel', 
            ]
        });
} );
</script>
@endsection


@section('content')
<style>
    .dataTables_wrapper tfoot {
    display: table-footer-group !important;
}
</style>
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <form action="{{route('report.cutDelivery.search')}}" method="GET">
        @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">
                
            </div>
            <div class="col-md-4">
                <select style="margin-top: 10px" class="form-control"  name="city_id"> 
                    <option value="" selected>Selecciona una ciudad</option>                    
                    @foreach($cities as $city)
                    <option value="{{$city->id}}">{{$city->name}}</option>
                    @endforeach
                </select>
                <select style="margin-top: 10px" class="form-control" name="type">
                    <option value="" selected>Selecciona un tipo</option> 
                    <option value="Mandado">Mandado</option> 
                    <option value="Pedido">Pedido</option>
                    <option value="Super">Supermercado</option>
                  </select>
            </div>
            <div class="col-md-4">
                <input style="margin-top: 10px" class="form-control" type="date" placeholder="Fecha Inicio" name="dateStart" value="{{isset($_GET['dateStart']) ? $_GET['dateStart'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="date" placeholder="Fecha Fin" name="dateEnd" value="{{isset($_GET['dateEnd']) ? $_GET['dateEnd'] : '' }}" />
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <button class="btn_1" type="submit">Buscar</button>
            </div>
        </div>
    </div>  
    </form>
    
<table id="example" class="display" class="table table-hover table-striped table-bordered">
    <thead>
        <tr>
            <th>Folio</th>
            <th>Ciudad</th>
            <th>Comisión por envío</th>
            <th>Comisión por aliado</th>
            <th>Costo del Pedido</th>
            <th>Costo de envío</th>
            <th>Propina</th>
            <th>Pago motorista</th>
            <th>Pago del Pedido</th>
            <th>Ganancia acasa</th>
            <th>Estatus</th>
            <th>Tipo</th>
            <th>Fecha de entrega</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($orders as $order)
            <tr>
                @if(isset($order->id))
                <td>{{$order->id}}</td>
                @else
                    <td>Sin Id</td>
                @endif
                @if(isset($order->user->city->name))
                <td>{{$order->user->city->name}}</td>
                @else
                    <td>Sin Ciudad</td>
                @endif
                <td>{{$tax[0]->value}}%</td>
                <td>{{$taxOrder[0]->value}}%</td>
                @if(isset($order->total_cost))
                    <td>Q{{$order->total_cost}}</td>
                @else
                    <td>Sin costo del pedido</td>
                @endif
                @if(isset($order->dalivery_cost))
                    <td>Q{{$order->dalivery_cost}}</td>
                @else
                    <td>Sin costo de envío</td>
                @endif
                @if(isset($order->tip))
                    <td>Q{{$order->tip}}</td>
                @else
                    <td>Sin propina</td>
                @endif
                @if(isset($order->dalivery_cost))
                    <td><?php
                     $porcent =  $tax[0]->value / 100;   
                     $desc = $order->dalivery_cost * $porcent;
                     echo "Q".$tot = ($order->dalivery_cost - $desc) + $order->tip;
                    ?></td>
                @else
                    <td>Sin total de motorista</td>
                @endif
                @if(isset($order->total_cost) && $order->type == 'Pedido')
                    <td><?php
                     $porcent =  $taxOrder[0]->value / 100;   
                     $desc = $order->total_cost * $porcent;
                     echo "Q".$tot = $order->total_cost - $desc;
                    ?></td>
                @elseif(isset($order->total_cost) && $order->type != 'Pedido')
                    <td>Q{{$order->total_cost}}</td>
                @else
                    <td>Sin total de pedido</td>
                @endif
                @if(isset($order->total_cost)&& $order->type == 'Pedido')
                    <td><?php
                     $porcentEnv =  $tax[0]->value / 100; 
                     $porcentPedi =  $taxOrder[0]->value / 100;   
                     $descEnv = $order->dalivery_cost * $porcentEnv;
                     $descPedi = $order->total_cost * $porcentPedi;
                     echo "Q".$tot = $descEnv + $descPedi;
                    ?></td>
                @elseif(isset($order->total_cost) && $order->type != 'Pedido')
                    <td><?php
                     $porcentEnv =  $tax[0]->value / 100;  
                     $descEnv = $order->dalivery_cost * $porcentEnv;
                     echo "Q".$tot = $descEnv + $order->total_cost;
                    ?></td>
                @else
                    <td>Sin ganancia para acasa</td>
                @endif
                @if(isset($order->status))
                    <td>{{$order->status}}</td> 
                @else
                    <td>Sin estatus</td>
                @endif
                @if(isset($order->type))
                <td>{{$order->type}}</td> 
                @else
                    <td>Sin tipo</td>
                @endif
                @if(isset($order->delivery_date))
                <td>{{$order->delivery_date}}</td>
                @else
                    <td>Sin hora de entrega</td>
                @endif
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td>Total:</td>
            <td></td>
            <td></td>
            <td></td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalCost}}</td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalDelivery}}</td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalTip}}</td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalMoto}}</td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalAlia}}</td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalAca}}</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
      </tfoot>
</table>
@stop
