@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-shop"></i>
        Corte de Caja Mandados y Supermercado
    </h1>
@stop

@section('javascript')
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
<script type="text/javascript" class="init">
      $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            
            buttons: [
                'excel', 
            ]
        });
} );
</script>
@endsection


@section('content')
<style>
    .dataTables_wrapper tfoot {
    display: table-footer-group !important;
}
</style>
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <form action="{{route('report.cutFavot.search')}}" method="GET">
        @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
                <input style="margin-top: 10px" class="form-control" type="date" placeholder="Fecha Inicio" name="dateStart" value="{{isset($_GET['dateStart']) ? $_GET['dateStart'] : '' }}" />
            </div>
            <div class="col-md-4">
                <input style="margin-top: 10px" class="form-control" type="date" placeholder="Fecha Fin" name="dateEnd" value="{{isset($_GET['dateEnd']) ? $_GET['dateEnd'] : '' }}" />
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <button class="btn_1" type="submit">Buscar</button>
            </div>
        </div>
    </div>  
    </form>
<table id="example" class="display" class="table table-hover table-striped table-bordered">
    <thead>
        <tr>
            <th>Folio</th>
            <th>Repartidor</th>
            <th>Rol</th>
            <th>Ciudad</th>
            <th>Placa</th>
            <th>Costo total</th>
            <th>Total acasa</th>
            <th>Estatus</th>
            <th>Tipo</th>
            <th>Fecha de entrega</th>
            <th>Fecha de corte</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($orders as $order)
            <tr>
                @if(isset($order->id))
                <td>{{$order->id}}</td>
                @else
                    <td>Sin Id</td>
                @endif
                @if(count($order->repartidor) > 0)
                <td>{{$order->repartidor[0]->name}} {{$order->repartidor[0]->last_name}}</td>
                <td>Motorista</td>
                <td>{{$order->repartidor[0]->city->name}}</td>
                <td>{{$order->repartidor[0]->enrollment}}</td>
                @else
                <td>Sin repartidor</td>
                <td>Sin rol</td>
                <td>Sin ciudad</td>
                <td>Sin Placa</td>
                @endif
                @if(isset($order->total_cost))
                    <td>Q{{$order->total_cost}}</td>
                @else
                    <td>Sin costo total</td>
                @endif
                @if(isset($order->total_cost))
                    <td>Q{{$order->total_cost}}</td>
                @else
                    <td>Sin costo total</td>
                @endif
                @if(isset($order->status))
                <td>{{$order->status}}</td> 
                @else
                    <td>Sin estatus</td>
                @endif
                @if(isset($order->type))
                <td>{{$order->type}}</td> 
                @else
                    <td>Sin tipo</td>
                @endif
                @if(isset($order->delivery_date))
                <td>{{$order->delivery_date}}</td>
                @else
                    <td>Sin hora de entrega</td>
                @endif
                <td>{{$order->sales_cut_date_favor}}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td>Total:</td>
            <td></td>
            <td></td>
            <td></td> 
            <td></td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalEnvio}}</td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalAca}}</td>
            <td></td>
            <td></td> 
            <td></td>
            <td></td>
        
       
        </tr>
      </tfoot>
</table>
<div class="col-md-12">
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
    <div class="col-md-3">
        <button class="btn_1" style="background: rgb(4, 121, 255);">Hacer Corte</button>
    </div>
</div>
@stop
