@extends('voyager::master')
@section('page_title', 'Reporte de Repartidores')

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-people"></i>
        Reporte Repartidor
    </h1>
@stop

@section('javascript')
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
<script type="text/javascript" class="init">
      $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            
            buttons: [
                'excel', 
            ]
        });
} );
</script>
@endsection


@section('content')
<style>
    .dataTables_wrapper tfoot {
    display: table-footer-group !important;
}
</style>
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
{{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css"> --}}

    <form action="{{route('report.delivery.search')}}" method="GET">
        @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3">
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="nombre" name="name" value="{{isset($_GET['name']) ? $_GET['name'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Apellidos" name="last_name" value="{{isset($_GET['last_name']) ? $_GET['last_name'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Ubicación actual" name="address" value="{{isset($_GET['address']) ? $_GET['address'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Dirección" name="direction" value="{{isset($_GET['direction']) ? $_GET['direction'] : '' }}" />
            </div>
            <div class="col-md-3">
                <input  style="margin-top: 10px" class="form-control" type="text" placeholder="Envio" name="cost" value="{{isset($_GET['cost']) ? $_GET['cost'] : '' }}" />
                <input  style="margin-top: 10px" class="form-control" type="text" placeholder="Total" name="total" value="{{isset($_GET['total']) ? $_GET['total'] : '' }}" />
                <input  style="margin-top: 10px" class="form-control" type="text" placeholder="Propina" name="tip" value="{{isset($_GET['tip']) ? $_GET['tip'] : '' }}" /> 
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Placa" name="enrollment" value="{{isset($_GET['enrollment']) ? $_GET['enrollment'] : '' }}" />
            </div>
            <div class="col-md-3">
                <select style="margin-top: 10px" class="form-control" name="status">
                    <option value="" selected>Selecciona un estatus</option>
                    <option value="Sin Pagar">Sin Pagar</option> 
                    <option value="Pendiente">Pendiente</option> 
                    <option value="Entregado">Entregado</option>
                    <option value="Completado">Completado</option>
                    <option value="Atendida">Atendida</option>
                    <option value="En Camino">En Camino</option>
                    <option value="Carrito">Carrito</option>
                  </select>
                  <select style="margin-top: 10px" class="form-control" name="type">
                    <option value="" selected>Selecciona un tipo</option> 
                    <option value="Mandado">Mandado</option> 
                    <option value="Pedido">Pedido</option>
                    <option value="Super">Supermercado</option>
                  </select>
                  <select style="margin-top: 10px" class="form-control"  name="city_id"> 
                    <option value="" selected>Selecciona una ciudad</option>                    
                    @foreach($cities as $city)
                    <option value="{{$city->id}}">{{$city->name}}</option>
                    @endforeach
                 </select>
                 <input style="margin-top: 10px" class="form-control" type="text" placeholder="Teléfono" name="phone" value="{{isset($_GET['phone']) ? $_GET['phone'] : '' }}" />
            </div>
            <div class="col-md-3">
                <input style="margin-top: 10px" class="form-control" type="date" placeholder="Fecha Inicio" name="dateStart" value="{{isset($_GET['dateStart']) ? $_GET['dateStart'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="date" placeholder="Fecha Fin" name="dateEnd" value="{{isset($_GET['dateEnd']) ? $_GET['dateEnd'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Distancia km" name="km" value="{{isset($_GET['km']) ? $_GET['km'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Horas 00:00:00" name="time" value="{{isset($_GET['time']) ? $_GET['time'] : '' }}" />
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <button class="btn_1" type="submit">Buscar</button>
            </div>
        </div>
    </div>
    </form>
    <div class="col-md-12">
        <div class="col-md-3"></div>
        <div class="col-md-3"></div>
        <div class="col-md-3"></div>
    </div>
<table id="example" class="display" class="table table-hover table-striped table-bordered">
    <thead>
        <tr>
            <th>Folio</th>
            <th>Repartidor</th>
            <th>Envio</th>
            <th>Total de pedido</th>
            <th>Propina</th>
            <th>Pago Motorista</th>
            <th>Fecha de entrega</th>
            <th>Distancia KM</th>
            <th>Tiempo de entrega</th>
            <th>Ubicación Actual</th>
            <th>Estatus del pedido</th>
            <th>Tipo de pedido</th>
            <th>Ciudad</th>
            <th>Dirección</th>
            <th>Placa</th>
            <th>Teléfono</th>
            <th>banco</th>
            <th>Cuenta de banco</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($deliveries as $delivery)
            <tr>
                <td>{{$delivery->order->id}}</td>
                @if(isset($delivery->user->name) || isset($delivery->user->last_name))
                    <td>{{$delivery->user->name}} {{$delivery->user->last_name}}</td>
                @else
                    <td>Sin repartidor</td>
                @endif
                @if(isset($delivery->order->dalivery_cost))
                    <td width="100">Q{{$delivery->order->dalivery_cost}}</td>
                @else
                    <td width="100">Sin costo de envío</td>
                @endif
                @if(isset($delivery->order->total_cost))
                    <td>Q{{$delivery->order->total_cost}}</td>
                @else
                    <td>Sin total de pedido</td>
                @endif
                @if(isset($delivery->order->tip))
                    <td>Q{{$delivery->order->tip}}</td>
                @else
                    <td>Sin propina</td>
                @endif
                @if(isset($delivery->order->dalivery_cost) && $delivery->order->status == 'Completado')
                    <td><?php
                     $porcent =  $tax[0]->value / 100;   
                     $desc = $delivery->order->dalivery_cost * $porcent;
                     echo "Q".$tot = ($delivery->order->dalivery_cost - $desc) + $delivery->order->tip;
                    ?></td>
                @else
                    <td>Sin pago, orden no completada</td>
                @endif
                @if(isset($delivery->order->delivery_date))
                    <td>{{$delivery->order->delivery_date}}</td>
                @else
                    <td>Sin fecha de entrega</td>
                @endif
                @if(isset($delivery->order->km))
                    <td>{{$delivery->order->km}} km</td>
                @else
                    <td>Sin kilometros</td>
                @endif
                @if(isset($delivery->time_start) && isset($delivery->time_end))
                    <td><?php
                    $horaInicio = new DateTime($delivery->time_start);
                    $horaTermino = new DateTime($delivery->time_end);
                    $interval = $horaInicio->diff($horaTermino);
                    echo $interval->format('%H horas %i minutos %s segundos');
                    ?></td>
                @else
                    <td>No se pudo obtener el tiempo</td>
                @endif
                @if (isset($delivery->address))
                    <td>{{$delivery->address}}</td>    
                @else
                    <td>Sin dirección actual</td>
                @endif
                @if (isset($delivery->order->status))
                    <td>{{$delivery->order->status}}</td>
                @else
                    <td>Sin estatus</td>
                @endif
                @if (isset($delivery->order->type))
                    <td>{{$delivery->order->type}}</td>
                @else
                    <td>Sin tipo</td>
                @endif
                @if (isset($delivery->user->city->name))
                    <td>{{$delivery->user->city->name}}</td>
                @else
                    <td>Sin ciudad</td>
                @endif
                @if (isset($delivery->user->address))
                    <td>{{$delivery->user->address}}</td>
                @else
                    <td>Sin dirección</td>
                @endif
                @if (isset($delivery->user->enrollment))
                    <td>{{$delivery->user->enrollment}}</td>
                @else
                    <td>Sin placa</td>
                @endif
                @if (isset($delivery->user->phone))
                    <td>{{$delivery->user->phone}}</td>
                @else
                    <td>Sin teléfono</td>
                @endif
                @if (isset($delivery->user->bank->name))
                    <td>{{$delivery->user->bank->name}}</td>
                @else
                    <td>Sin Banco</td>
                @endif

                @if (isset($delivery->user->bank_account))
                    <td>{{$delivery->user->bank_account}}</td>
                @else
                    <th>Sin cuenta de banco</th>
                @endif
                
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td>Total:</td>
            <td></td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalDelivery}}</td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalOrder}}</td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalTip}}</td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalMoto}}</td>
            <td></td>
            <td></td> 
            <td></td>
            <td></td> 
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
      </tfoot>
</table>
@stop
