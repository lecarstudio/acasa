@extends('voyager::master')
@section('page_title', 'Reporte de Corte de Aliados')

@section('page_header')
    <h1 class="page-title">
        <i class="icon voyager-treasure"></i>
        Corte de Caja Aliados
    </h1>
@stop

@section('javascript')
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
<script type="text/javascript" class="init">
      $(document).ready(function() {
        $('#example').DataTable( {
            dom: 'Bfrtip',
            
            buttons: [
                'excel', 
            ]
        });
} );
</script>
@endsection


@section('content')
<style>
    .dataTables_wrapper tfoot {
    display: table-footer-group !important;
}
</style>
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <form action="{{route('report.cutStore.search')}}" method="GET">
        @csrf
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">
            <input style="margin-top: 10px" class="form-control" type="text" placeholder="Nombre Propietario" name="property_name" value="{{isset($_GET['property_name']) ? $_GET['property_name'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="text" placeholder="Apellidos" name="property_last_name" value="{{isset($_GET['property_last_name']) ? $_GET['property_last_name'] : '' }}" />
            </div>
            <div class="col-md-4">
                <select style="margin-top: 10px" class="form-control"  name="city_id"> 
                    <option value="" selected>Selecciona una ciudad</option>                    
                    @foreach($cities as $city)
                    <option value="{{$city->id}}">{{$city->name}}</option>
                    @endforeach
                </select>
                <input  style="margin-top: 10px" class="form-control" type="text" placeholder="Estalecimiento" name="store" value="{{isset($_GET['store']) ? $_GET['store'] : '' }}" /> 
            </div>
            <div class="col-md-4">
                <input style="margin-top: 10px" class="form-control" type="date" placeholder="Fecha Inicio" name="dateStart" value="{{isset($_GET['dateStart']) ? $_GET['dateStart'] : '' }}" />
                <input style="margin-top: 10px" class="form-control" type="date" placeholder="Fecha Fin" name="dateEnd" value="{{isset($_GET['dateEnd']) ? $_GET['dateEnd'] : '' }}" />
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <button class="btn_1" type="submit">Buscar</button>
            </div>
        </div>
    </div>  
    </form>
<table id="example" class="display" class="table table-hover table-striped table-bordered">
    <thead>
        <tr>
            <th>Folio</th>
            <th>Establecimiento</th>
            <th>Propietario</th>
            <th>Rol</th>
            <th>Ciudad</th>
            <th>Banco</th>
            <th>Cuenta de banco</th>
            <th>Nit</th>
            <th>Dirección Fiscal</th>
            <th>Costo de pedido</th>
            <th>Comisión por pedido</th>
            <th>Total aliado</th>
            <th>Total acasa</th>
            <th>Estatus</th>
            <th>Tipo</th>
            <th>Fecha de entrega</th>
            <th>Fecha de corte</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($orders as $order)
            <tr>
                @if(isset($order->id))
                <td>{{$order->id}}</td>
                @else
                    <td>Sin Id</td>
                @endif
                <th>{{$order->store->name}}</th>
                <td>{{$order->store->user->name}} {{$order->store->user->last_name}}</td>
                <td>Aliado</td>
                <td>{{$order->store->user->city->name}}</td>
                <td>{{$order->store->user->bank->name}}</td>
                <td>{{$order->store->user->bank_account}}</td>
                <td>{{$order->store->user->nit}}</td>
                <td>{{$order->store->user->fiscal_direction}}</td>
                <td>Q{{$order->total_cost}}</td>
                <td>{{$tax[0]->value}}%</td>
                <td><?php
                    $porcent =  $tax[0]->value / 100;   
                    $desc = $order->total_cost * $porcent;
                    echo "Q".$tot = $order->total_cost - $desc;
                ?></td>
                <td><?php
                    $porcent =  $tax[0]->value / 100;   
                    echo "Q".$desc = $order->total_cost * $porcent;
                ?></td>
                <td>{{$order->status}}</td>
                <td>{{$order->type}}</td>
                <td>{{$order->delivery_date}}</td>
                <td>{{$order->sales_cut_date_partner}}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td>Total:</td>
            <td></td>
            <td></td>
            <td></td> 
            <td></td>
            <td></td> 
            <td></td>
            <td></td>
            <td></td>
            <td style="padding: 10px 10px 6px !important;">Q {{$costTotal}}</td>
            <td></td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalPartner}}</td>
            <td style="padding: 10px 10px 6px !important;">Q {{$totalAcasa}}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
      </tfoot>
</table>
<div class="col-md-12">
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
    <div class="col-md-3">
        <button class="btn_1" style="background: rgb(4, 121, 255);">Hacer Corte</button>
    </div>
</div>
@stop
