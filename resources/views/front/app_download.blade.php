@extends('layouts.app')
@section('content')
<div class="white_bg">
<div class="container margin_60_35">
    <div class="main_title">
        <h2 class="nomargin_top">{{$tittle[0]->value}}</h2>
        <h2 class="nomargin_top">_______________________________________________</h2>
    </div>
    <p class="app-words">{{$subtittle[0]->value}}</p>
    <p  class="app-words">{{$description[0]->value}}</p>
</div>
</div>
<div class="container margin_60_35">
    <div class="row app-top">
       
		<div class="col-md-4 top-link">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h3 class="start-now">{{$slogan[0]->value}}</h3>
                </div>
                <div class="col-md-6 col-sm-6">
                    <button type="submit" class="btn_4">
                        <i class="icon-apple"></i> App Store
                    </button>
                    
                </div>
                <div class="col-md-6 col-sm-6">
                    <button type="submit" class="btn_4">
                        <i class="icon-android"></i>Google Play
                    </button>
                </div>
            </div>          
        </div>

        <div class="col-md-7 col-md-7 text-right ">
			<img src="{{ asset('/storage/'.$image[0]->value) }}" alt="" class="img-responsive" style="float: right;">
		</div>
		
    </div><!-- End row -->
</div>
@endsection
@section('css')
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endsection