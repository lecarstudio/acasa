@extends('layouts.front')

@section('content')
    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="home" data-parallax="scroll" data-image-src="{{ asset('/storage/'.$imageHome) }}" 
    data-natural-width="1000" data-natural-height="300">
    <div id="subheader">
        <div id="sub_content">
            <h1>{{$tittlefinder[0]->value}}</h1>
            <p>
               {{$sloganfinder[0]->value}}
            </p>
            <form action="{{route('list.show')}}" method="POST">
                @csrf
                <div id="custom-search-input">
                    <div class="input-group ">
                        <select class="search-query" required name="city_id" required autocomplete="city_id" autofocus>
                            <option value="">{{$textfinder[0]->value}}</option>
                            @foreach($cities as $city)
                              <option value="{{$city->id}}">{{$city->name}}</option>
                            @endforeach
                        </select>
                        <span class="input-group-btn">
                        <input type="submit" class="btn_search" value="submit">
                        </span>
                    </div>
                </div>
            </form>
        </div><!-- End sub_content -->
    </div><!-- End subheader -->
    <div id="count" class="hidden-xs" style="display: none">
        <ul>
            <li><span class="number">0</span> Restaurantes</li>
            <li><span class="number">0</span> Comunidad</li>
            <li><span class="number">0</span> Usuarios Registrados</li>
        </ul>
    </div>
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->

    <div class="white_bg">
        <div class="row">
            <div class="container">
                <div class="center-content">
                    @foreach ($categories as $category)
                    <div class={{"col-md-".(12/count($categories))}}>
                        @if ($category->id == 3)
                        <a href="/favor/{{$category->id}}">
                        @else
                        <a href="/categories/{{$category->id}}">
                        @endif
                        
                            <div class="feature">
                                <div class="center-aling">
                                <img class="icono-welcome" src="{{ asset('/storage/'.$category->image) }}" alt="icono">
                                </div>                
                                <h3><span>{{$category->name}}</span></h3>
                            </div>
                        </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div><!-- End row -->
    </div>


    <div class="white_bg">
        <div class="container margin_60">
        <div class="flexslider">
            <ul class="slides">
                @foreach ($advertisements as $advertisement)
                <li>
                    <a href="/detail_page/{{$advertisement->store_id}}">
                    <img id="anuncio-min" src="{{ asset('/storage/'.$advertisement->images) }}" alt="">
                    <section class="flex-caption">
                        <p></p>
                    </section>
                    </a>
                </li>  
                @endforeach	
            </ul>
        </div>
        </div>
        </div>


     
 
    <div class="white_bg">
        <div class="container margin_60">
            <div class="main_title">
               <h2 class="nomargin_top">{{$topTittleStore[0]->value}}</h2>
            </div>
            
            <div class="row">
                @foreach ($stores as $store)
                <div class="col-md-6" style="margin-top: 5%;">
                    <a href="/detail_page/{{$store->id}}" class="strip_list">
                    <div style="display: none" class="ribbon_1">Tendencias</div>
                        <div class="desc">
                            <div class="thumb_strip">
                                <img id="retaurante-min" src="{{ asset('/storage/'.$store->presentation_cover) }}" alt="">
                            </div>
                            <div class="rating">
                            @for ($i = 0; $i < $store->stars; $i++)
                            <i class="icon_star voted"></i>
                            @endfor
                            </div>
                            <h3>{{$store->name}}</h3>
                            <div class="type">
                                @if (isset($store->subcategory->name))
                                {{$store->subcategory->name}}
                                @else
                                
                                @endif
                               
                            </div>
                            <div class="location">
                                Abre a las {{$store->horary_open}} <span class="opening">Cierra a las  {{$store->horary_close}}</span>
                            </div>
                        </div><!-- End desc-->
                    </a><!-- End strip_list-->
                </div><!-- End col-md-6-->
                @endforeach
            </div><!-- End row -->   
            
        </div><!-- End container -->
        </div><!-- End white_bg -->
   

<div class="white_bg">
<!-- Content ================================================== -->
<div class="container margin_60_35">
    <div class="main_title">
        <h2 class="nomargin_top">{{$tittlePromotion[0]->value}}</h2>
    </div>
	<div class="row" id="contacts">
        @foreach ($promotions as $promotion)
        <div class="col-md-6 col-sm-6">
        	<div class="box_style_2">
                <a href="/detail_page/{{$promotion->product->store_id}}" class="phone">
            	<h2 class="inner">{{$promotion->product->name}}</h2>
                <img src="{{ asset('/storage/'.$promotion->image) }}" width="848" height="480" alt="" class="img-responsive">
                <div class="col-md-10 col-sm-10">
                    <p style="font-size: 18px; text-align: justify;">{{$promotion->product->short_description}}</p>
                </div>
                <div class="col-md-2 col-sm-2">
                    <p style="color: #e91e63; font-weight: bold; font-size: 34px;">Q{{$promotion->price}}</p>
                </div>
                
                </a>
            </div>
    	</div>
        @endforeach
    </div><!-- End row -->
</div><!-- End container -->
<!-- End Content =============================================== -->
</div>
        <div class="high_light">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="image-welcome">
                            <img src="{{ asset('/storage/'.$imageApp[0]->value) }}" alt="phone">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="">
                            <h3>{{$tittleApp[0]->value}}</h3>
                            <a href="index.html"><i class="icon-apple"></i> App Store</a>
                            <a href="index.html"><i class="icon-android"></i>Google Play</a>
                        </div><!-- End container -->
                    </div>
                </div>
          </div><!-- End row -->
        </div><!-- End hight_light -->

       
    
	
	<div class="container margin_60">
      	<div class="row">
            @auth
            <div class="col-md-4 col-md-offset-2">
            	<a class="box_work" href="#0" data-toggle="modal" data-target="#alert">
                <img src="{{ asset('/storage/'.$imagePartner[0]->value) }}" width="848" height="480" alt="" class="img-responsive">
                <h3>{{$tittlePartner[0]->value}}<span></span></h3>
                <p class="bottom-welcome">{{$sloganPartner[0]->value}}</p>
                <div class="btn_1">{{$bottomPartner[0]->value}}</div>
                </a>
            </div>
            @endauth
            
            @guest
            <div class="col-md-4 col-md-offset-2">
            	<a class="box_work" href="#0" data-toggle="modal" data-target="#registerPartnerModal2">
                <img src="{{ asset('/storage/'.$imagePartner[0]->value) }}" width="848" height="480" alt="" class="img-responsive">
                <h3>{{$tittlePartner[0]->value}}<span></span></h3>
                <p class="bottom-welcome">{{$sloganPartner[0]->value}}</p>
                <div class="btn_1">{{$bottomPartner[0]->value}}</div>
                </a>
            </div>
            @endguest
         

            <div class="col-md-4">
            	<a class="box_work" href="/app-download">
                <img src="{{ asset('/storage/'.$imageDelivery[0]->value) }}" width="848" height="480" alt="" class="img-responsive">
				<h3>{{$tittleDelivery[0]->value}}<span></span></h3>
                <p>{{$sloganDelivery[0]->value}}</p>
                <div class="btn_1">{{$bottomDelivery[0]->value}}</div>
                </a>
            </div>
      </div><!-- End row -->
    </div><!-- End container -->


    <!-- Register partner modal -->   
<div class="modal" id="registerPartnerModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-popup">
            <a href="#" class="close-link"><i class="icon_close_alt2"></i></a>
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
            <div class="card-body">
                <div class="indent_title_in">
                    <h3 class="title-cost">QUIERO SER ALIADO ACASA</h3>
                </div>
                <form  action="{{route('registerPartner')}}" method="POST" id="formRegisterPartner">
                    @csrf
                    <div class="alert alert-danger" id="errorMsgRegisterPartner" style="display: none"></div>
                   
                    <div class="form-group">
                        <label for="company">{{ __('Nombre del negocio') }}</label>
                        <input id="company" type="text" class="form-control @error('company') is-invalid @enderror"
                            name="company" value="{{ old('company') }}" required autocomplete="company" autofocus>
                        @error('company')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="name">{{ __('Nombre') }}</label>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                            name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="last_name">{{ __('Apellidos') }}</label>
                        <input id="last_name" type="text"
                            class="form-control @error('last_name') is-invalid @enderror" name="last_name"
                            value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>
                        @error('last_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="email">{{ __('E-Mail') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="password">{{ __('Contraseña') }}</label>
                        <input id="password" type="password"
                            class="form-control @error('password') is-invalid @enderror" name="password"
                            required autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="password_confirmation">{{ __('Confirmar Contraseña') }}</label>
                        <input id="password_confirmation" type="password" class="form-control"
                            name="password_confirmation" required autocomplete="new-password">
                        @error('password_confirmation')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="phone">{{ __('Télefono') }}</label>
                                <input id="phone" type="number"
                                    class="form-control @error('phone') is-invalid @enderror" name="phone"
                                    value="{{ old('phone') }}" required autocomplete="phone" autofocus>
                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                        <div class="form-group">
                            <label for="city_id">{{ __('Ciudad') }}</label>
                            <select class="form-control @error('city_id') is-invalid @enderror"  name="city_id" required autocomplete="city_id" autofocus>
                              @foreach($cities as $city)      
                                <option value="{{$city->id}}">{{$city->name}}</option>
                              @endforeach
                            </select>
                            @error('city_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


                        <div class="form-group">
                            <label for="nit">{{ __('Nit') }}</label>
                            <input id="nit" type="text" class="form-control @error('nit') is-invalid @enderror"
                                name="nit" value="{{ old('nit') }}" required autocomplete="nit" autofocus>
                            @error('nit')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="fiscal_direction">{{ __('Dirección fiscal') }}</label>
                            <input id="fiscal_direction" type="text" class="form-control @error('fiscal_direction') is-invalid @enderror"
                                name="fiscal_direction" value="{{ old('fiscal_direction') }}" required autocomplete="fiscal_direction" autofocus>
                            @error('fiscal_direction')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>    

                        <div class="form-group">
                            <label for="bank_id">{{ __('Banco') }}</label>
                            <select class="form-control @error('bank_id') is-invalid @enderror"  name="bank_id" required autocomplete="bank_id" autofocus>
                              @foreach($banks as $bank)
                                <option value="{{$bank->id}}">{{$bank->name}}</option>
                              @endforeach
                            </select>
                            @error('bank_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="bank_account">{{ __('Numero de cuenta') }}</label>
                            <input id="bank_account" type="text" class="form-control @error('bank_account') is-invalid @enderror"
                                name="bank_account" value="{{ old('bank_account') }}" required autocomplete="bank_account" autofocus>
                            @error('bank_account')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="coin">{{ __('Moneda') }}</label>
                            <select class="form-control @error('coin') is-invalid @enderror"  name="coin" required autocomplete="coin" autofocus>
                                <option value="quetzal">{{ __('Quetzales')}}</option>
                                <option value="dolar">{{ __('Dólares')}}</option>
                            </select>

                            @error('coin')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="count_type">{{ __('Tipo de cuenta') }}</label>
                            <select class="form-control @error('count_type') is-invalid @enderror"  name="count_type" required autocomplete="count_type" autofocus>
                                <option value="monetaria">{{ __('Monetaria')}}</option>
                                <option value="ahorro">{{ __('Ahorro')}}</option>
                            </select>

                            @error('count_type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-9 col-sm-9">
                                    <input id="latitude" type="hidden" name="latitude">
                                    <input id="longitude" type="hidden" name="longitude">
                                    <input  class="form-control" id="address" name="address" type="text" required/>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <span class="input-group-btn">
                                        <button id="submit" type="button" class="form-control btn_2" style="color: #FFF;">Buscar</button>
                                    </span>
                                </div>
                            </div>                        
                            <div id="myMap"></div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 offset-md-12">
                                <div class="form-check">
                                    <input required class="" type="checkbox" name="conditions" id="conditions" {{ old('conditions') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="conditions">
                                        {{ __('Acepto los términos y condicione') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                    <button type="submit" class="btn_2">{{ __('Registrarme') }}
                    </button>

                </form>
            </div>

        </div>
    </div>
</div><!-- End modal --> 

@auth
      <!-- Alert modal -->   
      <div class="modal" id="alert" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content modal-popup">
                <a href="#" class="close-link"><i class="icon_close_alt2"></i></a>
                <div class="row">
                    <div class="col-md-12">
                    </div>
                </div>
                <div class="card-body">
                    <div class="indent_title_in">
                        <h3 class="title-cost">Tiene una sesión abierta como {{Auth::user()->name}} {{Auth::user()->last_name}}</h3>
                    </div>
                    <hr>
                    <p>Si desea registrarse como aliado, cierre sesión primero.</p>
                    <p>Si ya es aliado, puede ver más en Mi cuenta.</p>
                    <hr>
                    <a href="{{route('logout')}}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <div class="btn_2">Cerrar sesión</div>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </a>
                    <br>
                    @if (Auth::user()->role_id != 2)
                    <a href="/admin" data-dismiss="modal" aria-label="Close">
                        <div class="btn_2">Mi cuenta</div>
                    </a> 
                    @else
                    <a href="{{route('profile')}}" data-dismiss="modal" aria-label="Close">
                        <div class="btn_2">Mi cuenta</div>
                    </a> 
                    @endif
                  
        
                </div>
    
            </div>
        </div>
    </div><!-- End modal -->   
@endauth

    
@endsection

@section('css')
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@endsection