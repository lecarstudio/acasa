<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('home');

Auth::routes(); 
Route::group(['prefix' => '3eacb8d14340787eb6e1729b5de23cc9'], function () {
    Voyager::routes();
    Route::get('reports/delivery','ReportController@delivery');
    Route::get('reports/delivery/search','ReportController@searchDelivery')->name('report.delivery.search');
    Route::get('reports/orders','ReportController@order');
    Route::get('reports/orders/search','ReportController@searchOrder')->name('report.orders.search');
    Route::get('reports/favor','ReportController@favor');
    Route::get('reports/favor/search','ReportController@searchFavor')->name('report.favor.search');
    Route::get('reports/supermarket','ReportController@supermarket');
    Route::get('reports/supermarket/search','ReportController@searchSuper')->name('report.super.search');
    Route::get('sales-cut','ReportController@cutDelivery'); 
    Route::get('sales-cut/delivery/search','ReportController@searchCutDelivery')->name('report.cutDelivery.search');
    Route::get('sales-cut/partner','ReportController@cutStore'); 
    Route::get('sales-cut/partner/search','ReportController@searchCutStore')->name('report.cutStore.search');
    Route::get('sales-cut/favor','ReportController@cutFavor');
    Route::get('sales-cut/favor/search','ReportController@searchCutFavor')->name('report.cutFavot.search');
});

Route::group(['prefix' => '3eacb8d14340787eb6e1729b5de23cc9'], function () {
    Voyager::routes();
});

// Route::get('/admin', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'ProfileController@index')->name('profile');
Route::post('/profile/update', 'ProfileController@update')->name('profile.update');
Route::post('/profile/address', 'ProfileController@addAddress')->name('profile.address');
Route::get('/detail_page/{id}', 'DetailController@index');
Route::get('/order/product/add/{id}/{store}', 'DetailController@addItem')->name('addProduct');
Route::post('/order/update', 'OrderController@update')->name('order.update');
Route::get('/resume/{id}', 'ResumeController@index');
Route::get('/order', 'OrderController@index');
Route::get('/order/{status}/{id}', 'OrderController@index');
Route::post('/order/add', 'OrderController@addOrder')->name('order.create');
Route::get('/detail_order/{id}', 'DetailOrderController@index');
Route::get('/favorite', 'FavoriteController@index');
Route::get('/favorite/add/{store_id}', 'FavoriteController@create');
Route::get('/favorite/delete/{id}', 'FavoriteController@delete');
Route::get('/address/delete/{id}', 'ProfileController@destroy');
Route::get('/app-download', 'AppDownloadController@index');
Route::get('/categories/{id}', 'CategoriesController@index');
Route::get('/sub-categories/{id}/{category_id}', 'CategoriesController@subCategory');
Route::get('/find-order/{id}/{mes}', 'OrderController@findOrder');
Route::get('/list', 'ListController@index');
Route::get('/favor/{id}', 'FavorController@index');
Route::get('/supermarket/{id}', 'SupermarketController@index');

Route::resource('prueba', 'PruebaController');
Route::get('/app-download', 'AppDownloadController@index');
Route::post('/list/show', 'ListController@index')->name('list.show');
Route::post('/login-app', 'App\Auth\LoginController@login')->name('loginApp');
Route::post('/register-app', 'App\Auth\RegisterController@RegisterModal')->name('registerApp');
Route::post('/register-partner', 'App\Auth\RegisterController@RegisterPartner')->name('registerPartner');
Route::post('/register-partner', 'App\Auth\RegisterController@RegisterPartner')->name('registerPartner');
Route::post('/register-address', 'ProfileController@RegisterAddress')->name('register.address');
Route::post('/payment/order', 'OrderController@completePayment')->name('payment.payment');
Route::get('/notify/view/{id}', 'NotificationController@view')->name('notify.view');
Route::post('/register-payment', 'ProfileController@updateClient')->name('agregar.pago');
Route::get('/pp',  function (){
    return view('front.pp');
});
Route::get('/test',  function (){
    return view('front.test');
});



Route::get('/api/map-marker', 'Api\ApiRestaurantController@mapMarker');

