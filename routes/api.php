<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::namespace('App\Auth')->group(function () {
    Route::post('/login', 'LoginController@login');
    Route::post('/token', 'LoginController@token');
    Route::post('/register', 'RegisterController@registerMovil');

});
Route::namespace('App')->group(function () {
    Route::get('/store/category/{id}', 'StoreController@index');
    Route::get('/store/show/{id}', 'StoreController@show');
    Route::get('/categories', 'CategoryController@index');
    Route::get('/promotions', 'PromotionController@index');
    Route::post('/user/update', 'UserController@update');
    Route::post('/user/update/avatar', 'UserController@updateAvatar');
    Route::get('/address/{id}', 'AddressController@address');
    Route::post('/address', 'AddressController@addAddress');
    Route::delete('/address/{id}', 'AddressController@deleteAddress');
    Route::post('/order', 'OrderController@store');
    Route::post('/order/delete', 'OrderController@delete');
    Route::post('/order/option/delete', 'OrderController@deleteOption');
    Route::post('/order/confirm', 'OrderController@confirm');
    Route::post('/order/update/qty', 'OrderController@updateQty');
    Route::post('/order/option/update/qty', 'OrderController@updateOptionQty');
    Route::post('/order/option/store', 'OrderController@storeOption');
    Route::post('/get-order', 'OrderController@index');
    Route::post('/order/all', 'OrderController@all');
    Route::post('/order/resume', 'OrderController@resume');
    Route::get('/cities', 'CityController@index');
    Route::get('/ads/{id}', 'AdvertisementController@index');
    Route::post('/favorite', 'FavoriteController@store');
    Route::post('/favorite/delete', 'FavoriteController@destroy');
    Route::post('/favorite/user', 'FavoriteController@index');
    Route::post('/order/current', 'OrderController@currentOrder');
    Route::post('/notifications', 'NotificationController@index');
    Route::post('/store/search', 'StoreController@search');
    Route::post('/delivery/show', 'DeliveryController@show');
    Route::post('/delivery', 'DeliveryController@index');
    Route::post('/delivery/accept', 'DeliveryController@accept');
    Route::post('/delivery/confirm', 'DeliveryController@confirm');
    Route::post('/delivery/user', 'DeliveryController@user');
    Route::get('/resume/days', 'ResumeController@days');
});

