-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para acasa_test
CREATE DATABASE IF NOT EXISTS `acasa_test` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `acasa_test`;

-- Volcando estructura para tabla acasa_test.addresses
CREATE TABLE IF NOT EXISTS `addresses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `references` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `addresses_user_id_foreign` (`user_id`),
  CONSTRAINT `addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.addresses: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.address_stores
CREATE TABLE IF NOT EXISTS `address_stores` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.address_stores: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `address_stores` DISABLE KEYS */;
INSERT INTO `address_stores` (`id`, `address`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
	(1, 'Calle Ignacio Zaragoza, Ejido Modelo, Centro, 76000 Santiago de Querétaro, Qro., México', '23423423', '232342342342', '2020-07-15 02:19:39', '2020-07-15 02:19:39'),
	(2, 'asd', 'asd', 'asd', '2020-07-15 02:26:17', '2020-07-15 02:26:17');
/*!40000 ALTER TABLE `address_stores` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.advertisements
CREATE TABLE IF NOT EXISTS `advertisements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` bigint(20) unsigned NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `advertisements_store_id_foreign` (`store_id`),
  CONSTRAINT `advertisements_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.advertisements: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `advertisements` DISABLE KEYS */;
INSERT INTO `advertisements` (`id`, `store_id`, `images`, `status`, `created_at`, `updated_at`) VALUES
	(1, 1, 'advertisements\\July2020\\H872i7qZF30yEUNDPpNN.png', 1, '2020-07-18 18:44:00', '2020-07-18 22:14:30'),
	(2, 2, 'advertisements\\July2020\\C160ihgVKLvsoP2lZ196.png', 1, '2020-07-18 18:44:00', '2020-07-18 22:14:17'),
	(3, 3, 'advertisements\\July2020\\lgR2LImzyM4Vr1s0ZJBV.png', 1, '2020-07-18 18:45:00', '2020-07-18 22:13:35'),
	(4, 4, 'advertisements\\July2020\\gstxwXSPRKmw5eSGRF3I.jpg', 0, '2020-07-18 22:16:02', '2020-07-18 22:16:02');
/*!40000 ALTER TABLE `advertisements` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.cards
CREATE TABLE IF NOT EXISTS `cards` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cards_user_id_foreign` (`user_id`),
  CONSTRAINT `cards_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.cards: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.categories: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `image`, `name`, `slug`, `created_at`, `updated_at`) VALUES
	(1, 'categories\\July2020\\zotb7aNnHG19ImojC0Zg.png', 'Farmacias', 'farmacias', '2020-07-18 14:24:45', '2020-07-18 14:24:45'),
	(2, 'categories\\July2020\\UY6BiCFgnZOWPfQDA7am.png', 'Licores', 'licores', '2020-07-18 14:21:31', '2020-07-18 14:21:31'),
	(3, 'categories\\July2020\\ALA7H9ivM2T1G5rCB42d.png', 'Mandados', 'mandados', '2020-07-18 14:25:53', '2020-07-18 14:25:53'),
	(5, 'categories\\July2020\\t384ouLZv15hJgoL9BDm.png', 'Restaurantes', 'restaurantes', '2020-07-18 14:26:24', '2020-07-18 14:26:24'),
	(6, 'categories\\July2020\\TjiBbfHQbpkiZvLku0XR.png', 'Supermercado', 'supermercado', '2020-07-18 14:27:00', '2020-07-18 14:27:00');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.data_rows
CREATE TABLE IF NOT EXISTS `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.data_rows: ~159 rows (aproximadamente)
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
	(1, 1, 'id', 'number', 'ID', 1, 1, 0, 0, 0, 0, '{}', 1),
	(2, 1, 'name', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{}', 5),
	(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 6),
	(4, 1, 'password', 'password', 'Contraseña', 1, 0, 0, 1, 1, 0, '{}', 7),
	(5, 1, 'remember_token', 'text', 'RecordarToken', 0, 0, 0, 0, 0, 0, '{}', 9),
	(6, 1, 'created_at', 'timestamp', 'Creado', 0, 1, 1, 0, 0, 0, '{}', 11),
	(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
	(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '{}', 4),
	(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Rol', 0, 1, 1, 1, 1, 0, '{"model":"TCG\\\\Voyager\\\\Models\\\\Role","table":"roles","type":"belongsTo","column":"role_id","key":"id","label":"display_name","pivot_table":"roles","pivot":"0","taggable":"0"}', 3),
	(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{"model":"TCG\\\\Voyager\\\\Models\\\\Role","table":"roles","type":"belongsToMany","column":"id","key":"id","label":"display_name","pivot_table":"user_roles","pivot":"1","taggable":"0"}', 14),
	(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 15),
	(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
	(13, 2, 'name', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{}', 2),
	(14, 2, 'created_at', 'timestamp', 'Creado', 0, 0, 0, 0, 0, 0, '{}', 3),
	(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
	(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
	(17, 3, 'name', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{}', 2),
	(18, 3, 'created_at', 'timestamp', 'Creado', 0, 0, 0, 0, 0, 0, '{}', 3),
	(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
	(20, 3, 'display_name', 'text', 'Nombre para mostrar', 1, 1, 1, 1, 1, 1, '{}', 5),
	(21, 1, 'role_id', 'text', 'Rol', 0, 0, 0, 0, 0, 0, '{}', 2),
	(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
	(24, 4, 'name', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{}', 4),
	(25, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{"slugify":{"origin":"name"}}', 5),
	(26, 4, 'created_at', 'timestamp', 'Creado', 0, 0, 1, 0, 0, 0, '{}', 6),
	(27, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
	(28, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
	(29, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
	(30, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
	(31, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
	(32, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
	(33, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
	(34, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{"resize":{"width":"1000","height":"null"},"quality":"70%","upsize":true,"thumbnails":[{"name":"medium","scale":"50%"},{"name":"small","scale":"25%"},{"name":"cropped","crop":{"width":"300","height":"250"}}]}', 7),
	(35, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{"slugify":{"origin":"title","forceUpdate":true},"validation":{"rule":"unique:posts,slug"}}', 8),
	(36, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
	(37, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
	(38, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{"default":"DRAFT","options":{"PUBLISHED":"published","DRAFT":"draft","PENDING":"pending"}}', 11),
	(39, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
	(40, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
	(41, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
	(42, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
	(43, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
	(44, 6, 'author_id', 'text', 'Autor', 1, 0, 0, 0, 0, 0, '{}', 2),
	(45, 6, 'title', 'text', 'Título', 1, 1, 1, 1, 1, 1, '{}', 3),
	(46, 6, 'excerpt', 'text_area', 'Extracto', 0, 0, 1, 1, 1, 1, '{}', 4),
	(47, 6, 'body', 'rich_text_box', 'Cuerpo', 0, 0, 1, 1, 1, 1, '{}', 5),
	(48, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{"slugify":{"origin":"title"},"validation":{"rule":"unique:pages,slug"}}', 6),
	(49, 6, 'meta_description', 'text', 'Descripción meta', 0, 0, 1, 1, 1, 1, '{}', 7),
	(50, 6, 'meta_keywords', 'text', 'Meta Keywords', 0, 0, 1, 1, 1, 1, '{}', 8),
	(51, 6, 'status', 'select_dropdown', 'Estatus', 1, 1, 1, 1, 1, 1, '{"default":"INACTIVE","options":{"INACTIVE":"INACTIVE","ACTIVE":"ACTIVE"}}', 9),
	(52, 6, 'created_at', 'timestamp', 'Creado', 0, 1, 1, 0, 0, 0, '{}', 10),
	(53, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
	(54, 6, 'image', 'image', 'Imagen', 0, 1, 1, 1, 1, 1, '{}', 12),
	(55, 4, 'image', 'image', 'Imagen', 1, 1, 1, 1, 1, 1, '{}', 2),
	(56, 1, 'last_name', 'text', 'Apellidos', 0, 1, 1, 1, 1, 1, '{}', 8),
	(57, 1, 'phone', 'text', 'Télefono', 0, 1, 1, 1, 1, 1, '{}', 10),
	(58, 1, 'email_verified_at', 'timestamp', 'Verificar Email', 0, 1, 1, 1, 1, 1, '{}', 13),
	(59, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(60, 9, 'address', 'text', 'Dirección', 0, 1, 1, 1, 1, 1, '{}', 2),
	(61, 9, 'latitude', 'text', 'Latitud', 0, 1, 1, 1, 1, 1, '{}', 3),
	(62, 9, 'longitude', 'text', 'Longitud', 0, 1, 1, 1, 1, 1, '{}', 4),
	(63, 9, 'created_at', 'timestamp', 'Creado', 0, 1, 1, 1, 0, 1, '{}', 5),
	(64, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
	(65, 10, 'id', 'number', 'Id', 1, 1, 1, 0, 0, 0, '{}', 1),
	(66, 10, 'user_id', 'number', 'Id del usuario', 0, 0, 1, 1, 1, 0, '{}', 2),
	(67, 10, 'address_store_id', 'number', 'Id de la dirección de la tienda', 0, 0, 1, 1, 1, 1, '{}', 4),
	(68, 10, 'name', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{}', 8),
	(69, 10, 'logo', 'image', 'Logo', 1, 1, 1, 1, 1, 1, '{}', 9),
	(70, 10, 'cover', 'image', 'Portada', 0, 1, 1, 1, 1, 1, '{}', 11),
	(71, 10, 'images', 'multiple_images', 'Imagen de Banner', 0, 1, 1, 1, 1, 1, '{}', 12),
	(72, 10, 'description', 'text_area', 'Descripción', 0, 1, 1, 1, 1, 1, '{}', 13),
	(73, 10, 'short_description', 'text_area', 'Descripción Corta', 0, 1, 1, 1, 1, 1, '{}', 14),
	(74, 10, 'horary', 'time', 'Horario de inicio', 0, 1, 1, 1, 1, 1, '{}', 15),
	(75, 10, 'created_at', 'timestamp', 'Creado', 0, 1, 1, 1, 0, 1, '{}', 18),
	(76, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 19),
	(77, 10, 'close_horary', 'time', 'Horario de cierre', 0, 1, 1, 1, 1, 1, '{}', 17),
	(83, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(84, 13, 'user_id', 'text', 'Id del usuario', 1, 0, 0, 0, 0, 0, '{}', 2),
	(85, 13, 'name', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{}', 4),
	(86, 13, 'address', 'text', 'Dirección', 0, 1, 1, 1, 1, 1, '{}', 5),
	(87, 13, 'latitude', 'text', 'Latitud', 0, 1, 1, 1, 1, 1, '{}', 6),
	(88, 13, 'longitude', 'text', 'Longitud', 0, 1, 1, 1, 1, 1, '{}', 7),
	(89, 13, 'references', 'text', 'Referencias', 0, 1, 1, 1, 1, 1, '{}', 8),
	(90, 13, 'created_at', 'timestamp', 'Creado', 0, 1, 1, 1, 0, 1, '{}', 9),
	(91, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
	(92, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(93, 14, 'store_id', 'text', 'Id de la tienda', 1, 0, 1, 1, 1, 1, '{}', 2),
	(94, 14, 'category_id', 'text', 'Id de la categoría', 1, 0, 1, 1, 1, 1, '{}', 4),
	(95, 14, 'name', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{}', 6),
	(96, 14, 'image', 'image', 'Imagen', 0, 1, 1, 1, 1, 1, '{}', 7),
	(97, 14, 'price', 'number', 'Precio', 1, 1, 1, 1, 1, 1, '{}', 8),
	(98, 14, 'stars', 'number', 'Estrellas', 1, 1, 1, 1, 1, 1, '{}', 9),
	(99, 14, 'description', 'text_area', 'Descripción', 0, 1, 1, 1, 1, 1, '{}', 10),
	(100, 14, 'short_description', 'text_area', 'Descripción Corta', 0, 1, 1, 1, 1, 1, '{}', 11),
	(101, 14, 'status', 'select_dropdown', 'Estatus', 0, 1, 1, 1, 1, 1, '{"default":"1","options":{"0":"Agotado","1":"En Existencia"}}', 12),
	(102, 14, 'created_at', 'timestamp', 'Creado', 0, 1, 1, 1, 0, 1, '{}', 13),
	(103, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 14),
	(104, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(105, 15, 'user_id', 'text', 'Id del usuario', 1, 0, 0, 0, 0, 0, '{}', 2),
	(106, 15, 'product_id', 'text', 'Id del producto', 1, 0, 0, 0, 0, 0, '{}', 4),
	(107, 15, 'address_id', 'text', 'Id de la dirección', 1, 0, 0, 0, 0, 0, '{}', 6),
	(108, 15, 'card_id', 'text', 'Id de la tarjeta', 1, 0, 0, 0, 0, 0, '{}', 8),
	(109, 15, 'comments', 'text_area', 'Datos adicionales', 0, 1, 1, 1, 1, 1, '{}', 10),
	(110, 15, 'direction', 'text', 'Dirección', 0, 1, 1, 1, 1, 1, '{}', 11),
	(111, 15, 'delivery_date', 'date', 'Fecha de entrega', 1, 1, 1, 1, 1, 1, '{}', 12),
	(112, 15, 'delivery_hour', 'date', 'Hora de entrega', 1, 1, 1, 1, 1, 1, '{}', 13),
	(113, 15, 'payment_type', 'text', 'Tipo de pago', 1, 1, 1, 1, 1, 1, '{}', 14),
	(114, 15, 'phone', 'number', 'Teléfono', 1, 1, 1, 1, 1, 1, '{}', 15),
	(115, 15, 'tip', 'number', 'Propina', 1, 1, 1, 1, 1, 1, '{}', 16),
	(116, 15, 'total_cost', 'number', 'Total', 1, 1, 1, 1, 1, 1, '{}', 17),
	(117, 15, 'created_at', 'timestamp', 'Creado', 0, 1, 1, 1, 0, 1, '{}', 18),
	(118, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 19),
	(127, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(128, 17, 'product_id', 'number', 'Id del producto', 1, 0, 1, 1, 1, 1, '{}', 2),
	(129, 17, 'price', 'number', 'Precio', 1, 1, 1, 1, 1, 1, '{}', 4),
	(130, 17, 'validity_start', 'date', 'Fecha de inicio', 1, 1, 1, 1, 1, 1, '{}', 5),
	(131, 17, 'validity_end', 'date', 'Fecha de fin', 1, 1, 1, 1, 1, 1, '{}', 6),
	(132, 17, 'status', 'select_dropdown', 'Estatus', 1, 1, 1, 1, 1, 1, '{"default":"1","options":{"0":"Inactivo","1":"Activo"}}', 7),
	(133, 17, 'created_at', 'timestamp', 'Creado', 0, 1, 1, 1, 0, 1, '{}', 8),
	(134, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
	(135, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(136, 18, 'store_id', 'text', 'Id de la tienda', 1, 0, 1, 1, 1, 1, '{}', 2),
	(137, 18, 'images', 'image', 'Imagenes', 1, 1, 1, 1, 1, 1, '{}', 4),
	(138, 18, 'created_at', 'timestamp', 'Creado', 0, 1, 1, 1, 0, 1, '{}', 5),
	(139, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
	(148, 21, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(149, 21, 'user_id', 'number', 'Id del usuario', 1, 0, 0, 0, 0, 0, '{}', 2),
	(150, 21, 'store_id', 'number', 'Id de la tienda', 1, 1, 1, 1, 1, 1, '{}', 4),
	(151, 21, 'created_at', 'timestamp', 'Creado', 0, 1, 1, 1, 0, 1, '{}', 6),
	(152, 21, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
	(153, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(154, 24, 'product_id', 'number', 'Id del producto', 1, 0, 0, 0, 0, 0, '{}', 2),
	(155, 24, 'name', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{}', 4),
	(156, 24, 'extra-price', 'number', 'Precio extra', 0, 1, 1, 1, 1, 1, '{}', 5),
	(157, 24, 'created_at', 'timestamp', 'Creado', 0, 1, 1, 1, 0, 1, '{}', 6),
	(158, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
	(171, 10, 'store_belongsto_user_relationship', 'relationship', 'usuarios', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\User","table":"users","type":"belongsTo","column":"user_id","key":"id","label":"name","pivot_table":"address_stores","pivot":"0","taggable":"0"}', 3),
	(172, 10, 'store_belongsto_address_store_relationship', 'relationship', 'Dirección de la tienda', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\AddressStore","table":"address_stores","type":"belongsTo","column":"address_store_id","key":"id","label":"address","pivot_table":"address_stores","pivot":"0","taggable":"0"}', 5),
	(173, 14, 'product_belongsto_category_relationship', 'relationship', 'Categoría', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Category","table":"categories","type":"belongsTo","column":"category_id","key":"id","label":"name","pivot_table":"address_stores","pivot":"0","taggable":"0"}', 5),
	(174, 14, 'product_belongsto_store_relationship', 'relationship', 'Establecimiento', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Store","table":"stores","type":"belongsTo","column":"store_id","key":"id","label":"name","pivot_table":"address_stores","pivot":"0","taggable":"0"}', 3),
	(175, 13, 'address_belongsto_user_relationship', 'relationship', 'Usuario', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\User","table":"users","type":"belongsTo","column":"user_id","key":"id","label":"name","pivot_table":"address_stores","pivot":"0","taggable":"0"}', 3),
	(176, 18, 'advertisement_belongsto_store_relationship', 'relationship', 'Establecimiento', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Store","table":"stores","type":"belongsTo","column":"store_id","key":"id","label":"name","pivot_table":"address_stores","pivot":"0","taggable":"0"}', 3),
	(177, 21, 'favorite_belongsto_user_relationship', 'relationship', 'Usuario', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\User","table":"users","type":"belongsTo","column":"user_id","key":"id","label":"name","pivot_table":"address_stores","pivot":"0","taggable":"0"}', 3),
	(178, 21, 'favorite_belongsto_store_relationship', 'relationship', 'Establecimiento', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Store","table":"stores","type":"belongsTo","column":"store_id","key":"id","label":"name","pivot_table":"address_stores","pivot":"0","taggable":"0"}', 5),
	(179, 24, 'option_belongsto_product_relationship', 'relationship', 'Producto', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Product","table":"products","type":"belongsTo","column":"product_id","key":"id","label":"name","pivot_table":"address_stores","pivot":"0","taggable":"0"}', 3),
	(180, 15, 'order_belongsto_user_relationship', 'relationship', 'Usuario', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\User","table":"users","type":"belongsTo","column":"user_id","key":"id","label":"name","pivot_table":"address_stores","pivot":"0","taggable":"0"}', 3),
	(181, 15, 'order_belongsto_product_relationship', 'relationship', 'Producto', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Product","table":"products","type":"belongsTo","column":"product_id","key":"id","label":"name","pivot_table":"address_stores","pivot":"0","taggable":"0"}', 5),
	(182, 15, 'order_belongsto_address_relationship', 'relationship', 'Dirección', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Address","table":"addresses","type":"belongsTo","column":"address_id","key":"id","label":"address","pivot_table":"address_stores","pivot":"0","taggable":"0"}', 7),
	(183, 15, 'order_belongsto_card_relationship', 'relationship', 'Tarjeta', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Card","table":"cards","type":"belongsTo","column":"card_id","key":"id","label":"id","pivot_table":"address_stores","pivot":"0","taggable":"0"}', 9),
	(184, 17, 'promotion_belongsto_product_relationship', 'relationship', 'Producto', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Product","table":"products","type":"belongsTo","column":"product_id","key":"id","label":"name","pivot_table":"address_stores","pivot":"0","taggable":"0"}', 3),
	(185, 26, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
	(186, 26, 'name', 'text', 'Nombre', 1, 1, 1, 1, 1, 1, '{}', 2),
	(187, 26, 'created_at', 'timestamp', 'Creado', 0, 1, 1, 1, 0, 1, '{}', 3),
	(188, 26, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
	(189, 10, 'store_belongsto_sub_category_relationship', 'relationship', 'Subcategoría', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\SubCategory","table":"sub_categories","type":"belongsTo","column":"sub_category_id","key":"id","label":"name","pivot_table":"address_stores","pivot":"0","taggable":"0"}', 7),
	(190, 10, 'sub_category_id', 'number', 'Sub Category Id', 0, 0, 1, 1, 1, 1, '{}', 6),
	(191, 10, 'stars', 'text', 'Estrellas', 0, 1, 1, 1, 1, 1, '{}', 16),
	(192, 18, 'status', 'select_dropdown', 'Estatus', 0, 1, 1, 1, 1, 1, '{"default":"0","options":{"0":"Inactivo","1":"Activo"}}', 4),
	(193, 10, 'presentation_cover', 'image', 'Imagen de presentación', 1, 1, 1, 1, 1, 1, '{}', 10);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.data_types
CREATE TABLE IF NOT EXISTS `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.data_types: ~16 rows (aproximadamente)
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
	(1, 'users', 'users', 'Usuario', 'Usuarios', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', NULL, 1, 0, '{"order_column":"id","order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}', '2020-07-15 01:58:52', '2020-07-15 20:33:29'),
	(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}', '2020-07-15 01:58:52', '2020-07-15 19:53:56'),
	(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}', '2020-07-15 01:58:52', '2020-07-15 20:23:34'),
	(4, 'categories', 'categories', 'Categoría', 'Categorías', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}', '2020-07-15 01:58:52', '2020-07-18 14:13:43'),
	(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, NULL, NULL, 1, 0, '{"order_column":null,"order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}', '2020-07-15 01:58:52', '2020-07-15 20:47:09'),
	(9, 'address_stores', 'address-stores', 'Dirreción de tienda', 'Direcciones de tienda', 'voyager-location', 'App\\AddressStore', NULL, NULL, 'Dirección de los establecimientos registrados en la base de datos', 1, 0, '{"order_column":"id","order_display_column":"id","order_direction":"desc","default_search_key":null,"scope":null}', '2020-07-15 02:17:21', '2020-07-15 02:19:27'),
	(10, 'stores', 'stores', 'Establecimiento', 'Establecimientos', 'voyager-company', 'App\\Store', NULL, NULL, 'Establecimientos que se encuentran registrados en la aplicación.', 1, 0, '{"order_column":"id","order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}', '2020-07-15 16:25:39', '2020-07-18 22:26:18'),
	(13, 'addresses', 'addresses', 'Dirección', 'Direcciones', 'voyager-location', 'App\\Address', NULL, NULL, 'Direcciones que los usuarios registran en la aplicación.', 1, 0, '{"order_column":"id","order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}', '2020-07-15 16:43:10', '2020-07-15 20:45:15'),
	(14, 'products', 'products', 'Producto', 'Productos', 'voyager-hotdog', 'App\\Product', NULL, NULL, 'Productos registrados por los establecimientos.', 1, 0, '{"order_column":"id","order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}', '2020-07-15 16:50:10', '2020-07-18 23:42:27'),
	(15, 'orders', 'orders', 'Pedido', 'Pedidos', 'voyager-file-text', 'App\\Order', NULL, NULL, 'Pedidos registrados en la aplicación.', 1, 0, '{"order_column":"id","order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}', '2020-07-15 16:56:30', '2020-07-15 20:09:20'),
	(17, 'promotions', 'promotions', 'Promoción', 'Promociones', 'voyager-bolt', 'App\\Promotion', NULL, NULL, 'Promociones registradas.', 1, 0, '{"order_column":"id","order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}', '2020-07-15 17:06:15', '2020-07-18 23:45:51'),
	(18, 'advertisements', 'advertisements', 'Anuncio', 'Anuncios', 'voyager-paint-bucket', 'App\\Advertisement', NULL, NULL, 'Anuncios registrados.', 1, 0, '{"order_column":"id","order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}', '2020-07-15 17:08:32', '2020-07-18 22:12:26'),
	(21, 'favorites', 'favorites', 'Favorito', 'Favoritos', 'voyager-heart', 'App\\Favorite', NULL, NULL, 'Favoritos registrados por el usuario.', 1, 0, '{"order_column":"id","order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}', '2020-07-15 17:17:39', '2020-07-15 19:52:15'),
	(24, 'options', 'options', 'Opción', 'Opciones', 'voyager-list', 'App\\Option', NULL, NULL, 'Opciones de los productos.', 1, 0, '{"order_column":"id","order_display_column":null,"order_direction":"desc","default_search_key":null,"scope":null}', '2020-07-15 17:21:12', '2020-07-15 20:41:41'),
	(26, 'sub_categories', 'sub-categories', 'Subcategoría', 'Subcategorías', 'voyager-archive', 'App\\SubCategory', NULL, NULL, 'Se refiere a las subcategorías como el tipo de comida por ejemplo "China".', 1, 0, '{"order_column":"id","order_display_column":null,"order_direction":"desc","default_search_key":null}', '2020-07-18 09:49:53', '2020-07-18 09:49:53');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.delivery_orders
CREATE TABLE IF NOT EXISTS `delivery_orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `order_id` bigint(20) unsigned NOT NULL,
  `status` enum('Complete','Cancel','Process','Received') COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrival_time` time NOT NULL,
  `arrival_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `delivery_orders_user_id_foreign` (`user_id`),
  KEY `delivery_orders_order_id_foreign` (`order_id`),
  CONSTRAINT `delivery_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `delivery_orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.delivery_orders: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `delivery_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `delivery_orders` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.failed_jobs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.favorites
CREATE TABLE IF NOT EXISTS `favorites` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `store_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `favorites_user_id_foreign` (`user_id`),
  KEY `favorites_store_id_foreign` (`store_id`),
  CONSTRAINT `favorites_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`),
  CONSTRAINT `favorites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.favorites: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.menus: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'admin', '2020-07-15 01:58:52', '2020-07-15 01:58:52');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.menu_items
CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.menu_items: ~22 rows (aproximadamente)
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
	(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-07-15 01:58:52', '2020-07-15 01:58:52', 'voyager.dashboard', NULL),
	(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2020-07-15 01:58:52', '2020-07-15 02:20:03', 'voyager.media.index', NULL),
	(3, 1, 'Usuarios', '', '_self', 'voyager-person', '#000000', NULL, 3, '2020-07-15 01:58:52', '2020-07-15 22:33:24', 'voyager.users.index', 'null'),
	(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-07-15 01:58:52', '2020-07-15 01:58:52', 'voyager.roles.index', NULL),
	(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 16, '2020-07-15 01:58:52', '2020-07-18 09:51:12', NULL, NULL),
	(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-07-15 01:58:52', '2020-07-15 02:20:03', 'voyager.menus.index', NULL),
	(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-07-15 01:58:52', '2020-07-15 02:20:03', 'voyager.database.index', NULL),
	(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-07-15 01:58:52', '2020-07-15 02:20:03', 'voyager.compass.index', NULL),
	(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-07-15 01:58:52', '2020-07-15 02:20:03', 'voyager.bread.index', NULL),
	(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 17, '2020-07-15 01:58:52', '2020-07-18 09:51:12', 'voyager.settings.index', NULL),
	(11, 1, 'Categorías', '', '_self', 'voyager-categories', '#000000', NULL, 6, '2020-07-15 01:58:52', '2020-07-18 09:51:12', 'voyager.categories.index', 'null'),
	(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-07-15 01:58:53', '2020-07-15 02:20:03', 'voyager.hooks', NULL),
	(15, 1, 'Direcciones de tienda', '', '_self', 'voyager-location', NULL, NULL, 5, '2020-07-15 02:17:21', '2020-07-15 02:20:03', 'voyager.address-stores.index', NULL),
	(16, 1, 'Establecimientos', '', '_self', 'voyager-company', NULL, NULL, 8, '2020-07-15 16:25:42', '2020-07-18 09:51:12', 'voyager.stores.index', NULL),
	(18, 1, 'Direcciones', '', '_self', 'voyager-location', NULL, NULL, 9, '2020-07-15 16:43:10', '2020-07-18 09:51:12', 'voyager.addresses.index', NULL),
	(19, 1, 'Productos', '', '_self', 'voyager-hotdog', NULL, NULL, 10, '2020-07-15 16:50:10', '2020-07-18 09:51:12', 'voyager.products.index', NULL),
	(20, 1, 'Pedidos', '', '_self', 'voyager-file-text', NULL, NULL, 11, '2020-07-15 16:56:30', '2020-07-18 09:51:12', 'voyager.orders.index', NULL),
	(22, 1, 'Promociones', '', '_self', 'voyager-bolt', NULL, NULL, 12, '2020-07-15 17:06:15', '2020-07-18 09:51:12', 'voyager.promotions.index', NULL),
	(23, 1, 'Anuncios', '', '_self', 'voyager-paint-bucket', NULL, NULL, 13, '2020-07-15 17:08:32', '2020-07-18 09:51:12', 'voyager.advertisements.index', NULL),
	(25, 1, 'Favoritos', '', '_self', 'voyager-heart', NULL, NULL, 14, '2020-07-15 17:17:39', '2020-07-18 09:51:12', 'voyager.favorites.index', NULL),
	(26, 1, 'Opciones', '', '_self', 'voyager-list', NULL, NULL, 15, '2020-07-15 17:21:12', '2020-07-18 09:51:12', 'voyager.options.index', NULL),
	(27, 1, 'Subcategorías', '', '_self', 'voyager-archive', NULL, NULL, 7, '2020-07-18 09:49:54', '2020-07-18 09:51:12', 'voyager.sub-categories.index', NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.migrations: ~40 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_01_01_000000_add_voyager_user_fields', 1),
	(4, '2016_01_01_000000_create_data_types_table', 1),
	(5, '2016_01_01_000000_create_pages_table', 1),
	(6, '2016_01_01_000000_create_posts_table', 1),
	(7, '2016_02_15_204651_create_categories_table', 1),
	(8, '2016_05_19_173453_create_menu_table', 1),
	(9, '2016_10_21_190000_create_roles_table', 1),
	(10, '2016_10_21_190000_create_settings_table', 1),
	(11, '2016_11_30_135954_create_permission_table', 1),
	(12, '2016_11_30_141208_create_permission_role_table', 1),
	(13, '2016_12_26_201236_data_types__add__server_side', 1),
	(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
	(15, '2017_01_14_005015_create_translations_table', 1),
	(16, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
	(17, '2017_03_06_000000_add_controller_to_data_types_table', 1),
	(18, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
	(19, '2017_04_21_000000_add_order_to_data_rows_table', 1),
	(20, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
	(21, '2017_08_05_000000_add_group_to_settings_table', 1),
	(22, '2017_11_26_013050_add_user_role_relationship', 1),
	(23, '2017_11_26_015000_create_user_roles_table', 1),
	(24, '2018_03_11_000000_add_user_settings', 1),
	(25, '2018_03_14_000000_add_details_to_data_types_table', 1),
	(26, '2018_03_16_000000_make_settings_value_nullable', 1),
	(27, '2019_08_19_000000_create_failed_jobs_table', 1),
	(33, '2020_07_15_044128_create_address_stores_table', 2),
	(34, '2020_07_15_044129_create_stores_table', 2),
	(35, '2020_07_15_044131_create_cards_table', 2),
	(36, '2020_07_15_044132_create_addresses_table', 2),
	(37, '2020_07_15_044133_create_products_table', 2),
	(38, '2020_07_15_044134_create_orders_table', 2),
	(39, '2020_07_15_044135_create_promotions_table', 2),
	(40, '2020_07_15_044141_create_advertisements_table', 2),
	(41, '2020_07_15_044142_create_delivery_orders_table', 2),
	(42, '2020_07_15_044143_create_favorites_table', 2),
	(43, '2020_07_15_044144_create_options_table', 2),
	(44, '2020_07_15_044145_create_order_products_table', 2),
	(45, '2020_07_15_044147_create_option_order_products_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.options
CREATE TABLE IF NOT EXISTS `options` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra-price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `options_product_id_foreign` (`product_id`),
  CONSTRAINT `options_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.options: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
/*!40000 ALTER TABLE `options` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.option_order_products
CREATE TABLE IF NOT EXISTS `option_order_products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` bigint(20) unsigned NOT NULL,
  `order_product_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `option_order_products_option_id_foreign` (`option_id`),
  KEY `option_order_products_order_product_id_foreign` (`order_product_id`),
  KEY `option_order_products_product_id_foreign` (`product_id`),
  CONSTRAINT `option_order_products_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`),
  CONSTRAINT `option_order_products_order_product_id_foreign` FOREIGN KEY (`order_product_id`) REFERENCES `order_products` (`id`),
  CONSTRAINT `option_order_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.option_order_products: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `option_order_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `option_order_products` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `address_id` bigint(20) unsigned NOT NULL,
  `card_id` bigint(20) unsigned NOT NULL,
  `comments` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direction` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_date` date NOT NULL,
  `delivery_hour` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_type` enum('Card','Cash') COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `tip` int(11) NOT NULL,
  `total_cost` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_user_id_foreign` (`user_id`),
  KEY `orders_product_id_foreign` (`product_id`),
  KEY `orders_address_id_foreign` (`address_id`),
  KEY `orders_card_id_foreign` (`card_id`),
  CONSTRAINT `orders_address_id_foreign` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`),
  CONSTRAINT `orders_card_id_foreign` FOREIGN KEY (`card_id`) REFERENCES `cards` (`id`),
  CONSTRAINT `orders_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.orders: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.order_products
CREATE TABLE IF NOT EXISTS `order_products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_products_order_id_foreign` (`order_id`),
  KEY `order_products_product_id_foreign` (`product_id`),
  CONSTRAINT `order_products_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `order_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.order_products: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `order_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_products` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.pages: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
	(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2020-07-15 01:58:52', '2020-07-15 01:58:52');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.permissions: ~91 rows (aproximadamente)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
	(1, 'browse_admin', NULL, '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(2, 'browse_bread', NULL, '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(3, 'browse_database', NULL, '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(4, 'browse_media', NULL, '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(5, 'browse_compass', NULL, '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(6, 'browse_menus', 'menus', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(7, 'read_menus', 'menus', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(8, 'edit_menus', 'menus', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(9, 'add_menus', 'menus', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(10, 'delete_menus', 'menus', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(11, 'browse_roles', 'roles', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(12, 'read_roles', 'roles', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(13, 'edit_roles', 'roles', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(14, 'add_roles', 'roles', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(15, 'delete_roles', 'roles', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(16, 'browse_users', 'users', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(17, 'read_users', 'users', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(18, 'edit_users', 'users', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(19, 'add_users', 'users', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(20, 'delete_users', 'users', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(21, 'browse_settings', 'settings', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(22, 'read_settings', 'settings', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(23, 'edit_settings', 'settings', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(24, 'add_settings', 'settings', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(25, 'delete_settings', 'settings', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(26, 'browse_categories', 'categories', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(27, 'read_categories', 'categories', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(28, 'edit_categories', 'categories', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(29, 'add_categories', 'categories', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(30, 'delete_categories', 'categories', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(31, 'browse_posts', 'posts', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(32, 'read_posts', 'posts', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(33, 'edit_posts', 'posts', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(34, 'add_posts', 'posts', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(35, 'delete_posts', 'posts', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(36, 'browse_pages', 'pages', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(37, 'read_pages', 'pages', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(38, 'edit_pages', 'pages', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(39, 'add_pages', 'pages', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(40, 'delete_pages', 'pages', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(41, 'browse_hooks', NULL, '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(42, 'browse_address_stores', 'address_stores', '2020-07-15 02:17:21', '2020-07-15 02:17:21'),
	(43, 'read_address_stores', 'address_stores', '2020-07-15 02:17:21', '2020-07-15 02:17:21'),
	(44, 'edit_address_stores', 'address_stores', '2020-07-15 02:17:21', '2020-07-15 02:17:21'),
	(45, 'add_address_stores', 'address_stores', '2020-07-15 02:17:21', '2020-07-15 02:17:21'),
	(46, 'delete_address_stores', 'address_stores', '2020-07-15 02:17:21', '2020-07-15 02:17:21'),
	(47, 'browse_stores', 'stores', '2020-07-15 16:25:41', '2020-07-15 16:25:41'),
	(48, 'read_stores', 'stores', '2020-07-15 16:25:41', '2020-07-15 16:25:41'),
	(49, 'edit_stores', 'stores', '2020-07-15 16:25:41', '2020-07-15 16:25:41'),
	(50, 'add_stores', 'stores', '2020-07-15 16:25:41', '2020-07-15 16:25:41'),
	(51, 'delete_stores', 'stores', '2020-07-15 16:25:41', '2020-07-15 16:25:41'),
	(57, 'browse_addresses', 'addresses', '2020-07-15 16:43:10', '2020-07-15 16:43:10'),
	(58, 'read_addresses', 'addresses', '2020-07-15 16:43:10', '2020-07-15 16:43:10'),
	(59, 'edit_addresses', 'addresses', '2020-07-15 16:43:10', '2020-07-15 16:43:10'),
	(60, 'add_addresses', 'addresses', '2020-07-15 16:43:10', '2020-07-15 16:43:10'),
	(61, 'delete_addresses', 'addresses', '2020-07-15 16:43:10', '2020-07-15 16:43:10'),
	(62, 'browse_products', 'products', '2020-07-15 16:50:10', '2020-07-15 16:50:10'),
	(63, 'read_products', 'products', '2020-07-15 16:50:10', '2020-07-15 16:50:10'),
	(64, 'edit_products', 'products', '2020-07-15 16:50:10', '2020-07-15 16:50:10'),
	(65, 'add_products', 'products', '2020-07-15 16:50:10', '2020-07-15 16:50:10'),
	(66, 'delete_products', 'products', '2020-07-15 16:50:10', '2020-07-15 16:50:10'),
	(67, 'browse_orders', 'orders', '2020-07-15 16:56:30', '2020-07-15 16:56:30'),
	(68, 'read_orders', 'orders', '2020-07-15 16:56:30', '2020-07-15 16:56:30'),
	(69, 'edit_orders', 'orders', '2020-07-15 16:56:30', '2020-07-15 16:56:30'),
	(70, 'add_orders', 'orders', '2020-07-15 16:56:30', '2020-07-15 16:56:30'),
	(71, 'delete_orders', 'orders', '2020-07-15 16:56:30', '2020-07-15 16:56:30'),
	(77, 'browse_promotions', 'promotions', '2020-07-15 17:06:15', '2020-07-15 17:06:15'),
	(78, 'read_promotions', 'promotions', '2020-07-15 17:06:15', '2020-07-15 17:06:15'),
	(79, 'edit_promotions', 'promotions', '2020-07-15 17:06:15', '2020-07-15 17:06:15'),
	(80, 'add_promotions', 'promotions', '2020-07-15 17:06:15', '2020-07-15 17:06:15'),
	(81, 'delete_promotions', 'promotions', '2020-07-15 17:06:15', '2020-07-15 17:06:15'),
	(82, 'browse_advertisements', 'advertisements', '2020-07-15 17:08:32', '2020-07-15 17:08:32'),
	(83, 'read_advertisements', 'advertisements', '2020-07-15 17:08:32', '2020-07-15 17:08:32'),
	(84, 'edit_advertisements', 'advertisements', '2020-07-15 17:08:32', '2020-07-15 17:08:32'),
	(85, 'add_advertisements', 'advertisements', '2020-07-15 17:08:32', '2020-07-15 17:08:32'),
	(86, 'delete_advertisements', 'advertisements', '2020-07-15 17:08:32', '2020-07-15 17:08:32'),
	(92, 'browse_favorites', 'favorites', '2020-07-15 17:17:39', '2020-07-15 17:17:39'),
	(93, 'read_favorites', 'favorites', '2020-07-15 17:17:39', '2020-07-15 17:17:39'),
	(94, 'edit_favorites', 'favorites', '2020-07-15 17:17:39', '2020-07-15 17:17:39'),
	(95, 'add_favorites', 'favorites', '2020-07-15 17:17:39', '2020-07-15 17:17:39'),
	(96, 'delete_favorites', 'favorites', '2020-07-15 17:17:39', '2020-07-15 17:17:39'),
	(97, 'browse_options', 'options', '2020-07-15 17:21:12', '2020-07-15 17:21:12'),
	(98, 'read_options', 'options', '2020-07-15 17:21:12', '2020-07-15 17:21:12'),
	(99, 'edit_options', 'options', '2020-07-15 17:21:12', '2020-07-15 17:21:12'),
	(100, 'add_options', 'options', '2020-07-15 17:21:12', '2020-07-15 17:21:12'),
	(101, 'delete_options', 'options', '2020-07-15 17:21:12', '2020-07-15 17:21:12'),
	(102, 'browse_sub_categories', 'sub_categories', '2020-07-18 09:49:54', '2020-07-18 09:49:54'),
	(103, 'read_sub_categories', 'sub_categories', '2020-07-18 09:49:54', '2020-07-18 09:49:54'),
	(104, 'edit_sub_categories', 'sub_categories', '2020-07-18 09:49:54', '2020-07-18 09:49:54'),
	(105, 'add_sub_categories', 'sub_categories', '2020-07-18 09:49:54', '2020-07-18 09:49:54'),
	(106, 'delete_sub_categories', 'sub_categories', '2020-07-18 09:49:54', '2020-07-18 09:49:54');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.permission_role
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.permission_role: ~90 rows (aproximadamente)
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 1),
	(8, 1),
	(9, 1),
	(10, 1),
	(11, 1),
	(12, 1),
	(13, 1),
	(14, 1),
	(15, 1),
	(16, 1),
	(17, 1),
	(18, 1),
	(19, 1),
	(20, 1),
	(21, 1),
	(22, 1),
	(23, 1),
	(24, 1),
	(25, 1),
	(26, 1),
	(27, 1),
	(28, 1),
	(29, 1),
	(30, 1),
	(31, 1),
	(32, 1),
	(33, 1),
	(34, 1),
	(35, 1),
	(36, 1),
	(37, 1),
	(38, 1),
	(39, 1),
	(40, 1),
	(42, 1),
	(43, 1),
	(44, 1),
	(45, 1),
	(46, 1),
	(47, 1),
	(48, 1),
	(49, 1),
	(50, 1),
	(51, 1),
	(57, 1),
	(58, 1),
	(59, 1),
	(60, 1),
	(61, 1),
	(62, 1),
	(63, 1),
	(64, 1),
	(65, 1),
	(66, 1),
	(67, 1),
	(68, 1),
	(69, 1),
	(70, 1),
	(71, 1),
	(77, 1),
	(78, 1),
	(79, 1),
	(80, 1),
	(81, 1),
	(82, 1),
	(83, 1),
	(84, 1),
	(85, 1),
	(86, 1),
	(92, 1),
	(93, 1),
	(94, 1),
	(95, 1),
	(96, 1),
	(97, 1),
	(98, 1),
	(99, 1),
	(100, 1),
	(101, 1),
	(102, 1),
	(103, 1),
	(104, 1),
	(105, 1),
	(106, 1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.posts: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
	(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-07-15 01:58:52', '2020-07-15 01:58:52');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` bigint(20) unsigned NOT NULL,
  `category_id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `stars` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_store_id_foreign` (`store_id`),
  KEY `products_category_id_foreign` (`category_id`),
  CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `products_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.products: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `store_id`, `category_id`, `name`, `image`, `price`, `stars`, `description`, `short_description`, `status`, `created_at`, `updated_at`) VALUES
	(1, 3, 5, 'Monster Burger', 'products\\July2020\\gpOp4MCRVAHpLhuYVmzu.jpg', 90, 4, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 1, '2020-07-18 23:29:30', '2020-07-18 23:29:30'),
	(2, 4, 5, 'Sushitoys', 'products\\July2020\\hvTLhqFk87y7YjpF0U4M.jpg', 80, 5, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 0, '2020-07-18 23:31:00', '2020-07-18 23:43:24');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.promotions
CREATE TABLE IF NOT EXISTS `promotions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL,
  `price` int(11) NOT NULL,
  `validity_start` date NOT NULL,
  `validity_end` date NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `promotions_product_id_foreign` (`product_id`),
  CONSTRAINT `promotions_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.promotions: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `promotions` DISABLE KEYS */;
INSERT INTO `promotions` (`id`, `product_id`, `price`, `validity_start`, `validity_end`, `status`, `created_at`, `updated_at`) VALUES
	(1, 1, 80, '2020-07-18', '2020-07-21', 1, '2020-07-18 23:45:59', '2020-07-18 23:45:59'),
	(2, 2, 75, '2020-07-18', '2020-07-13', 1, '2020-07-18 23:46:37', '2020-07-18 23:46:37');
/*!40000 ALTER TABLE `promotions` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.roles: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'Administrator', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(2, 'user', 'Normal User', '2020-07-15 01:58:52', '2020-07-15 01:58:52');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.settings: ~11 rows (aproximadamente)
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
	(1, 'site.title', 'Site Title', 'Acasa', '', 'text', 1, 'Site'),
	(2, 'site.description', 'Site Description', 'Te lo llevamos acasa', '', 'text', 2, 'Site'),
	(3, 'site.logo', 'Site Logo', 'settings\\July2020\\iS4uT5XnrYTOy7uWY02p.png', '', 'image', 3, 'Site'),
	(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
	(5, 'admin.bg_image', 'Admin Background Image', 'settings\\July2020\\4fP1ojpjAPVD7Em1VXHV.png', '', 'image', 5, 'Admin'),
	(6, 'admin.title', 'Admin Title', 'Acasa Admin', '', 'text', 1, 'Admin'),
	(7, 'admin.description', 'Admin Description', 'Bienvenido a Acasa Admin', '', 'text', 2, 'Admin'),
	(8, 'admin.loader', 'Admin Loader', 'settings\\July2020\\NW6QR047m1SaF0agBUY8.png', '', 'image', 3, 'Admin'),
	(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\July2020\\3SO3bKM8PR3Ltb8wqE4Q.png', '', 'image', 4, 'Admin'),
	(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
	(11, 'admin.costKm', 'Costo por km', '5', NULL, 'text', 6, 'Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.stores
CREATE TABLE IF NOT EXISTS `stores` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `address_store_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `presentation_cover` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `horary` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `close_horary` time DEFAULT NULL,
  `sub_category_id` bigint(20) unsigned DEFAULT NULL,
  `stars` tinyint(4) DEFAULT NULL,
  `category_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stores_user_id_foreign` (`user_id`),
  KEY `stores_address_store_id_foreign` (`address_store_id`),
  KEY `stores_sub_category_id_index` (`sub_category_id`),
  KEY `stores_category_id_index` (`category_id`),
  CONSTRAINT `stores_address_store_id_foreign` FOREIGN KEY (`address_store_id`) REFERENCES `address_stores` (`id`),
  CONSTRAINT `stores_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.stores: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
INSERT INTO `stores` (`id`, `user_id`, `address_store_id`, `name`, `logo`, `presentation_cover`, `cover`, `images`, `description`, `short_description`, `horary`, `created_at`, `updated_at`, `close_horary`, `sub_category_id`, `stars`, `category_id`) VALUES
	(1, 1, 1, 'Casa Cantonesa', 'stores\\July2020\\hvLZJc8NNdxVaxw4p1J6.png', 'stores\\July2020\\aUTV3T9b0sYIyvx1cOEp.png', 'stores\\July2020\\eQM3oVxFIm5Fn0EeCtB3.png', '["stores\\\\July2020\\\\9OQIGxmsVB00XIjdSHbh.png","stores\\\\July2020\\\\jBiWwrK7LFR3nbmZXF4n.png","stores\\\\July2020\\\\b7O7HNRZwVor6Zj9doDU.png"]', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '09:00:00', '2020-07-18 15:03:00', '2020-07-18 22:30:56', '22:00:00', 1, 4, 2),
	(2, 1, 1, 'Del Griego', 'stores\\July2020\\qQa0ePELFZceUXKxoDFn.png', 'stores\\July2020\\xpLNzTjgDyU6exg3OMQZ.png', 'stores\\July2020\\gff0rgyoPNT3MnRexuVo.png', '["stores\\\\July2020\\\\AbMLmXRUQ9lse5bOyqux.png","stores\\\\July2020\\\\lTaEg2SexjkzfhrbNIka.png","stores\\\\July2020\\\\hqNvkecJmfJizcOb89mU.png"]', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '09:00:00', '2020-07-18 17:37:00', '2020-07-18 22:30:03', '21:00:00', 1, 5, 1),
	(3, 1, 1, 'La Estancia', 'stores\\July2020\\2sawrTpaUmvhu83zfon1.png', 'stores\\July2020\\372hE83vqu42fYd1ugjv.png', 'stores\\July2020\\DzHaKe0jYQH7Gp4KGXBY.png', '["stores\\\\July2020\\\\ym9nYKVeaGBG3U9YP7Ze.png","stores\\\\July2020\\\\3dz1KbJWxA69zsIYAbaR.png","stores\\\\July2020\\\\agKjtr65josaPpA5VunE.png"]', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '08:00:00', '2020-07-18 17:39:00', '2020-07-18 22:29:33', '23:00:00', 1, 2, 3),
	(4, 1, 1, 'Restaurante Luka', 'stores\\July2020\\lqxrtut2mVcKbu1etmr6.png', 'stores\\July2020\\cQ60iz8rcdc6aiEKr1sQ.png', 'stores\\July2020\\Sj9zwbBwwaxFClBUgnL7.png', '["stores\\\\July2020\\\\cdvQkZmGRMQxY0qudzr9.jpg","stores\\\\July2020\\\\mrOUeaUIBWKv26qW6GUL.jpg","stores\\\\July2020\\\\PbiEfl2Lo7UeSzbCdkG0.jpg"]', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '09:00:00', '2020-07-18 17:41:00', '2020-07-18 22:29:05', '21:00:00', 1, 4, 2),
	(5, 1, 1, 'Pizzeria Vesuvio', 'stores\\July2020\\cBlKy3WfUAkyssmL2W5r.png', 'stores\\July2020\\Oz7rxmRsNR8TFWNzGur4.jpg', 'stores\\July2020\\SAnOxytSdEfU5ouD5hKO.png', '["stores\\\\July2020\\\\qOvvepi6xkUv9kJ2SKoq.png","stores\\\\July2020\\\\UXRxBGH9HQBpAxfI10E2.png","stores\\\\July2020\\\\0ikZGkCXc44CIT9xeabg.png"]', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '07:00:00', '2020-07-18 17:45:00', '2020-07-18 22:28:43', '19:00:00', 1, 3, 2),
	(6, 1, 1, 'Pollo Campera', 'stores\\July2020\\F8gz2JyRJZorJcCFdV7T.png', 'stores\\July2020\\rPnREyWRcyPDYurQNhsv.png', 'stores\\July2020\\JwHDkLlnIp0hy6nRHXjA.png', '["stores\\\\July2020\\\\qdZTCkOyJCbl7CjbVDhN.jpg","stores\\\\July2020\\\\484zs7zAnNAAd3IZZPdq.jpg","stores\\\\July2020\\\\ofZXFdvyhMg0lZiYBGJo.jpg"]', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '08:00:00', '2020-07-18 17:46:00', '2020-07-18 22:28:24', '12:00:00', 1, 5, 4);
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.sub_categories
CREATE TABLE IF NOT EXISTS `sub_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sub_categories_category_id_index` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.sub_categories: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `sub_categories` DISABLE KEYS */;
INSERT INTO `sub_categories` (`id`, `name`, `created_at`, `updated_at`, `category_id`) VALUES
	(1, 'China', '2020-07-18 09:50:33', '2020-07-18 09:50:33', 1),
	(2, 'China1', '2020-07-18 09:50:33', '2020-07-18 09:50:33', 2),
	(3, 'China2', '2020-07-18 09:50:33', '2020-07-18 09:50:33', 2);
/*!40000 ALTER TABLE `sub_categories` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.translations
CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.translations: ~30 rows (aproximadamente)
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
	(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2020-07-15 01:58:52', '2020-07-15 01:58:52'),
	(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(21, 'menu_items', 'title', 2, 'pt', 'Media', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2020-07-15 01:58:53', '2020-07-15 01:58:53'),
	(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2020-07-15 01:58:53', '2020-07-15 01:58:53');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.users: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `role_id`, `name`, `last_name`, `phone`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Admin', NULL, NULL, 'admin@admin.com', 'users\\July2020\\PvjqUoaEdgLCrE3oYDdW.jpg', NULL, '$2y$10$3zZoBNrjj6kJrzKLk8VmT.6ydnh2NIeKUGJ1hOK03hAL/WZgl/Gwi', 'hzGTTtMcb0ivFLvWr6jO0Pf8RA9XmLKIXhaHmDr2dy3iZnaAoDgrqCkEMRbA', '{"locale":"es"}', '2020-07-15 01:58:52', '2020-07-16 15:16:36'),
	(2, NULL, 'Gustavo', NULL, NULL, 'gustavo@gmail.com', 'users\\July2020\\tavWpSoE1fxUR4dgd6te.jpg', NULL, '$2y$10$JajKFe.c2oNi5FhDi4m2IOt1gdlVIEEPJ1zBiTqyhysn4sXZkspm.', NULL, '{"locale":"es"}', '2020-07-16 16:24:40', '2020-07-16 16:24:40');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Volcando estructura para tabla acasa_test.user_roles
CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla acasa_test.user_roles: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
	(2, 1),
	(2, 2);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
