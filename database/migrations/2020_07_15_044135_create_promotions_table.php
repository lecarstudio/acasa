<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePromotionsTable extends Migration {

	public function up()
	{
		Schema::create('promotions', function(Blueprint $table) {
			$table->id();
			$table->foreignId('product_id')->constrained();
			$table->integer('price');
			$table->date('validity_start');
			$table->date('validity_end');
			$table->enum('status', array('Disable', 'Active'))->default('Active');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('promotions');
	}
}