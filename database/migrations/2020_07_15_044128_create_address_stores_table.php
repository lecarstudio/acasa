<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAddressStoresTable extends Migration {

	public function up()
	{
		Schema::create('address_stores', function(Blueprint $table) {
			$table->id();
			$table->string('address')->nullable();
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('address_stores');
	}
}