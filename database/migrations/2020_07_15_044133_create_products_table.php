<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->id();
			$table->foreignId('store_id')->constrained();
			$table->foreignId('category_id')->constrained();
			$table->string('name', 191);
			$table->string('image')->nullable();
			$table->integer('price')->default('0');
			$table->integer('stars')->default('0');
			$table->text('description')->nullable();
			$table->string('short_description')->nullable();
			$table->enum('status', array('Disable', 'Active'));
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('products');
	}
}