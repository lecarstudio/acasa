<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	public function up()
	{
		Schema::create('orders', function(Blueprint $table) {
			$table->id();
			$table->foreignId('user_id')->constrained();
			$table->foreignId('address_id')->constrained()->nullable();
			$table->foreignId('card_id')->constrained()->nullable();
			$table->string('comments')->nullable();
			$table->string('direction')->nullable();
			$table->date('delivery_date');
			$table->string('delivery_hour');
			$table->enum('payment_type',['Card','Cash']);
			$table->enum('status',['Pendiente','Entregada']);
			$table->integer('phone');
			$table->integer('tip');
			$table->integer('total_cost');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('orders');
	}
}