<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOptionsTable extends Migration {

	public function up()
	{
		Schema::create('options', function(Blueprint $table) {
			$table->id();
			$table->foreignId('product_id')->constrained();
			$table->string('name');
			$table->integer('extra-price')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('options');
	}
}