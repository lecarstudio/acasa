<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeliveryOrdersTable extends Migration {

	public function up()
	{
		Schema::create('delivery_orders', function(Blueprint $table) {
			$table->id();
			$table->foreignId('user_id')->constrained();
			$table->foreignId('order_id')->constrained();
			$table->enum('status', array('Complete', 'Cancel', 'Process', 'Received'));
			$table->time('arrival_time');
			$table->date('arrival_date');	
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('delivery_orders');
	}
}