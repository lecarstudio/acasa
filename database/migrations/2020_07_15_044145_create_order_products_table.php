<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderProductsTable extends Migration {

	public function up()
	{
		Schema::create('order_products', function(Blueprint $table) {
			$table->id();
			$table->foreignId('order_id')->constrained();
			$table->foreignId('product_id')->constrained();
			$table->integer('qty');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('order_products');
	}
}