<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStoresTable extends Migration {

	public function up()
	{
		Schema::create('stores', function(Blueprint $table) {
			$table->id();
			$table->foreignId('user_id')->constrained();
			$table->foreignId('address_store_id')->constrained();
			$table->foreignId('category_id')->constrained();
			$table->foreignId('sub_category_id')->constrained();
			$table->string('name');
			$table->string('logo');
			$table->string('cover')->nullable();
			$table->string('images')->nullable();
			$table->text('description')->nullable();
			$table->string('short_description', 191)->nullable();
			$table->string('horary')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('stores');
	}
}