<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdvertisementsTable extends Migration {

	public function up()
	{
		Schema::create('advertisements', function(Blueprint $table) {
			$table->id();
			$table->foreignId('store_id')->constrained();
			$table->string('images');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('advertisements');
	}
}