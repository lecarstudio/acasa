<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOptionOrderProductsTable extends Migration {

	public function up()
	{
		Schema::create('option_order_products', function(Blueprint $table) {
			$table->id();
			$table->foreignId('option_id')->constrained();
			$table->foreignId('order_product_id')->constrained();
			$table->foreignId('product_id')->constrained();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('option_order_products');
	}
}