<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCardsTable extends Migration {

	public function up()
	{
		Schema::create('cards', function(Blueprint $table) {
			$table->id();
			$table->foreignId('user_id')->constrained();
			$table->string('token');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('cards');
	}
}